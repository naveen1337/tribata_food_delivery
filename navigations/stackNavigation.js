import React from 'react';
import {createStackNavigator, StackView} from '@react-navigation/stack';
import {useSelector} from 'react-redux';

import RestaurantScreen from '../screens/RestaurantScreen';
import HomeScreen from '../screens/HomeScreen';

import LoginScreen from '../screens/LoginScreen';
import OTPScreen from '../screens/OTPScreen';
import OTPSuccessScreen from '../screens/OTPSuccessScreen';
import OrderTrackingScreen from '../screens/OrderTrackingScreen';
import AddressEntryScreen from '../screens/AddressEntryScreen';
import UserProfileScreen from '../screens/UserProfileScreen';
import ProfileEditScreen from '../screens/ProfileEditScreen';
import OrderHistoryScreen from '../screens/OrderHistoryScreen';
import SearchScreen from '../screens/SearchScreen';
import NoServiceScreen from '../screens/NoServiceScreen';
import CartScreen from '../screens/CartScreen';
import ProductsSearchScreen from '../screens/ProductsSearchScreen';

import AddMoneyScreen from '../screens/AddMoneyScreen';
import CheckOutScreen from '../screens/CheckOutScreen';
import PickUpLocationScreen from '../screens/PickUpLocationScreen';
import SlotBookingScreen from '../screens/SlotBookingScreen';
import WebViewScreen from '../screens/WebViewScreen';

import PickUpScreen from '../screens/PickUpScreen';
import PickupCheckOutScreen from '../screens/PickupCheckOutScreen';
import QRCodeScreen from '../screens/QRcodeScreen';
import EditAddressScreen from '../screens/EditAddressScreen';
import TopRestaurantScreen from '../screens/TopRestaurantScreen';

import BottomTabNavigation from './bottomTabNavigation';
import PickupScreen from '../screens/PickUpScreen';

const Stack = createStackNavigator();
const Services = createStackNavigator();
const HireDelivery = createStackNavigator();
const UserProfile = createStackNavigator();
const Auth = createStackNavigator();

const screenOptionsConfig = {
  headerShown: false,
};

export const AuthStack = (props) => {
  return (
    <Auth.Navigator screenOptions={screenOptionsConfig}>
      <Auth.Screen name="LoginScreen" component={LoginScreen} />
      <Auth.Screen name="OTPScreen" component={OTPScreen} />
      <Auth.Screen name="OTPSuccessScreen" component={OTPSuccessScreen} />
      <Auth.Screen name="ProfileEditScreen" component={ProfileEditScreen} />
      <Auth.Screen name="AddressEntryScreen" component={AddressEntryScreen} />
      <Auth.Screen name="HomeStack" component={HomeStack} />
      <Auth.Screen name="WebViewScreen" component={WebViewScreen} />
    </Auth.Navigator>
  );
};

export const HireDeliveryStack = (props) => {
  return (
    <HireDelivery.Navigator
      initialRouteName={'PickUpLocationScreen'}
      screenOptions={screenOptionsConfig}>
      <HireDelivery.Screen
        name="PickUpLocationScreen"
        component={PickUpLocationScreen}
      />
      <HireDelivery.Screen name="PickUpScreen" component={PickUpScreen} />
      <HireDelivery.Screen
        name="PickupCheckOutScreen"
        component={PickupCheckOutScreen}
      />
    </HireDelivery.Navigator>
  );
};

export const UserProfileStack = (props) => {
  return (
    <UserProfile.Navigator>
      <UserProfile.Screen
        name="ProfileEditScreen"
        component={ProfileEditScreen}
      />
    </UserProfile.Navigator>
  );
};

export const HomeStack = (props) => {
  return (
    <Stack.Navigator screenOptions={screenOptionsConfig}>
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
    </Stack.Navigator>
  );
};

export const SearchStack = (props) => {
  return (
    <Stack.Navigator
      screenOptions={{...screenOptionsConfig, keyboardHidesTabBar: true}}>
      <Stack.Screen name="SearchScreen" component={SearchScreen} />
      <Stack.Screen name="RestaurantScreen" component={RestaurantScreen} />
    </Stack.Navigator>
  );
};

export const UserStack = (props) => {
  return (
    <Stack.Navigator screenOptions={screenOptionsConfig}>
      <Stack.Screen name="UserProfileScreen" component={UserProfileScreen} />
    </Stack.Navigator>
  );
};

export const OrderStack = (props) => {
  return (
    <Stack.Navigator screenOptions={screenOptionsConfig}>
      <Stack.Screen name="CartScreen" component={CartScreen} />
      <Stack.Screen name="CheckOutScreen" component={CheckOutScreen} />
    </Stack.Navigator>
  );
};

export const UserLoginStack = (props) => {
  return (
    <Stack.Navigator screenOptions={{...screenOptionsConfig}}>
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
      <Stack.Screen name="OTPScreen" component={OTPScreen} />
      <Stack.Screen name="OTPSuccessScreen" component={OTPSuccessScreen} />
      <Stack.Screen name="ProfileEditScreen" component={ProfileEditScreen} />
      <Stack.Screen name="AddressEntryScreen" component={AddressEntryScreen} />
      <Stack.Screen name="HomeStack" component={HomeStack} />
      <Stack.Screen name="WebViewScreen" component={WebViewScreen} />
    </Stack.Navigator>
  );
};
