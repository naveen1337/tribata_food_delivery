import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {SideDrawer} from '../components/';
import BottomTabNavigation from './bottomTabNavigation';
import InitialScreen from '../screens/InitialScreen';
import OrderHistoryScreen from '../screens/OrderHistoryScreen';
import NoServiceScreen from '../screens/NoServiceScreen';
import AddMoneyScreen from '../screens/AddMoneyScreen';
import WebViewScreen from '../screens/WebViewScreen';
import RestaurantScreen from '../screens/RestaurantScreen';
import FavoritesScreen from '../screens/FavoritesScreen';
import QRcodeScanScreen from '../screens/QrcodeScanScreen';
import PastOrdersScreen from '../screens/PastOrdersScreen';
import QRcodeScreen from '../screens/QRcodeScreen';
import ProfileEditScreen from '../screens/ProfileEditScreen';
import EditAddressScreen from '../screens/EditAddressScreen';
import OrderTrackingScreen from '../screens/OrderTrackingScreen';
import SingleServiceScreen from '../screens/SingleServiceScreen';
import TopRestaurantScreen from '../screens/TopRestaurantScreen';
import SlotBookingScreen from '../screens/SlotBookingScreen';
import AddressEntryScreen from '../screens/AddressEntryScreen';
import UserLoginScreen from '../screens/LoginScreen';
import CheckOutScreen from '../screens/CheckOutScreen';
import HomeScreen from '../screens/HomeScreen';
import ProductsSearchScreen from '../screens/ProductsSearchScreen';
import CouponScreen from '../screens/CouponScreen';

import {
  HomeStack,
  UserStack,
  OrderStack,
  HireDeliveryStack,
  UserProfileStack,
  AuthStack,
} from './stackNavigation';

const Drawer = createDrawerNavigator();

const DrawerNavigation = (props) => {
  return (
    <Drawer.Navigator drawerContent={(props) => <SideDrawer {...props} />}>
      <Drawer.Screen name="InitialScreen" component={InitialScreen} />
      <Drawer.Screen name="BottomStack" component={BottomTabNavigation} />
      <Drawer.Screen name="OrderStack" component={OrderStack} />
      <Drawer.Screen name="OrderHistoryScreen" component={OrderHistoryScreen} />
      <Drawer.Screen
        options={{unmountOnBlur: true}}
        name="RestaurantScreen"
        component={RestaurantScreen}
      />
      <Drawer.Screen name="AuthStack" component={AuthStack} />

      <Drawer.Screen
        options={{unmountOnBlur: true}}
        name="CouponScreen"
        component={CouponScreen}
      />

      <Drawer.Screen name="CheckOutScreen" component={CheckOutScreen} />
      <Drawer.Screen
        name="ProductsSearchScreen"
        component={ProductsSearchScreen}
      />
      <Drawer.Screen name="UserStack" component={UserStack} />
      <Drawer.Screen name="HireDeliveryStack" component={HireDeliveryStack} />
      <Drawer.Screen name="ProfileEditScreen" component={ProfileEditScreen} />
      <Drawer.Screen name="QRcodeScreen" component={QRcodeScreen} />
      <Drawer.Screen name="PastOrdersScreen" component={PastOrdersScreen} />
      <Drawer.Screen name="EditAddressScreen" component={EditAddressScreen} />
      <Drawer.Screen name="SlotBookingScreen" component={SlotBookingScreen} />
      <Drawer.Screen name="AddressEntryScreen" component={AddressEntryScreen} />
      <Drawer.Screen name="HomeScreen" component={HomeScreen} />

      <Drawer.Screen name="NoServiceScreen" component={NoServiceScreen} />
      <Drawer.Screen
        options={{unmountOnBlur: true}}
        name="AddMoneyScreen"
        component={AddMoneyScreen}
      />

      <Drawer.Screen name="WebViewScreen" component={WebViewScreen} />
      <Drawer.Screen name="FavoritesScreen" component={FavoritesScreen} />
      <Drawer.Screen name="QRcodeScanScreen" component={QRcodeScanScreen} />
      <Drawer.Screen
        options={{unmountOnBlur: true}}
        name="OrderTrackingScreen"
        component={OrderTrackingScreen}
      />

      <Drawer.Screen
        name="SingleServiceScreen"
        component={SingleServiceScreen}
      />
      <Drawer.Screen
        name="TopRestaurantScreen"
        component={TopRestaurantScreen}
      />
    </Drawer.Navigator>
  );
};

export default DrawerNavigation;
