import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Pressable,
  TouchableOpacity,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import LinearGradient from 'react-native-linear-gradient';
import {useSelector} from 'react-redux';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

import {
  HomeStack,
  UserStack,
  SearchStack,
  AuthStack,
} from '../navigations/stackNavigation';

import HomeScreen from '../screens/HomeScreen';
import SearchScreen from '../screens/SearchScreen';
import CartScreen from '../screens/CartScreen';
import UserProfileScreen from '../screens/UserProfileScreen';

const Tab = createBottomTabNavigator();

export default BottomTabNavigation = ({navigation}) => {
  const cartState = useSelector((state) => state.user.cart);

  function MyTabBar({state, descriptors, navigation}) {
    const image1 = '../assets/icons/home.png';
    const image2 = '../assets/icons/search.png';
    const image3 = '../assets/icons/cart.png';
    const image4 = '../assets/icons/user.png';

    const focusedOptions = descriptors[state.routes[state.index].key].options;

    if (focusedOptions.tabBarVisible === false) {
      return null;
    }
    return (
      <LinearGradient style={tailwind('py-1')} colors={['#2E2E2E', '#000000']}>
        <View style={tailwind('flex flex-row justify-between')}>
          {state.routes.map((route, index) => {
            const {options} = descriptors[route.key];
            const label =
              options.tabBarLabel !== undefined
                ? options.tabBarLabel
                : options.title !== undefined
                ? options.title
                : route.name;

            const isFocused = state.index === index;

            const onPress = () => {
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                navigation.navigate(route.name);
              }
            };
            const onLongPress = () => {
              navigation.emit({
                type: 'tabLongPress',
                target: route.key,
              });
            };
            return (
              <TouchableOpacity
                key={label}
                accessibilityRole="button"
                accessibilityState={isFocused ? {selected: true} : {}}
                accessibilityLabel={options.tabBarAccessibilityLabel}
                testID={options.tabBarTestID}
                onPress={onPress}
                onLongPress={onLongPress}
                style={{flex: 1}}>
                <View
                  style={tailwind('flex flex-col items-center justify-center')}>
                  <View>
                    {index === 0 ? (
                      <Image
                        style={tailwind('w-6 h-6')}
                        source={require(image1)}
                      />
                    ) : index === 1 ? (
                      <Image
                        style={tailwind('w-6 h-6')}
                        source={require(image2)}
                      />
                    ) : index === 2 ? (
                      <>
                        <Image
                          style={tailwind('w-6 h-6')}
                          source={require(image3)}
                        />
                        {cartState.length > 0 ? (
                          <View
                            style={[
                              tailwind(
                                'absolute bg-black px-2 rounded-full left-4 bottom-1',
                              ),
                            ]}>
                            <Text
                              style={[
                                tailwind('text-white'),
                                styles.RegularFont,
                              ]}>
                              {cartState.length}
                            </Text>
                          </View>
                        ) : null}
                      </>
                    ) : (
                      <Image
                        style={tailwind('w-6 h-6')}
                        source={require(image4)}
                      />
                    )}
                  </View>
                  <Text
                    style={[
                      tailwind('text-center'),
                      styles.RegularFont,
                      styles.ColorSecondary,
                    ]}>
                    {label}
                  </Text>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </LinearGradient>
    );
    // <Tab.Navigator tabBar={(props) => <MyTabBar {...props} />}>
  }
  const UserState = useSelector((state) => state.app.userinfo);

  return (
    <Tab.Navigator
      tabBarOptions={{keyboardHidesTabBar: true}}
      tabBar={(props) => <MyTabBar {...props} />}>
      {/* This a Entry Screen Stack */}

      <Tab.Screen
        name="HomeStack"
        component={HomeStack}
        options={{
          tabBarLabel: 'Home',
        }}
      />
      <Tab.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{
          tabBarLabel: 'Search',
        }}
      />
      <Tab.Screen
        name="CartScreen"
        component={CartScreen}
        options={{
          tabBarLabel: 'Cart',
        }}
      />
      {UserState.login ? (
        <Tab.Screen
          name="UserStack"
          component={UserStack}
          options={{
            tabBarLabel: 'User',
          }}
        />
      ) : (
        <Tab.Screen
          options={{tabBarVisible: false}}
          name="AuthStack"
          component={AuthStack}
          options={{
            tabBarLabel: 'User',
          }}
        />
      )}
    </Tab.Navigator>
  );
};
