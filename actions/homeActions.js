import {
  SAVE_SERVICES,
  SAVE_TOPSELLERS,
  SAVE_PROMO_RESTAURANT,
  SAVE_TOP_HOTELS,
} from './actionTypes';

import {
  fetchServicesRemote,
  fecthTopSellersRemote,
  fetchPromoRestaurantsRemote,
  fetchTopHotelsRemote,
} from '../utils/homeRemoteService';

export const saveServices = (payload) => ({
  type: SAVE_SERVICES,
  payload,
});
export const saveTopSellers = (payload) => ({
  type: SAVE_TOPSELLERS,
  payload,
});

export const savePromoRestaurants = (payload) => ({
  type: SAVE_PROMO_RESTAURANT,
  payload,
});

export const saveTopHotels = (payload) => ({
  type: SAVE_TOP_HOTELS,
  payload,
});

export const fetchServices = (payload) => {
  return async (dispatch) => {
    try {
      // console.log(`Action: fetchServices Payload: ${payload}`)
      console.log('Fetch Service Hit');
      const data = await fetchServicesRemote(payload);
      // console.log(data)
      if (data) {
        dispatch(saveServices(data));
      }
    } catch (err) {
      console.log('Error', err);
    }
  };
};

export const fetchTopSellers = (payload) => {
  return async (dispatch) => {
    try {
      console.log(`Action: fetchTopSellers Payload: ${payload}`);
      const data = await fecthTopSellersRemote(payload);
      if (data) {
        dispatch(saveTopSellers(data));
      }
    } catch (err) {
      console.log('Error', err);
    }
  };
};

export const fetchPromoRestaurants = (payload) => {
  return async (dispatch) => {
    try {
      console.log(`Action: fetchPromoRestaurants Payload: ${payload}`);
      const data = await fetchPromoRestaurantsRemote(payload);
      if (data) {
        dispatch(savePromoRestaurants(data));
      }
    } catch (err) {
      console.log('Error', err);
    }
  };
};

export const fetchTopHotelsAction = (payload) => {
  return async (dispatch) => {
    try {
      console.log(`Action: fetchTopHotelsAction Payload: ${payload}`);
      const data = await fetchTopHotelsRemote(payload);
      if (data) {
        dispatch(saveTopHotels(data));
      }
    } catch (err) {
      console.log('Error', err);
    }
  };
};
