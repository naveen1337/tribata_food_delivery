import { fetchRestaurantInfoRemote } from '../utils/restaurantRemoteService'
import {SAVE_RESTAURANT_INFO} from './actionTypes';

export const saveRestaurantInfo = (payload) => ({
  type: SAVE_RESTAURANT_INFO,
  payload,
});


export const fetchRestaurantInfo = (payload) => {
    return async (dispatch) => {
      try {
        console.log(`Action: fetchRestaurantInfo Payload: ${payload}`)
        const data = await fetchRestaurantInfoRemote(payload);
        if(data){
            dispatch(saveRestaurantInfo(data));
        }

      } catch (err) {
        console.log('Error',err)
      }
    };
  };
