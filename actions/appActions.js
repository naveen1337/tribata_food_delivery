import _ from 'lodash';
import {PermissionsAndroid} from 'react-native';
import store from '../store';
import Geolocation from 'react-native-geolocation-service';
import {
  SAVE_USER_INFO,
  SAVE_LOCATION_COORS,
  SAVE_PICKUP_COST,
  SAVE_FAVORITE_RES,
  SAVE_FAVORITE_PRODUCT,
  SAVE_TRANSACTIONS,
} from './actionTypes';
import {fetchGetApiRemote} from '../utils/homeRemoteService';
import {
  getFavRestaurantRemote,
  getFavProductsRemote,
  getWalletHistoryRemote,
} from '../utils/userRemoteService';

export const saveLocationCords = (payload) => ({
  type: SAVE_LOCATION_COORS,
  payload,
});

export const save_user = (payload) => ({
  type: SAVE_USER_INFO,
  payload,
});

export const save_favorites_res = (payload) => ({
  type: SAVE_FAVORITE_RES,
  payload,
});
export const save_favorites_product = (payload) => ({
  type: SAVE_FAVORITE_PRODUCT,
  payload,
});

export const save_pickupcost = (payload) => ({
  type: SAVE_PICKUP_COST,
  payload,
});

export const save_transactions = (payload) => ({
  type: SAVE_TRANSACTIONS,
  payload,
});

export const fetchCoords = (payload) => {
  // const locationState = store.getState().app.location;
  //   console.log('>>', locationState);
  return new Promise((resolve, reject) => {
    try {
      console.log('Fetch Coords Hit');
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      )
        .then((granted) => {
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log('PERMISSION OK');
            Geolocation.getCurrentPosition(
              (position) => {
                let obj = {
                  latitude: position.coords.latitude,
                  longitude: position.coords.longitude,
                };
                store.dispatch(saveLocationCords(obj));
                resolve(obj);
              },
              (error) => {
                // See error code charts below.
                reject(err);
              },
              {
                enableHighAccuracy: true,
                timeout: 15000,
                maximumAge: 10000,
                accuracy: 'high',
                forceRequestLocation: true,
              },
            );
          } else {
            //   console.log('Permission Rejected')
            reject('Permission is Rejected');
          }
        })
        .catch((err) => {
          console.log('Error', err);
        });
      // const data = await fetchServicesRemote(payload);
      // // console.log(data)
      // dispatch(saveServices(data));
    } catch (err) {
      console.log('Error', err);
    }
  });
};

export const fetchPickupcostAction = (payload) => {
  return async (dispatch) => {
    try {
      console.log(`Action: fetchPickupcostAction Payload: ${payload}`);
      const data = await fetchGetApiRemote(payload);
      if (data) {
        dispatch(save_pickupcost(data.delivery1));
      }
    } catch (err) {
      console.log('Error', err);
    }
  };
};

export const saveFavoritesAction = (payload) => {
  return async (dispatch) => {
    try {
      let fav_restaurant = await getFavRestaurantRemote(payload);
      let fav_products = await getFavProductsRemote(payload);
      if (fav_restaurant) {
        dispatch(save_favorites_res(fav_restaurant));
      }
      if (fav_products) {
        dispatch(save_favorites_product(fav_products));
      }
    } catch (err) {
      console.log('Error', err);
    }
  };
};

export const saveTransactionHistoryAction = (payload) => {
  return async (dispatch) => {
    try {
      const history = await getWalletHistoryRemote(payload);
      if (history) {
        dispatch(save_transactions(history));
      }
    } catch (err) {
      dispatch(save_transactions(false));
      console.log('Error', err);
    }
  };
};
