// Home Reducer
export const SAVE_SERVICES = 'SAVE_SERVICES';
export const SAVE_TOPSELLERS = 'SAVE_TOPSELLERS';
export const SAVE_PROMO_RESTAURANT = 'SAVE_PROMO_RESTAURANT';
export const SAVE_TOP_HOTELS = 'SAVE_TOP_HOTELS';
// Restaurant
export const SAVE_OFFER = 'SAVE_OFFER';
export const SAVE_RESTAURANT_INFO = 'SAVE_RESTAURANT_INFO';

// User
export const ADDTO_CART = 'ADDTO_CART';
export const CLEAR_CART = 'CLEAR_CART';
export const SAVE_RESTAURANT = 'SAVE_RESTAURANT';
export const SAVE_FAVORITE_RES = 'SAVE_FAVORITE_RES';
export const SAVE_FAVORITE_PRODUCT = 'SAVE_FAVORITE_PRODUCT';
export const SAVE_COUPON = 'SAVE_COUPON';
export const SAVE_QRCODE = 'SAVE_QRCODE';

// App
export const SAVE_LOCATION_COORS = 'SAVE_LOCATION_COORS';
export const SAVE_USER_INFO = 'SAVE_USER_INFO';
export const SAVE_TRANSACTIONS = 'SAVE_TRANSACTIONS';
export const SAVE_PICKUP_COST = 'SAVE_PICKUP_COST';
export const CHANGE_NETWORK = 'CHANGE_NETWORK';
