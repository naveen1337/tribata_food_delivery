import _ from 'lodash';
import store from '../store';
import {
  ADDTO_CART,
  SAVE_RESTAURANT,
  SAVE_COUPON,
  CLEAR_CART,
  SAVE_QRCODE,
} from './actionTypes';
import {
  addUserAddressRemote,
  getUserAddresesRemote,
} from '../utils/userRemoteService';
import {fetchRestaurantInfoRemote} from '../utils/restaurantRemoteService';
import cloneDeep from 'lodash/cloneDeep';
import {save_user} from '../actions/appActions';

export const saveRestaurant = (payload) => ({
  type: SAVE_RESTAURANT,
  payload,
});
export const saveCart = (payload) => ({
  type: ADDTO_CART,
  payload,
});
export const save_coupon = (payload) => ({
  type: SAVE_COUPON,
  payload,
});
export const clearCart = (payload) => ({
  type: CLEAR_CART,
  payload,
});

export const saveQRCode = (payload) => ({
  type: SAVE_QRCODE,
  payload,
});

export const insertCartAction = (payload) => {
  return async (dispatch) => {
    // console.log('Insert Cart Action +++++++++++++++');
    const cartState = _.cloneDeep(store.getState().user.cart);
    // console.log(cartState)
    let isincart = _.find(cartState, {variation_id: payload.variation_id});
    if (isincart) {
      console.log('incart');
      let index = _.findIndex(cartState, {variation_id: payload.variation_id});
      let newobj = {
        ...payload,
      };
      newobj.quantity = newobj.quantity + 1;
      cartState.splice(index, 1, newobj);
      // console.log('New', cartState);
      dispatch(saveCart(cartState));
    } else {
      // console.log('not in cart');
      let newcart = [...cartState, payload];
      // console.log('New', newcart);
      dispatch(saveCart(newcart));
    }
  };
};

export const removeCartAction = (payload) => {
  return async (dispatch) => {
    console.log('Remove Cart Action +++++++++++++++');
    const cartState = _.cloneDeep(store.getState().user.cart);
    const item = _.find(cartState, {variation_id: payload.id});
    const index = _.findIndex(cartState, {variation_id: payload.id});
    if (item) {
      if (item.quantity > 1) {
        console.log('Reduce quantity');
        item.quantity = item.quantity - 1;
        cartState.splice(index, 1, item);
        dispatch(saveCart(cartState));
        //
      } else {
        cartState.splice(index, 1);
        dispatch(saveCart(cartState));
      }
    }
  };
};

export const saveRestaurantAction = (payload) => {
  return async (dispatch) => {
    const res_data = await fetchRestaurantInfoRemote(payload);
    if (res_data) {
      let newobj = {
        id: res_data[0].id,
        cityid: res_data[0].city_id,
        city_name: res_data[0].city_name,
        name: res_data[0].Restaurant_name,
        address: res_data[0].Restaurant_address,
        image: res_data[0].Restaurant_image,
        special: res_data[0].special,
      };
      dispatch(saveRestaurant(newobj));
    } else {
      console.log('saveRestaurantAction failed');
    }
  };
};

export const saveUserAddressAction = (payload) => {
  return async (dispatch) => {
    const alladdress = await getUserAddresesRemote({
      mobile_number: payload.mobile_number,
    });
    if (alladdress) {
      const updateuser = cloneDeep(store.getState().app.userinfo);
      updateuser.alladdress = alladdress;
      updateuser.selectedaddress = alladdress[alladdress.length - 1];
      dispatch(save_user(updateuser));
      return true;
    } else {
      console.log('saveRestaurantAction failed');
      return false;
    }
  };
};
