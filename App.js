import React, {useState, useEffect} from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import tailwind from 'tailwind-rn';
import {styles} from './constants/styles';
import messaging from '@react-native-firebase/messaging';
import NetInfo from '@react-native-community/netinfo';
import Modal from 'react-native-modal';
import RNExitApp from 'react-native-exit-app';
import {View, Text, Alert, Pressable} from 'react-native';

import DrawerNavigation from './navigations/rootDrawerNavigation';

import store from './store';

messaging().setBackgroundMessageHandler(async (remoteMessage) => {
  console.log('Message handled in the background!', remoteMessage);
});

const App = () => {
  const [online, setOnline] = useState(true);
  const unsubscribe = NetInfo.addEventListener((state) => {
    console.log('Is connected?', state.isConnected);
    if (!state.isConnected) {
      if (online) {
        setOnline(false);
      }
    }
  });

  const exit = () => {
    RNExitApp.exitApp();
  };

  useEffect(() => {
    const unsubscribenotification = messaging().onMessage(
      async (remoteMessage) => {
        console.log('Message', remoteMessage);
        Alert.alert(
          'A new FCM message arrived!',
          JSON.stringify(remoteMessage),
        );
      },
    );

    (async () => {
      const authStatus = await messaging().requestPermission();
      const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;

      if (enabled) {
        console.log('Authorization status:', authStatus);
      }
    })();
  }, []);

  return (
    <Provider store={store}>
      <NavigationContainer>
        <DrawerNavigation />
      </NavigationContainer>
      {online ? null : (
        <Modal isVisible={true}>
          <View style={tailwind('bg-white p-4 rounded-lg')}>
            <Text
              style={[tailwind('text-lg text-center pb-5'), styles.MediumFont]}>
              Internet connection is Mandatory to use that Application
            </Text>
            <View style={tailwind('flex flex-row justify-center')}>
              <Pressable
                onPress={exit}
                style={[
                  tailwind('w-32 bg-black border border-gray-300 rounded py-2'),
                ]}>
                <Text
                  style={[
                    tailwind('text-center text-white uppercase'),
                    styles.MediumFont,
                  ]}>
                  Exit the app
                </Text>
              </Pressable>
            </View>
          </View>
        </Modal>
      )}
    </Provider>
  );
};

export default App;
