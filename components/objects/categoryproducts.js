import React, {useState, useEffect} from 'react';
import {View, Text, Pressable} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import Collapsible from 'react-native-collapsible';
import {useSelector, useDispatch} from 'react-redux';

import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import {RestaurantProduct} from '../';
import _ from 'lodash';

import {fetchProductsRemote} from '../../utils/restaurantRemoteService';

export default function CategoryProducts(props) {
  const userState = useSelector((state) => state.app.userinfo);

  const [collapse, setcollapse] = useState(true);
  const [products, setproducts] = useState([]);
  const [loading, setloading] = useState(false);
  const [placeholder, setplaceholder] = useState(0);

  // useEffect(() => {
  //   console.log(props.name);
  // }, [collapse]);

  // For dummy component by number of products count
  useEffect(() => {
    setproducts([]);
    let dummy = _.range(props.count);
    setplaceholder(dummy);
  }, []);

  const productsremote = async (arg) => {
    setcollapse(!collapse);
    if (collapse && products.length === 0) {
      setloading(true);
      let data = await fetchProductsRemote({
        branch_id: props.hotelid,
        cat_id: arg,
        mobile_number: userState.login
          ? userState.mobilenumber
          : '+91987654321',
      });
      // console.log('>', data);
      setproducts(data);
      setloading(false);
    }
  };

  return (
    <Pressable
      key={props.id}
      onPress={() => {
        productsremote(props.id);
      }}
      style={tailwind('py-1 px-2 my-2')}>
      <View style={tailwind('px-4 flex flex-row justify-between')}>
        <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
          {props.name}
        </Text>
        <Icon name="chevron-down-outline" size={25} color="#C39E50" />
      </View>
      <Collapsible style={tailwind('py-2 px-2')} collapsed={collapse}>
        {loading
          ? placeholder.map((item) => {
              return (
                <View
                  key={item}
                  style={tailwind(
                    'h-32 mt-4 rounded rounded-lg mx-2 bg-white',
                  )}>
                  <Text
                    style={[
                      tailwind('px-4 py-3 text-base'),
                      styles.SemiBoldFont,
                    ]}>
                    Loading...
                  </Text>
                </View>
              );
            })
          : products.map((item) => {
              // console.log(">>>",item)

              return (
                <RestaurantProduct
                  onLayout={() => {
                    console.log('List Layout');
                  }}
                  key={item.id}
                  id={item.id}
                  veg={item.type === '1'}
                  name={item.product_name}
                  desc={item.description}
                  price={item.price}
                  cityid={props.cityid}
                  hotelid={item.restaurant_id}
                  hasvariation={item.variation === 'true'}
                  variation={item.price_variation}
                  variation_id={item.variation_id}
                  hasaddon={item.addon}
                  addon_content={item.addon_content}
                  status={item.product_status === '1'}
                />
              );
            })}
      </Collapsible>
    </Pressable>
  );
}
