// Visible in User Profile Screen, have a edit delete action

import React, {useState} from 'react';
import {View, Text, Pressable, Alert} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Modal from 'react-native-modal';
import Spinner from 'react-native-loading-spinner-overlay';
import {deleteAddressRemote} from '../../utils/userRemoteService';
import {useNavigation} from '@react-navigation/native';

export default function UserAddressBox(props) {
  const navigation = useNavigation();

  const [modal, setModal] = useState(false);
  const [spinner, setspinner] = useState(false);

  const handleDelete = async () => {
    setModal(false);
    setspinner(true);
    const result = await deleteAddressRemote({
      address_id: props.address.address_id,
    });
    if (result) {
      console.log('Deleted');
      setspinner(false);
    } else {
      console.log('Not Deleted');
      setspinner(false);
    }
  };

  return (
    <View elevation={3} style={tailwind('bg-white p-3 mx-3 my-2 rounded-lg')}>
      {/* Spinner */}
      <Spinner visible={spinner} />

      <View style={tailwind('flex flex-row justify-between px-2')}>
        <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
          {props.address.address_name}
        </Text>
        <View style={tailwind('flex flex-row')}>
          <Pressable
            onPress={() =>
              navigation.navigate('EditAddressScreen', {
                id: props.address.address_id,
              })
            }>
            <Text
              style={[
                tailwind('text-base uppercase px-2'),
                styles.SemiBoldFont,
                styles.ColorSecondary,
              ]}>
              Edit
            </Text>
          </Pressable>

          <Pressable onPress={() => setModal(true)}>
            <Text
              style={[
                tailwind('text-base uppercase px-2'),
                styles.SemiBoldFont,
                styles.ColorSecondary,
              ]}>
              Delete
            </Text>
          </Pressable>
        </View>
      </View>
      <View style={tailwind('py-2 px-2')}>
        <Text style={[tailwind('text-base'), styles.RegularFont]}>
          {props.address.address}
        </Text>
      </View>
      <View style={tailwind('px-2')}>
        <Text style={[tailwind('text-xs'), styles.RegularFont]}>
          {props.address.landmark}
        </Text>
      </View>
      {modal ? (
        <Modal isVisible={true}>
          <View style={tailwind('bg-white rounded-2xl p-4')}>
            <Text style={[tailwind('text-xl py-3'), styles.MediumFont]}>
              Are you sure want to delete the address
            </Text>

            <View style={tailwind('flex flex-row justify-between my-3 ')}>
              <Pressable
                onPress={() => setModal(false)}
                style={tailwind('border rounded-xl py-2 w-32')}>
                <Text
                  style={[
                    tailwind('text-center text-base'),
                    styles.SemiBoldFont,
                  ]}>
                  Cancel
                </Text>
              </Pressable>
              <Text style={tailwind('px-4')}></Text>
              <Pressable
                onPress={handleDelete}
                style={tailwind('border  bg-black rounded-xl py-2 w-32')}>
                <Text
                  style={[
                    tailwind('text-center text-white text-base'),
                    styles.SemiBoldFont,
                  ]}>
                  Ok
                </Text>
              </Pressable>
            </View>
          </View>
        </Modal>
      ) : null}
    </View>
  );
}
