import React from 'react';
import {View, Text, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function WalletBox(props) {
  return (
    <View elevation={4} style={tailwind('pt-4 pb-2 px-3 bg-white rounded-xl')}>
      <Text style={[tailwind('text-sm'), styles.RegularFont, styles.ColorGray]}>
        Your Bucketry Balance
      </Text>
      <View style={tailwind('flex flex-row justify-between py-2 items-center')}>
        <View style={tailwind('flex flex-row items-center')}>
          <Text style={[tailwind('text-xl'), styles.SemiBoldFont]}>₹</Text>
          <Text style={[tailwind('px-1 text-2xl'), styles.SemiBoldFont]}>
            {props.amount}
          </Text>
        </View>
        <Pressable onPress={() => props.navigation.navigate('AddMoneyScreen')}>
          <Text
            style={[
              tailwind('uppercase'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            Add Money
          </Text>
        </Pressable>
      </View>
    </View>
  );
}
