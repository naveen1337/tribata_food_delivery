import React from 'react';
import {View, Text} from 'react-native';
import tailwind from 'tailwind-rn';
import {useSelector} from 'react-redux';
import {styles} from '../../constants/styles';

export default function transactionHistory({item}) {
  const userState = useSelector((state) => state.app.userinfo);
  return (
    <View
      elevation={2}
      style={tailwind('bg-white mx-3 px-3 py-1 my-2 rounded-lg')}>
      <View style={tailwind('flex flex-row justify-between items-center py-1')}>
        {item.type === 0 ? (
          <Text
            style={[
              tailwind('text-base text-black w-8/12'),
              styles.RegularFont,
            ]}>
            Added to Wallet
          </Text>
        ) : null}
        {item.type === 1 ? (
          item.sender_phone === userState.mobilenumber ? (
            <Text
              style={[
                tailwind('text-base text-black w-8/12'),
                styles.RegularFont,
              ]}>
              Amount Transfer to {item.receiver}
            </Text>
          ) : (
            <Text
              style={[
                tailwind('text-base text-black w-8/12'),
                styles.RegularFont,
              ]}>
              Amount Received from {item.sender_name}
            </Text>
          )
        ) : null}
        {item.type === 1 ? (
          item.sender_phone === userState.mobilenumber ? (
            <Text
              style={[tailwind('text-base text-red-400'), styles.SemiBoldFont]}>
              - ₹ {item.amount}
            </Text>
          ) : (
            <Text
              style={[
                tailwind('text-base text-green-500'),
                styles.SemiBoldFont,
              ]}>
              + ₹ {item.amount}
            </Text>
          )
        ) : (
          <Text
            style={[tailwind('text-base text-green-500'), styles.SemiBoldFont]}>
            + ₹ {item.amount}
          </Text>
        )}
      </View>
      <Text style={[tailwind('py-1 text-gray-500'), styles.RegularFont]}>
        {item.date}
      </Text>
    </View>
  );
}
