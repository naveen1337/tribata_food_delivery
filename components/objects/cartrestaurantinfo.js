import React from 'react';
import {View, Text,Image} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function CartRestaurantInfo(props) {
  return (
    <View style={tailwind('flex flex-row px-3')}>
      <Image resizeMode={'contain'} style={tailwind('w-20 h-20 rounded-lg')} source={{uri:props.image}}/>
      <View style={tailwind('px-3 pt-1')}>
        <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
          {props.name}
        </Text>
        <Text
          style={[tailwind('text-xs text-gray-500'), styles.SemiBoldFont]}>
          {props.special}
        </Text>
      </View>
    </View>
  );
}
