import React from 'react';
import {View, Text, Image} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function NearByRestaurant(props) {
  return (
    <View
      style={tailwind('bg-red-200 flex flex-row h-24 rounded w-6/12 mx-2 ')}>
      <View style={tailwind('flex flex-col justify-center z-10')}>
        <Image
          style={tailwind('w-20 h-20 rounded-lg')}
          source={{uri: props.image}}
        />
      </View>

      <View style={tailwind('bg-red-400 flex flex-col z-40')}>
        <Text>{props.name}</Text>
      </View>
    </View>
  );
}
