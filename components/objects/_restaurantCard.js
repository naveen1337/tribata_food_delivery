import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  Pressable,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export default function RestaurantCard(props) {
  return (
    <Pressable
      onPress={() =>
        props.navigation.navigate('RestaurantScreen', {
          hotel_id: props.id,
        })
      }
      style={[tailwind('flex flex-row')]}>
      <Image
        resizeMode={'contain'}
        style={[tailwind('rounded-3xl'), mystyles.image]}
        source={{uri: props.image}}
      />
      <View
        style={[
          tailwind(
            'bg-white my-2 h-28 border border-gray-300 border-2 rounded-xl flex-grow',
          ),
          mystyles.textbox,
        ]}>
        <View style={[mystyles.textcontent]}>
          <View style={tailwind('flex flex-row my-4 justify-between')}>
            <View style={tailwind('w-44 border-b-2 border-gray-300')}>
              <Text
                numberOfLines={1}
                style={[tailwind('text-base'), styles.SemiBoldFont]}>
                {props.name}
              </Text>
              <Text
                style={[
                  tailwind('text-xs'),
                  styles.ColorSecondary,
                  styles.RegularFont,
                ]}>
                {props.special}
              </Text>
            </View>
            {/* If store is closed show closed button */}
            {props.status ? (
              <Text></Text>
            ) : (
              <Text
                style={[
                  tailwind('uppercase text-red-400'),
                  mystyles.adjust,
                  styles.SemiBoldFont,
                ]}>
                Closed
              </Text>
            )}
          </View>

          {!props.offer ? (
            <View
              style={[
                tailwind('mr-16 flex flex-row items-center justify-between'),
              ]}>
              <View
                style={[tailwind('flex flex-row items-center items-center')]}>
                <Icon name="star" size={15} color="#CBA960" />
                <Text
                  style={[
                    tailwind('text-xs ml-1 text-gray-600'),
                    styles.SemiBoldFont,
                  ]}>
                  {props.rating}
                </Text>
              </View>
              <Text
                style={[
                  tailwind('text-xs ml-1 text-gray-600'),
                  styles.SemiBoldFont,
                ]}>
                {props.city}
              </Text>
              <Text
                style={[
                  tailwind('text-xs ml-1 text-gray-600'),
                  styles.SemiBoldFont,
                ]}>
                {props.distance}
              </Text>
            </View>
          ) : (
            <View
              style={[
                tailwind('mr-16 flex flex-row items-center justify-between'),
                mystyles.dashed,
              ]}>
              <Text style={[tailwind('ml-2'), styles.MediumFont]}>
                {props.offtertext} Offer
              </Text>
            </View>
          )}
        </View>
      </View>
    </Pressable>
  );
}

const mystyles = StyleSheet.create({
  image: {
    zIndex: 1000,
    position: 'relative',
    left: 10,
    top: 24,
    width: 80,
    height: 80,
  },
  textbox: {
    position: 'relative',
    right: 30,
    width: Dimensions.get('window').width - 60,
  },
  textcontent: {
    position: 'relative',
    left: 50,
  },
  adjust: {
    position: 'relative',
    right: 60,
  },
  dashed: {
    borderRadius: 5,
    borderWidth: 2,
    borderStyle: 'dashed',
    borderColor: '#CBA960',
  },
});

// Used In single Service Screen and Search Screen
