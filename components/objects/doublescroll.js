import React, {useState, useEffect} from 'react';
import {View, Text} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import chunk from 'lodash/chunk';

// Not used anywhere
export default function DoubleScroll(props) {
  const [items, setitems] = useState([]);

  useEffect(() => {
    if (props.items / 2 != 0) {
      props.items.pop();
      console.log('>>> IN', props.items.length);
      const result = chunk(props.items);
      console.log('> Out', result.length);
      setitems(result);
    }
  }, []);

  return (
    <View style={tailwind('border')}>
      {items.map((item) => {
        return <Text>{console.log(item)}</Text>;
      })}
    </View>
  );
}
