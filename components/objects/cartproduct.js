import React, {useState, useEffect} from 'react';
import {View, Text, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';

export default function CartProduct(props) {
  const [totaladdons, setTotaladdons] = useState(0);

  const cartState = useSelector((state) => state.user.cart);

  useEffect(() => {
    let addontotal = 0;
    props.addons.forEach((item) => {
      // console.log('7', parseInt(item.price) * parseInt(props.quantity));
      addontotal += parseInt(item.price) * parseInt(props.quantity);
    });
    setTotaladdons(addontotal);
  }, [cartState]);

  return (
    <View elevation={3} style={tailwind('mx-3 my-2 bg-white py-2 px-2 rounded-lg')}>
      <Text style={[tailwind('text-sm'), styles.SemiBoldFont]}>
        {props.name}
      </Text>
      <View style={tailwind('flex flex-row justify-between items-center py-1')}>
        {props.hasvariation ? (
          <Text
            style={[
              tailwind('text-xs py-1'),
              styles.ColorSecondary,
              styles.RegularFont,
            ]}>
            ({props.variation})
          </Text>
        ) : null}
        {props.addons.length > 0 ? (
          <Text
            style={[
              tailwind('text-xs py-1'),
              styles.ColorSecondary,
              styles.RegularFont,
            ]}>
            ({props.addons.length} Addons)
          </Text>
        ) : null}
      </View>

      <View style={tailwind('flex pt-1 flex-row justify-between items-center')}>
        <View style={tailwind('flex flex-row')}>
          <Text style={[tailwind('mr-1'), styles.MediumFont, styles.ColorGray]}>
            {props.quantity}
          </Text>
          <Text style={[tailwind('mr-1'), styles.MediumFont, styles.ColorGray]}>
            X
          </Text>
          <Text style={[tailwind('mr-1'), styles.MediumFont, styles.ColorGray]}>
            ₹ {props.price}
          </Text>

          {props.addons.length > 0 ? (
            <>
              <Text
                style={[tailwind('mr-1'), styles.MediumFont, styles.ColorGray]}>
                +
              </Text>
              <Text
                style={[tailwind('mr-1'), styles.MediumFont, styles.ColorGray]}>
                {totaladdons}
              </Text>
            </>
          ) : null}

          <Text
            style={[tailwind('ml-2'), styles.SemiBoldFont, styles.ColorGray]}>
            ₹ {parseInt(props.quantity) * parseInt(props.price) + totaladdons}
          </Text>
        </View>

        <View style={tailwind('flex flex-row items-center')}>
          <Pressable
            onPress={() =>
              props.removefromCart({
                id: props.id,
              })
            }>
            <Icon name="remove-circle" size={35} color="#000000" />
          </Pressable>

          <Text
            style={[
              tailwind('mx-2 mt-1'),
              styles.SemiBoldFont,
              styles.ColorGray,
            ]}>
            {props.quantity}
          </Text>
          <Pressable
            onPress={() =>
              props.addtoCart({
                id: props.id,
              })
            }>
            <Icon name="add-circle" size={35} color="#000000" />
          </Pressable>
        </View>
      </View>
    </View>
  );
}
