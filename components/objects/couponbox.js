import React from 'react';
import {View, Image, Text, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector, useDispatch} from 'react-redux';
import {save_coupon} from '../../actions/userAction';

export default function Couponbox(props) {
  const dispatch = useDispatch();
  return (
    <Pressable
      onPress={() =>
        props.navigation.navigate('CouponScreen', {
          store: props.store,
        })
      }>
      <Text
        style={[tailwind('text-base text-gray-500 p-2'), styles.SemiBoldFont]}>
        Coupon
      </Text>

      <View
        style={[
          tailwind(
            'bg-white rounded-lg p-3 mx-3 flex flex-row items-center justify-between',
          ),
          {
            borderRadius: 1,
            borderStyle: 'dashed',
            borderWidth: 1,
            borderColor: '#CBA960',
          },
        ]}>
        <View style={tailwind('flex flex-row items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-7 h-7')}
            source={require('../../assets/icons/discount.png')}
          />
          {props.coupon ? (
            <Text
              style={[
                tailwind('px-3 text-gray-700 uppercase'),
                styles.SemiBoldFont,
              ]}>
              {props.coupon.code}
            </Text>
          ) : (
            <Text
              style={[
                tailwind('px-3 text-gray-700 uppercase'),
                styles.SemiBoldFont,
              ]}>
              Apply Coupon Code
            </Text>
          )}
        </View>
        {props.coupon ? (
          <Pressable
            onPress={() => dispatch(save_coupon(false))}
            style={tailwind('px-3')}>
            <Icon name="close" size={25} color="#000000" />
          </Pressable>
        ) : (
          <Text
            style={[
              tailwind('uppercase text-base'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            Apply
          </Text>
        )}
      </View>
    </Pressable>
  );
}
