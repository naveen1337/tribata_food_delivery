import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Pressable,
  Dimensions,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export default function SmallRestaurantCard(props) {
  return (
    // hotel_id
    <Pressable
      onPress={() => {
        props.navigation.navigate('RestaurantScreen', {
          hotel_id: props.id,
        });
      }}
      style={[{width: Dimensions.get('window').width - 80}]}>
      <View style={tailwind('flex flex-row ')}>
        <Image
          resizeMode={'contain'}
          style={[tailwind('border rounded-xl'), mystyles.image]}
          source={{uri: props.image}}
        />

        <View
          style={[
            tailwind(
              'bg-white border border-gray-200 border-2 my-2 h-28 rounded-2xl flex-grow',
            ),
            mystyles.textbox,
          ]}>
          <View style={mystyles.textcontent}>
            <View style={tailwind('flex flex-row my-4 justify-between')}>
              <View
                style={[
                  tailwind('w-44 pt-1 relative right-1'),
                  {
                    borderWidth: 1,
                    borderTopColor: 'transparent',
                    borderLeftColor: 'transparent',
                    borderRightColor: 'transparent',
                    borderBottomColor: '#C39E50',
                  },
                ]}>
                <Text
                  numberOfLines={1}
                  style={[tailwind('text-sm pb-1'), styles.SemiBoldFont]}>
                  {props.name}
                </Text>
                <Text
                  style={[
                    tailwind('text-xs'),
                    styles.ColorSecondary,
                    styles.RegularFont,
                  ]}>
                  {props.special}
                </Text>

                {/* <Text
                  style={tailwind('border-b-2 mx-2 border-gray-200')}></Text> */}
              </View>

              {props.status ? (
                <Text></Text>
              ) : (
                <Text
                  style={[
                    tailwind('uppercase text-red-400'),
                    mystyles.adjust,
                    styles.SemiBoldFont,
                  ]}>
                  Closed
                </Text>
              )}
            </View>

            {props.hasoffer ? (
              <View
                style={[
                  tailwind('mr-16 flex flex-row items-center'),
                  mystyles.dashed,
                ]}>
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-5 h-5')}
                  source={require('../../assets/icons/discount.png')}
                />
                <Text style={[tailwind('ml-2'), styles.MediumFont]}>
                  {props.offer} Offer
                </Text>
              </View>
            ) : (
              <View
                style={[
                  tailwind('mr-16 flex flex-row items-center justify-between'),
                ]}>
                <Text
                  style={[
                    tailwind('text-xs ml-1 text-gray-600'),
                    styles.RegularFont,
                  ]}>
                  {props.city}
                </Text>
                <Icon name="ellipse" size={5} color="#CBA960" />
                <Text
                  style={[
                    tailwind('text-xs ml-1 text-gray-600'),
                    styles.RegularFont,
                  ]}>
                  {props.distance}
                </Text>
              </View>
            )}
          </View>
        </View>
      </View>
    </Pressable>
  );
}

const mystyles = StyleSheet.create({
  image: {
    zIndex: 1000,
    position: 'relative',
    left: 5,
    top: 24,
    width: 80,
    height: 80,
  },
  textbox: {
    position: 'relative',
    right: 40,
    width: Dimensions.get('window').width - 130,
  },
  textcontent: {
    position: 'relative',
    left: 50,
    bottom: 4,
    // backgroundColor:"green",
    // right:30
  },
  adjust: {
    position: 'relative',
    right: 20,
  },
  dashed: {
    borderRadius: 5,
    borderWidth: 1,
    borderStyle: 'dashed',
    borderColor: '#CBA960',
  },
});

// Used In Home Screen Coupon Offers and Nearby Restaurants, Same Bubble Design but Width Is small
