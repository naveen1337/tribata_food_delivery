import React, {useState, useEffect} from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector, useDispatch} from 'react-redux';
import {addFavResRemote} from '../../utils/userRemoteService';
import {save_favorites_res} from '../../actions/appActions';
import Snackbar from 'react-native-snackbar';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';

export default function RestaurantInfo(props) {
  const RestaurantInfoState = useSelector((state) => state.restaurant.info);

  const dispatch = useDispatch();
  const FavoritesState = useSelector((state) => state.app.fav_res);

  const [favorite, setfavorite] = useState(false);

  const userState = useSelector((state) => state.app.userinfo);
  const locationState = useSelector((state) => state.app.location);

  useEffect(() => {
    const isinbox = find(FavoritesState, {id: props.id});

    if (isinbox) {
      setfavorite(true);
    } else {
      setfavorite(false);
    }
  }, []);

  const toggle = async () => {
    if (userState.login) {
      const responce = await addFavResRemote({
        mobile_number: userState.mobilenumber,
        shop_id: props.id,
      });
      if (favorite) {
        const index = findIndex(FavoritesState, {id: props.id});
        let newFavoriteState = [...FavoritesState];
        newFavoriteState.splice(index, 1);
        dispatch(save_favorites_res(newFavoriteState));
        setfavorite(false);
        Snackbar.show({
          numberOfLines: 2,
          textColor: '#000000',
          backgroundColor: '#CBA960',
          text: 'Removed',
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: '#000000',
            onPress: () => {},
          },
        });
      } else {
        let newFavoriteState = [...FavoritesState];
        newFavoriteState.push(RestaurantInfoState[0]);
        dispatch(save_favorites_res(newFavoriteState));
        setfavorite(true);
        Snackbar.show({
          numberOfLines: 2,
          textColor: '#000000',
          backgroundColor: '#CBA960',
          text: 'Added',
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: '#000000',
            onPress: () => {},
          },
        });
      }
    } else {
      // User is not logged in
      Snackbar.show({
        numberOfLines: 2,
        textColor: '#000000',
        backgroundColor: '#CBA960',
        text: 'Login to Enable that feature',
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Ok',
          textColor: '#000000',
          onPress: () => {},
        },
      });
    }
  };

  return (
    <View style={tailwind('')}>
      <Pressable
        onPress={() =>
          props.navigation.navigate('ProductsSearchScreen', {
            branch_id: props.id,
            cityid: props.city_id,
          })
        }
        style={[
          tailwind(
            'flex flex-row  border mx-3 mb-4 items-center   rounded py-2',
          ),
          {borderColor: '#CBA960'},
        ]}>
        <Text
          style={[
            tailwind('text-yellow-400 flex-grow mx-3'),
            styles.RegularFont,
            styles.ColorSecondary,
          ]}>
          Search in {props.name}
        </Text>
        <Icon
          style={tailwind('mx-3')}
          name="search"
          size={20}
          color="#CBA960"
        />
      </Pressable>
      <View style={tailwind('flex flex-row')}>
        <Image
          style={tailwind('w-24 rounded-lg h-24 mx-2')}
          source={{uri: props.image}}
        />
        <View style={tailwind('flex flex-row flex-grow justify-between px-3')}>
          <View style={tailwind('flex flex-col')}>
            <Text
              style={[
                tailwind('text-base'),
                styles.SemiBoldFont,
                styles.ColorSecondary,
              ]}>
              {props.name}
            </Text>
            <View
              style={[
                {
                  borderBottomColor: '#CBA960',
                  borderLeftColor: 'transparent',
                  borderRightColor: 'transparant',
                  borderTopColor: 'transparent',
                  borderWidth: 1,
                },
              ]}>
              <Text
                style={[
                  tailwind('text-xs pb-2'),
                  styles.RegularFont,
                  styles.ColorGray,
                ]}>
                {props.special}
              </Text>
            </View>

            <View style={tailwind('flex mt-3  flex-row items-center')}>
              <View style={tailwind('flex flex-row items-center')}>
                <Icon name="star" size={12} color="#CBA960" />
                <Text
                  style={[
                    styles.ColorSecondary,
                    styles.RegularFont,
                    tailwind('mx-1 text-xs'),
                  ]}>
                  {props.rating}
                </Text>
              </View>
              <Icon
                style={tailwind('mx-1')}
                name="ellipse"
                size={5}
                color="#CBA960"
              />
              <Text
                style={[
                  styles.ColorSecondary,
                  styles.RegularFont,
                  tailwind('text-xs mx-1'),
                ]}>
                {props.city_name}
              </Text>
              <Icon
                style={tailwind('mx-1')}
                name="ellipse"
                size={5}
                color="#CBA960"
              />
              <Text
                style={[
                  styles.ColorSecondary,
                  styles.RegularFont,
                  tailwind('text-xs mx-2'),
                ]}>
                {props.distance} km
              </Text>
            </View>
          </View>

          {favorite ? (
            <Pressable style={tailwind('relative right-5')} onPress={toggle}>
              <Icon name="heart" size={20} color="#CBA960" />
            </Pressable>
          ) : (
            <Pressable style={tailwind('relative right-5')} onPress={toggle}>
              <Icon name="heart-outline" size={20} color="#CBA960" />
            </Pressable>
          )}
        </View>
      </View>
    </View>
  );
}
