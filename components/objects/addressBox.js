// used in Cart Screen

import React, {useState, useEffect} from 'react';
import {View, Text, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Modal from 'react-native-modal';
import {useSelector, useDispatch} from 'react-redux';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import _ from 'lodash';
import {save_user} from '../../actions/appActions';

export default function AddressBox(props) {
  const dispatch = useDispatch();

  const userState = useSelector((state) => state.app.userinfo);
  const [modal, setmodal] = useState(false);
  const [value, setvalue] = useState('');

  const changevalue = (e) => {
    // console.log(e);
    let found = _.find(userState.alladdress, {address_id: e});
    if (found) {
      setvalue(found);
    }
  };
  const setselectedaddress = () => {
    console.log('=======');
    // console.log(value);
    let userclone = _.cloneDeep(userState);
    userclone.selectedaddress = value;
    dispatch(save_user(userclone));
    setmodal(false);
  };

  useEffect(() => {
    setvalue(userState.alladdress[0]);
  }, []);

  return (
    <View elevation={3} style={tailwind('bg-white p-3 mx-3 rounded-lg px-2')}>
      <View style={tailwind('flex flex-row justify-between items-center py-1')}>
        <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
          {props.address.address_name}
        </Text>
        <Pressable onPress={() => setmodal(true)}>
          <Text
            style={[
              tailwind('uppercase text-base'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            Change
          </Text>
        </Pressable>
      </View>
      <View style={tailwind('w-11/12')}>
        <Text style={[tailwind('text-sm'), styles.RegularFont]}>
          {props.address.address}
        </Text>
      </View>

      {/* Modal Section */}
      <Modal isVisible={modal} animationInTiming={100} animationOutTiming={100}>
        <View style={tailwind('bg-white rounded-2xl px-6 py-5')}>
          <View
            style={tailwind('flex flex-row justify-between items-center py-1')}>
            <Text style={[tailwind('text-lg'), styles.MediumFont]}>
              Select Delivery Address
            </Text>
            <Pressable onPress={() => setmodal(false)} style={tailwind('p-3')}>
              <Icon name="close" size={25} color="#000000" />
            </Pressable>
          </View>
          <View style={tailwind('border border-gray-200 rounded-lg py-2 my-2')}>
            <Picker
              style={{height: 30, width: '100%'}}
              selectedValue={value.address_id}
              mode={'dropdown'}
              onValueChange={(e, i) => changevalue(e)}>
              {userState.alladdress.map((item) => {
                return (
                  <Picker.Item
                    key={item.address_id}
                    label={item.address_name}
                    value={item.address_id}
                  />
                );
              })}
            </Picker>
          </View>
          <View style={tailwind('py-1 px-3')}>
            <Text
              style={[
                tailwind('text-base'),
                styles.RegularFont,
                styles.ColorSecondary,
              ]}>
              {value.address}
            </Text>
          </View>

          <Pressable
            onPress={setselectedaddress}
            style={[tailwind('py-3 bg-black w-full rounded-lg my-2')]}>
            <Text
              style={[
                tailwind('text-base uppercase text-center'),
                styles.ColorSecondary,
                styles.SemiBoldFont,
              ]}>
              Continue
            </Text>
          </Pressable>
          <Text
            style={[
              tailwind('text-center uppercase py-1 text-gray-600'),
              styles.RegularFont,
            ]}>
            OR
          </Text>
          <Pressable
            onPress={() => {
              setmodal(false);
              props.navigation.navigate('AddressEntryScreen', {
                back: 'cart',
              });
            }}
            style={tailwind('py-2 border rounded-lg border-gray-300')}>
            <Text
              style={[
                tailwind('text-base uppercase text-center'),
                styles.ColorSecondary,
                styles.SemiBoldFont,
              ]}>
              Add Address
            </Text>
          </Pressable>
        </View>
      </Modal>
    </View>
  );
}
