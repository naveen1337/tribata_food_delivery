import React from 'react';
import {View, Text, Image, Alert, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import {useSelector} from 'react-redux';
import Snackbar from 'react-native-snackbar';

export default function Services(props) {
  const userState = useSelector((state) => state.app.userinfo);
  const goto = () => {
    if (props.name === 'Foods') {
      props.navigation.navigate('SingleServiceScreen', {
        goto: 'nearby',
      });
    }

    if (props.name === 'Meats') {
      Snackbar.show({
        text: `${props.name} Service Currently Unavailable`,
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Ok',
          textColor: 'yellow',
          onPress: () => {},
        },
      });
    }
    if (props.name === 'Grocery') {
      Snackbar.show({
        text: `${props.name} Service Currently Unavailable`,
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Ok',
          textColor: 'yellow',
          onPress: () => {},
        },
      });
    }

    if (props.name === 'Fruits & Vegetables') {
      Snackbar.show({
        text: `${props.name} Service Currently Unavailable`,
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Ok',
          textColor: 'yellow',
          onPress: () => {},
        },
      });
    }

    if (props.name === 'Medicines') {
      Snackbar.show({
        text: `${props.name} Service Currently Unavailable`,
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Ok',
          textColor: 'yellow',
          onPress: () => {},
        },
      });
    }

    if (props.name === 'Wallet') {
      if (userState.login) {
        props.navigation.navigate('AddMoneyScreen');
      } else {
        Snackbar.show({
          text: `Login First`,
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: 'yellow',
            onPress: () => {},
          },
        });
      }
    }
    if (props.name === 'Slot Bookings') {
      if (userState.login) {
        props.navigation.navigate('SlotBookingScreen');
      } else {
        Snackbar.show({
          text: `Login First`,
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: 'yellow',
            onPress: () => {},
          },
        });
      }
    }
    if (props.name === 'Hire Delivery Boy') {
      if (userState.login) {
        props.navigation.navigate('HireDeliveryStack');
      } else {
        Snackbar.show({
          text: `Login First`,
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: 'yellow',
            onPress: () => {},
          },
        });
      }
    }
  };
  return (
    <Pressable
      onPress={() => {
        goto();
      }}
      style={tailwind('mx-2 pb-5 rounded-full')}>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-20 h-20')}
        source={{uri: props.image}}
      />
      <Text
        numberOfLines={2}
        style={[
          tailwind('text-center text-xs pt-2 w-20'),
          styles.ColorSecondary,
          styles.RegularFont,
        ]}>
        {props.name}
      </Text>
    </Pressable>
  );
}
