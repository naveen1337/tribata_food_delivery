// Checkout button apear in cart screen
import React, {useState, useEffect} from 'react';
import {View, Text, Image, Pressable, Alert} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';
import Snackbar from 'react-native-snackbar';

export default function CheckoutButton(props) {
  const cartState = useSelector((state) => state.user.cart);
  const userState = useSelector((state) => state.app.userinfo);

  const handleNavigate = () => {
    if (userState.login) {
      if (userState.alladdress.length > 0) {
        if (props.available) {
          if (parseInt(props.mincheckout) < parseInt(props.total)) {
            props.navigation.navigate('CheckOutScreen', {
              packing: props.packing,
              delivery: props.delivery,
              tax: props.tax,
              total: props.total,
            });
          } else {
            Snackbar.show({
              numberOfLines: 2,
              text: `Order Total Should be grater than ${props.mincheckout}`,
              duration: Snackbar.LENGTH_LONG,
              action: {
                text: 'Ok',
                textColor: 'yellow',
                onPress: () => {},
              },
            });
          }
        } else {
          Snackbar.show({
            numberOfLines: 2,
            text: `Restaurant Out Of Service`,
            duration: Snackbar.LENGTH_LONG,
            action: {
              text: 'Ok',
              textColor: 'yellow',
              onPress: () => {},
            },
          });
        }
      } else {
        props.navigation.navigate('AddressEntryScreen', {
          back: 'cart',
        });
      }
    } else {
      props.navigation.navigate('AuthStack');
    }
  };

  useEffect(() => {
    // console.log('3523452', userState);
  }, []);

  return (
    <View style={tailwind('w-full')}>
      {cartState.length > 0 ? (
        <View
          style={[
            tailwind(
              'rounded-lg mx-4 px-2 py-3 flex flex-row items-center justify-between',
            ),
            styles.BackgroundPrimary,
          ]}>
          <View style={tailwind('flex flex-row')}>
            <Text style={[tailwind('text-white uppercase'), styles.MediumFont]}>
              Total{' '}
            </Text>
            <Text
              style={[
                tailwind('text-white px-1 text-base'),
                styles.SemiBoldFont,
              ]}>
              ₹ {props.total}
            </Text>
          </View>
          <Pressable
            onPress={handleNavigate}
            style={tailwind(
              'border border-yellow-500 py-1 px-3  rounded flex flex-row items-center',
            )}>
            <Text style={[tailwind('text-white px-2'), styles.SemiBoldFont]}>
              Continue
            </Text>
          </Pressable>
        </View>
      ) : null}
    </View>
  );
}

// props.navigation.navigate('CheckOutScreen', {
//   packing: props.packing,
//   delivery: props.delivery,
//   tax: props.tax,
//   total: props.total,
//
// })
