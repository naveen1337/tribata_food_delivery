import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function OrderProcessingBar(props) {
  return (
    <View
      style={tailwind(
        'bg-white flex flex-row items-center py-2 justify-between',
      )}>
      <View style={tailwind('flex px-2 flex-row items-center')}>
        <Image
          resizeMode={'contain'}
          style={[tailwind('w-6 h-6'), {tintColor: '#000000'}]}
          source={require('../../assets/icons/cart.png')}
        />
        <Text
          style={[tailwind('text-sm text-gray-600 px-3'), styles.MediumFont]}>
          Your order is Processing
        </Text>
      </View>
      <Pressable
        onPress={() => {
          props.navigation.navigate('OrderHistoryScreen');
        }}
        style={tailwind(
          'border border-2 border-gray-600 px-4 mx-2 rounded-2xl',
        )}>
        <Text style={[tailwind('text-sm text-gray-600'), styles.MediumFont]}>
          Track
        </Text>
      </Pressable>
    </View>
  );
}
