import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Pressable,
  Dimensions,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

export default function SmallRestaurantCard(props) {
  const navigation = useNavigation();
  return (
    <Pressable
      android_ripple={{color: '#7f8c8d', borderless: false}}
      onPress={() =>
        navigation.navigate('RestaurantScreen', {hotel_id: props.id})
      }
      elevation={3}
      style={[
        tailwind('flex flex-row rounded-xl bg-white  m-2 mr-4'),
        {
          height: 100,
          marginLeft: 35,
          width:
            Dimensions.get('window').width - `${props.fullwidth ? 65 : 150}`,
        },
      ]}>
      <View
        style={{
          borderRadius: 8,
          overflow: 'hidden',
          borderWidth: 1,
          borderColor: '#CBA960',
          width: 74,
          height: 74,
          position: 'relative',
          top: 12,
          right: 35,
        }}>
        <Image
          resizeMode={'contain'}
          style={[tailwind(''), {width: '100%', height: '100%'}]}
          source={{uri: props.image}}
        />
      </View>
      <View
        style={{
          flexGrow: 1,
          position: 'relative',
          right: 25,
          width: Dimensions.get('window').width - 80,
          paddingVertical: 10,
        }}>
        <Text style={[styles.SemiBoldFont, tailwind('text-base')]}>
          {props.name}
        </Text>
        <Text
          style={[
            styles.RegularFont,
            styles.ColorSecondary,
            tailwind('text-xs pb-1'),
          ]}>
          {props.special}
        </Text>

        <View
          style={[
            tailwind('px-1 my-1 flex flex-row items-center'),
            {
              width:
                Dimensions.get('window').width -
                `${props.fullwidth ? 120 : 250}`,
              borderRadius: 6,
              borderWidth: 1.3,
              borderStyle: 'dashed',
              borderColor: '#CBA960',
            },
          ]}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-5 h-5')}
            source={require('../../assets/icons/discount.png')}
          />
          <Text style={[styles.MediumFont, {fontSize: 13, paddingVertical: 2}]}>
            {' '}
            {props.offer} Offer
          </Text>
        </View>
      </View>
    </Pressable>
  );
}

// Used In Home Screen Coupon Offers and Nearby Restaurants, Same Bubble Design but Width Is small
