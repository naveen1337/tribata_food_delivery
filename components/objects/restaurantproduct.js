import React, {useState, useEffect} from 'react';
import {View, Text, Alert, Image, Pressable} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {Picker} from '@react-native-picker/picker';
import find from 'lodash/find';
import findIndex from 'lodash/findIndex';
import Snackbar from 'react-native-snackbar';
import Modal from 'react-native-modal';
import {
  addFavProductRemote,
  getFavProductsRemote,
} from '../../utils/userRemoteService';
import {save_favorites_product} from '../../actions/appActions';

import {
  insertCartAction,
  removeCartAction,
  clearCart,
  saveRestaurantAction,
} from '../../actions/userAction';
import {tail} from 'lodash';

export default function RestaurantProduct(props) {
  const dispatch = useDispatch();
  const selectStoreState = useSelector((state) => state.user.restaurant);
  const FavoritesState = useSelector((state) => state.app.fav_product);
  const cartState = useSelector((state) => state.user.cart);
  const LocationState = useSelector((state) => state.app.location);
  const userState = useSelector((state) => state.app.userinfo);

  const [favorite, setfavorite] = useState(false);
  const [quantity, setquantity] = useState(0);
  const [addonmodal, setaddonmodal] = useState(false);
  const [clearcartmodal, setclearcartmodal] = useState(false);
  const [loading, setloading] = useState(false);
  // The All Addons
  const [addons, setaddons] = useState([]);
  const [svar, setsvar] = useState('');

  //Favorites Toggle
  const toggle = async () => {
    if (userState.login) {
      const responce = await addFavProductRemote({
        mobile_number: userState.mobilenumber,
        branch_id: props.hotelid,
        product_id: props.id,
      });
      if (responce) {
        if (favorite) {
          setfavorite(false);
          const index = findIndex(FavoritesState, {id: props.id});
          let newFavoriteState = [...FavoritesState];
          newFavoriteState.splice(index, 1);
          dispatch(save_favorites_product(newFavoriteState));
        } else {
          setfavorite(true);
          const getProducts = await getFavProductsRemote({
            mobile_number: userState.mobilenumber,
            latitude: LocationState.latitude,
            longitude: LocationState.longitude,
          });
          // console.log('>>', getProducts);
          dispatch(save_favorites_product(getProducts));
        }
      } else {
        Snackbar.show({
          numberOfLines: 2,
          textColor: '#000000',
          backgroundColor: 'red',
          text: 'Failed',
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: '#000000',
            onPress: () => {},
          },
        });
      }
    }
  };

  // Set the First Index variation (if it has) on Mounted
  useEffect(() => {
    const isinbox = find(FavoritesState, {id: props.id});
    if (isinbox) {
      setfavorite(true);
    }
    if (props.hasvariation) {
      // Set all variation quantity is equal to 0
      props.variation.forEach((item) => {
        item.quantity = 0;
        // Change the key name amount to price for simplicity
        item.price = item.amount;
      });
      // Set the First Index Variation on Mounted
      setsvar(props.variation[0]);
      setloading(true);
    } else {
      setquantity(0);
    }
    if (props.hasaddon) {
      // Add checked false in each addon content on mount
      props.addon_content.forEach((item) => {
        item.checked = false;
      });
      setaddons(props.addon_content);
    }
  }, []);

  // Change Change Variation
  const changevariation = (e) => {
    // console.log('input:', e);
    let isincart = find(cartState, {variation_id: e});
    if (isincart) {
      console.log('item in a cart');
      setsvar(isincart);
    } else {
      console.log('Item not in a cart');
      let variationresult = find(props.variation, {variation_id: e});
      setsvar(variationresult);
    }
  };

  const handleIncrement = (variation_id) => {
    if (cartState.length === 0) {
      // Save the Restaurnant
      dispatch(
        saveRestaurantAction({
          latitude: LocationState.latitude,
          longitude: LocationState.longitude,
          hotel_id: props.hotelid,
        }),
      );
      // Check the addon present
      if (props.hasaddon) {
        setaddonmodal(true);
      } else {
        addtoCart(variation_id);
      }
    } else {
      // Check that store
      if (selectStoreState.id === props.hotelid) {
        console.log('handleIncrement clicked', variation_id);
        const isincart = find(cartState, {variation_id: variation_id.id});
        // If item not in cart, handle addon conditions
        if (isincart) {
          addtoCart(variation_id);
        } else {
          if (props.hasaddon) {
            setaddonmodal(true);
          } else {
            addtoCart(variation_id);
          }
        }
      } else {
        setclearcartmodal(true);
        // Alert.alert('Wait', 'Your cart Contains another restaurant products');
      }
    }
  };

  // Toggle Add on Checkbox [REMOVED]
  const togglecheckbox = (e) => {
    // console.log('Add', e);
    let getselectedaddon = find(addons, {id: e});
    if (getselectedaddon) {
      getselectedaddon.checked = !getselectedaddon.checked;
      // console.log(getselectedaddon)
      let index = findIndex(addons, {id: getselectedaddon.id});
      addons.splice(index, 1, getselectedaddon);
      let newaddons = [...addons];
      // console.log(newaddons)
      setaddons(newaddons);
    }
  };

  const clearCartMethod = (args) => {
    dispatch(clearCart());
    setclearcartmodal(false);
    // console.log(args);
    if (props.hasvariation) {
      handleIncrement({id: svar.variation_id});
    } else {
      handleIncrement({id: props.variation_id});
    }
  };

  // Add on Insert
  const insertaddon = (arg) => {
    console.log('Insert Addon');
    let filtertrue = addons.filter((item) => item.checked === true);
    // Pass the addons
    addtoCart(arg, filtertrue);
    setaddonmodal(false);
  };

  const addtoCart = (variation, addon = []) => {
    console.log('##########################################');
    console.log('Add to Cart Payload', variation);
    if (props.hasvariation) {
      let isincart = find(cartState, {variation_id: variation.id});
      if (isincart === undefined) {
        // Variation not in cart
        let newobj = {
          id: props.id,
          name: props.name,
          price: svar.price,
          quantity: svar.quantity + 1,
          variation: svar.variation,
          variation_id: svar.variation_id,
          addons: addon,
        };
        // console.log('Cart Obj', newobj);
        dispatch(insertCartAction(newobj));
      } else {
        // Variation in a cart
        dispatch(insertCartAction(isincart));
      }
    } else {
      console.log('NO Variation Add to Cart');
      let isincart = find(cartState, {variation_id: variation.id});
      if (isincart) {
        dispatch(insertCartAction(isincart));
        setloading(!setloading);
      } else {
        // Not in a Cart
        let newobj = {
          id: props.id,
          name: props.name,
          price: props.price,
          quantity: quantity + 1,
          variation: false,
          variation_id: variation.id,
          addons: addon,
        };
        console.log('Cart Obj', newobj);
        dispatch(insertCartAction(newobj));
        setloading(!setloading);
      }
    }
  };

  const removefromCart = (e) => {
    // console.log(e);
    dispatch(removeCartAction(e));
    setloading(!setloading);
  };

  // Find that Item on Cart, and Update the Local State Quantity, when CartState Update
  useEffect(() => {
    console.log('Cart Mutated');
    if (props.hasvariation) {
      // If it has a variation
      let result = find(cartState, {variation_id: svar.variation_id});
      if (result) {
        // console.log('Found Variation', result);
        let newsvar = {
          ...svar,
        };
        newsvar.quantity = result.quantity;
        setsvar(newsvar);
      } else {
        setsvar(props.variation[0]);
      }
    } else {
      // It not has a Variation
      let result = find(cartState, {variation_id: props.variation_id});
      if (result) {
        console.log('Result', result);
        setquantity(result.quantity);
      } else {
        setquantity(0);
      }
    }
  }, [cartState, loading]);

  return (
    <View elevation={4} style={tailwind('px-3 rounded-lg bg-white my-2')}>
      <View style={tailwind('flex flex-row')}>
        <View style={tailwind('flex-grow pl-2')}>
          <View
            style={[
              tailwind('flex flex-row justify-between items-center py-2'),
            ]}>
            <View style={tailwind('w-11/12')}>
              <Text style={[tailwind('text-sm'), styles.SemiBoldFont]}>
                {props.name}
              </Text>
            </View>

            {favorite ? (
              <Pressable onPress={toggle}>
                <Icon name="heart" size={20} color="#CBA960" />
              </Pressable>
            ) : (
              <Pressable onPress={toggle}>
                <Icon name="heart-outline" size={20} color="#CBA960" />
              </Pressable>
            )}
          </View>
          <View style={tailwind('flex flex-row items-center')}>
            {props.veg ? (
              <Image
                resizeMode={'contain'}
                style={tailwind('w-3 h-3')}
                source={require('../../assets/icons/veg.png')}
              />
            ) : (
              <Image
                resizeMode={'contain'}
                style={tailwind('w-3 h-3')}
                source={require('../../assets/icons/nonveg.png')}
              />
            )}
            <Text
              numberOfLines={2}
              style={[
                tailwind('text-xs ml-2 w-10/12'),
                styles.RegularFont,
                styles.ColorGray,
              ]}>
              {props.desc}
            </Text>
          </View>

          <View
            style={tailwind(
              'flex flex-row justify-between  py-0 items-center',
            )}>
            {props.hasvariation ? (
              <View style={tailwind('')}>
                <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
                  ₹ {svar.price}
                </Text>
              </View>
            ) : (
              <View style={tailwind('')}>
                <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
                  ₹ {props.price}
                </Text>
              </View>
            )}

            <View style={tailwind('flex flex-row justify-between')}>
              {/* {console.log('Svar', svar)} */}
              {props.hasvariation ? (
                // If variation Load Picker
                <View>
                  <Picker
                    mode={'dropdown'}
                    selectedValue={svar.variation_id}
                    style={{height: 40, width: 120}}
                    onValueChange={(itemValue) => {
                      changevariation(itemValue);
                    }}>
                    {props.variation.map((item) => {
                      return (
                        <Picker.Item
                          key={item.variation_id}
                          label={item.variation}
                          value={item.variation_id}
                        />
                      );
                    })}
                  </Picker>
                </View>
              ) : (
                <View style={{height: 40, width: 120}}></View>
              )}
            </View>

            {props.hasvariation ? (
              <View style={tailwind('flex flex-row items-center')}>
                {svar.quantity > 0 ? (
                  <>
                    <Pressable
                      style={tailwind('')}
                      onPress={() => {
                        removefromCart({
                          id: svar.variation_id,
                        });
                      }}>
                      <Icon name="remove-circle" size={35} color="#000000" />
                    </Pressable>

                    <Text
                      style={[
                        tailwind('text-base mx-2 w-3'),
                        styles.SemiBoldFont,
                      ]}>
                      {svar.quantity}
                    </Text>
                  </>
                ) : (
                  <></>
                )}
                <Pressable
                  style={tailwind('')}
                  onPress={() => handleIncrement({id: svar.variation_id})}>
                  <Icon name="add-circle" size={35} color="#000000" />
                </Pressable>
              </View>
            ) : (
              // NO Variation Quantity Button
              <View
                style={tailwind('flex flex-row justify-between items-center')}>
                {quantity > 0 ? (
                  <>
                    <Pressable
                      onPress={() => {
                        removefromCart({
                          id: props.variation_id,
                        });
                      }}>
                      <Icon name="remove-circle" size={35} color="#000000" />
                    </Pressable>
                    <Text
                      style={[tailwind('text-base relative w-7 text-center'), styles.SemiBoldFont]}>
                      {quantity}
                    </Text>
                  </>
                ) : (
                  <></>
                )}
                <Pressable
                  onPress={() => handleIncrement({id: props.variation_id})}>
                  <Icon name="add-circle" size={35} color="#000000" />
                </Pressable>
              </View>
            )}
          </View>
          {props.hasaddon ? (
            <View style={tailwind('flex flex-row justify-between')}>
              <Text style={tailwind('pb-1')}></Text>
              <Text
                style={[
                  styles.RegularFont,
                  {fontSize: 11},
                  styles.ColorSecondary,
                ]}>
                customizable
              </Text>
            </View>
          ) : (
            <Text style={tailwind('pb-0')}></Text>
          )}
        </View>
      </View>

      {/* Add on Modal */}
      <Modal
        style={[{justifyContent: 'flex-end', margin: 0}]}
        isVisible={addonmodal}>
        <View
          style={[tailwind('bg-white px-3 py-6'), styles.BackgroundSecondary]}>
          <View>
            <Text style={[tailwind('px-2 text-base'), styles.SemiBoldFont]}>
              {props.name}
            </Text>
            <Text style={[tailwind('px-2 text-xs'), styles.RegularFont]}>
              {props.desc}
            </Text>
            <View style={[tailwind('py-4')]}>
              <Pressable
                style={[
                  tailwind('px-2 text-sm'),
                  styles.RegularFont,
                  styles.ColorSecondary,
                ]}
                onPress={() => {
                  setaddonmodal(false);
                }}>
                <Text
                  style={[
                    tailwind(''),
                    styles.SemiBoldFont,
                    styles.ColorSecondary,
                  ]}>
                  {'X Addons'}
                </Text>
              </Pressable>
            </View>
          </View>
          {addons.map((item) => {
            return (
              <Pressable
                elevation={4}
                onPress={() => togglecheckbox(item.id)}
                key={item.id}
                style={tailwind(
                  'flex my-2 py-4 flex-row justify-between items-center bg-white rounded px-4',
                )}>
                <View elevation={5} style={tailwind('flex flex-row')}>
                  {item.checked ? (
                    <Icon name="checkbox" size={20} color="green" />
                  ) : (
                    <Icon name="square-outline" size={20} color="#000000" />
                  )}
                  <Text
                    style={[tailwind('text-base px-2'), styles.SemiBoldFont]}>
                    {item.name}
                  </Text>
                </View>
                <Text>
                  <Text
                    style={[tailwind('px-2 text-base'), styles.SemiBoldFont]}>
                    ₹ {item.price}
                  </Text>
                </Text>
              </Pressable>
            );
          })}
          {props.hasvariation ? (
            <Pressable
              elevation={3}
              onPress={() => insertaddon({id: svar.variation_id})}
              style={tailwind('bg-black py-3  rounded-lg my-3')}>
              <Text
                style={[
                  tailwind('text-center uppercase text-base'),
                  styles.SemiBoldFont,
                  styles.ColorSecondary,
                ]}>
                Add to cart
              </Text>
            </Pressable>
          ) : (
            <Pressable
              elevation={3}
              onPress={() => insertaddon({id: props.variation_id})}
              style={tailwind('bg-black py-3  rounded-lg my-3')}>
              <Text
                style={[
                  tailwind('text-center uppercase text-base'),
                  styles.SemiBoldFont,
                  styles.ColorSecondary,
                ]}>
                Add to cart
              </Text>
            </Pressable>
          )}
        </View>
      </Modal>

      {/* clear cart modal */}

      <Modal isVisible={clearcartmodal}>
        <View style={tailwind('bg-white p-4 rounded-lg')}>
          <Text style={[tailwind('text-lg pb-5'), styles.MediumFont]}>
            Your cart contains another restaurant do you want to clear?
          </Text>
          <View style={tailwind('flex flex-row justify-between')}>
            <Pressable
              onPress={() => setclearcartmodal(false)}
              style={[tailwind('w-32 border border-gray-300 rounded py-2')]}>
              <Text
                style={[tailwind('text-center uppercase'), styles.MediumFont]}>
                No
              </Text>
            </Pressable>
            <Pressable
              onPress={() => {
                clearCartMethod();
              }}
              style={[tailwind('w-32 rounded bg-black border py-2')]}>
              <Text
                style={[
                  tailwind('text-center text-white uppercase'),
                  styles.MediumFont,
                ]}>
                Yes
              </Text>
            </Pressable>
          </View>
        </View>
      </Modal>
    </View>
  );
}
