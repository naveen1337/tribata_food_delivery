import React from 'react';
import {View, Text, Image} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function OrderTracking(props) {
  return (
    <View elevation={3} style={tailwind('bg-white rounded-lg px-3 py-2')}>
      <View
        style={tailwind(
          'flex flex-row justify-between items-center border-b-2 border-yellow-200',
        )}>
        <View style={tailwind('flex flex-row items-center pb-2 ')}>
          <Text
            style={[
              tailwind('text-base'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            OID
          </Text>
          <Text
            style={[
              tailwind('text-base text-black mx-3'),
              styles.SemiBoldFont,
            ]}>
            {props.id}
          </Text>
        </View>
        <Text style={[tailwind('text-base text-black'), styles.SemiBoldFont]}>
          {props.date}
        </Text>
      </View>
      {props.orderstatus != 'Cancelled' ? (
        <View style={tailwind('flex flex-row justify-between px-3 pt-3')}>
          <View style={tailwind('py-2')}>
            <Image
              resizeMode={'contain'}
              style={tailwind('w-12 h-12')}
              source={require('../../assets/icons/placed.png')}
            />
            <Text
              style={[
                tailwind('text-center text-xs text-black py-1'),
                styles.RegularFont,
              ]}>
              Placed
            </Text>
          </View>

          <View style={tailwind('py-2')}>
            {props.processing ? (
              <Image
                style={tailwind('w-12 h-12')}
                source={require('../../assets/icons/placed.png')}
              />
            ) : (
              <Image
                resizeMode={'contain'}
                style={tailwind('w-12 h-12')}
                source={require('../../assets/icons/packed.png')}
              />
            )}

            <Text
              style={[
                tailwind('text-center text-xs text-black py-1'),
                styles.RegularFont,
              ]}>
              Process
            </Text>
          </View>

          <View style={tailwind('py-2')}>
            {props.packed ? (
              <Image
                style={tailwind('w-12 h-12')}
                source={require('../../assets/icons/placed.png')}
              />
            ) : (
              <Image
                style={tailwind('w-12 h-12')}
                source={require('../../assets/icons/ontheway.png')}
              />
            )}

            <Text
              style={[
                tailwind('text-center text-xs text-black py-1'),
                styles.RegularFont,
              ]}>
              Pick up
            </Text>
          </View>

          <View style={tailwind('py-2  px-1')}>
            {props.delivered ? (
              <Image
                style={tailwind('w-12 h-12')}
                source={require('../../assets/icons/placed.png')}
              />
            ) : (
              <Image
                style={tailwind('w-12 h-12')}
                source={require('../../assets/icons/delivered.png')}
              />
            )}

            <Text
              style={[
                tailwind('text-center text-black text-xs py-1'),
                styles.RegularFont,
              ]}>
              Delivered
            </Text>
          </View>
        </View>
      ) : (
        <View style={tailwind('flex flex-row items-center py-4')}>
          <Image
            style={tailwind('w-4 h-4')}
            source={require('../../assets/icons/fail.png')}
          />
          <Text style={[tailwind('text-red-600 px-1'), styles.MediumFont]}>
            Cancelled
          </Text>
        </View>
      )}
    </View>
  );
}
