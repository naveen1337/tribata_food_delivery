// used in Cart Screen

import React from 'react';
import {View, Text} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function PricingCard(props) {
  return (
    <View elevation={3} style={tailwind('bg-white p-4 mx-3 rounded-lg')}>
      <View style={tailwind('flex flex-row justify-between py-1')}>
        <Text
          style={[tailwind('text-sm text-indigo-700'), styles.SemiBoldFont]}>
          Sub Total
        </Text>
        <Text
          style={[tailwind('text-sm text-indigo-700'), styles.SemiBoldFont]}>
          ₹ {props.itemstotal}
        </Text>
      </View>
      {props.hasdelivery ? (
        <View style={tailwind('flex flex-row justify-between py-1')}>
          <Text
            style={[tailwind('text-sm text-gray-600'), styles.SemiBoldFont]}>
            Delivery Charge
          </Text>
          <Text
            style={[tailwind('text-sm text-gray-600'), styles.SemiBoldFont]}>
            ₹ {props.delivery}
          </Text>
        </View>
      ) : null}

      <View style={tailwind('flex flex-row justify-between py-1')}>
        <Text style={[tailwind('text-sm text-blue-700'), styles.SemiBoldFont]}>
          Packing Charge
        </Text>
        <Text style={[tailwind('text-sm text-blue-700'), styles.SemiBoldFont]}>
          ₹ {props.packing}
        </Text>
      </View>

      {props.hascoupon ? (
        <View style={tailwind('flex flex-row justify-between py-1')}>
          <Text
            style={[tailwind('text-sm text-green-600'), styles.SemiBoldFont]}>
            Coupon code offer
          </Text>
          <Text
            style={[tailwind('text-sm text-green-600'), styles.SemiBoldFont]}>
            -₹ {props.coupon}
          </Text>
        </View>
      ) : null}

      <View style={tailwind('flex flex-row justify-between py-1')}>
        <Text style={[tailwind('text-sm text-gray-500'), styles.SemiBoldFont]}>
          Tax
        </Text>
        <Text style={[tailwind('text-sm text-gray-500 '), styles.SemiBoldFont]}>
          ₹ {props.tax}
        </Text>
      </View>
    </View>
  );
}
