import React, {useState, useEffect} from 'react';
import {View, Text, Image, Pressable, addons} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';

export default function CartButton(props) {
  const cartState = useSelector((state) => state.user.cart);
  // console.log("d",cartState)
  const [total, settotal] = useState(0);

  useEffect(() => {
    let total = 0;
    cartState.forEach((item, i) => {
      // console.log(item);
      if (item.addons.length > 0) {
        let addontotal = 0;
        item.addons.forEach((addonitem) => {
          addontotal += parseInt(addonitem.price) * item.quantity;
        });
        total += addontotal;
        // console.log('Addontot', addontotal);
      }
      total += parseInt(item.price) * item.quantity;
      console.log('From Cart Button Total', total);
    });
    settotal(total);
  }, [cartState]);

  return (
    <View>
      {cartState.length > 0 ? (
        <View
          style={[
            tailwind(
              'rounded-lg mx-4 px-2 py-3 flex flex-row items-center justify-between',
            ),
            styles.BackgroundPrimary,
          ]}>
          <View style={tailwind('flex flex-row')}>
            <Text
              style={[tailwind('text-white text-base'), styles.RegularFont]}>
              {cartState.length} Item
            </Text>
            <Text
              style={[
                tailwind('text-white px-6 text-base'),
                styles.RegularFont,
              ]}>
              Rs. {total}
            </Text>
          </View>
          <Pressable
            onPress={() => props.navigation.navigate('OrderStack')}
            style={tailwind(
              'border border-yellow-500 py-1 px-3  rounded flex flex-row items-center',
            )}>
            <Text style={[tailwind('text-white px-2'), styles.SemiBoldFont]}>
              View Cart
            </Text>
            <Image
              resizeMode={'contain'}
              source={require('../../assets/icons/cart.png')}
              style={tailwind('w-6 h-6')}
            />
          </Pressable>
        </View>
      ) : null}
    </View>
  );
}
