import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  Pressable,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

export default function RestaurantCard(props) {
  const navigation = useNavigation();

  return (
    <Pressable
      android_ripple={{color: '#7f8c8d', borderless: false}}
      onPress={() =>
        navigation.navigate('RestaurantScreen', {hotel_id: props.id})
      }
      elevation={3}
      style={[
        tailwind('flex flex-row rounded-xl bg-white  m-2'),
        {marginLeft: 45},
      ]}>
      <View
        style={{
          borderRadius: 8,
          overflow: 'hidden',
          borderWidth: 1,
          borderColor: '#CBA960',
          width: 74,
          height: 74,
          position: 'relative',
          top: 12,
          right: 35,
          backgroundColor: '#ffffff',
        }}>
        <Image
          resizeMode={'contain'}
          style={[tailwind(''), {width: '100%', height: '100%'}]}
          source={{uri: props.image}}
        />
      </View>
      <View
        style={{
          flexGrow: 1,
          position: 'relative',
          right: 25,
          width: Dimensions.get('window').width,
          paddingVertical: 10,
        }}>
        <Text style={[styles.SemiBoldFont, tailwind('text-base')]}>
          {props.name}
        </Text>
        <Text
          style={[
            styles.RegularFont,
            styles.ColorSecondary,
            tailwind('text-xs pb-1'),
          ]}>
          {props.special}
        </Text>
        <View
          style={[
            tailwind(''),
            {
              width: Dimensions.get('window').width - 120,
              borderWidth: 1,
              borderBottomColor: 'transparent',
              borderLeftColor: 'transparent',
              borderRightColor: 'transparent',
              borderTopColor: '#00000029',
            },
          ]}>
          {props.showdistance ? (
            <View
              style={tailwind(
                'flex flex-row justify-between items-center py-2',
              )}>
              <View style={tailwind('flex flex-row items-center')}>
                <Icon name="star" size={13} color="#CBA960" />
                <Text style={[tailwind('text-xs px-1'), styles.MediumFont]}>
                  {props.rating}
                </Text>
              </View>
              <Icon name="ellipse" size={7} color="#CBA960" />
              <Text style={[tailwind('text-xs'), styles.RegularFont]}>
                {props.city}
              </Text>
              <Icon name="ellipse" size={7} color="#CBA960" />
              <Text style={[tailwind('text-xs'), styles.RegularFont]}>
                {props.distance}
              </Text>
            </View>
          ) : null}

          {props.hasoffer ? (
            <View
              style={[
                tailwind('px-1 my-1 flex flex-row items-center'),
                {
                  width: Dimensions.get('window').width - 150,
                  borderRadius: 6,
                  borderWidth: 1.3,
                  borderStyle: 'dashed',
                  borderColor: '#CBA960',
                },
              ]}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-5 h-5')}
                source={require('../../assets/icons/discount.png')}
              />
              <Text
                style={[styles.MediumFont, {fontSize: 13, paddingVertical: 2}]}>
                {props.offer} | {props.offercode}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
    </Pressable>
  );
}
