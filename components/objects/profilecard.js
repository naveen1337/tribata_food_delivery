import React from 'react';
import {View, Text, Pressable, Image} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export default function Profilecard(props) {
  return (
    <View style={tailwind('px-4')}>
      <View
        style={tailwind('flex flex-row justify-between items-center py-2 ')}>
        <Text
          style={[tailwind('text-lg text-yellow-100'), styles.SemiBoldFont]}>
          {props.name}
        </Text>
        <Pressable
          onPress={() => {
            props.navigation.navigate('ProfileEditScreen', {
              name: props.name,
              mail: props.mail,
              mobilenumber: props.mobilenumber,
            });
          }}>
          <Text
            style={[
              tailwind('text-sm uppercase'),
              styles.RegularFont,
              styles.ColorSecondary,
            ]}>
            Edit Profile
          </Text>
        </Pressable>
      </View>
      <Text style={[tailwind('text-sm text-gray-400'), styles.RegularFont]}>
        {props.mail}
      </Text>
      <View style={tailwind('flex flex-row pt-3')}>
        <Text style={[tailwind('text-sm text-gray-400'), styles.SemiBoldFont]}>
          {props.mobilenumber}
        </Text>
        <Text
          style={[
            tailwind('text-sm text-gray-400 uppercase px-4 '),
            styles.RegularFont,
            styles.ColorSecondary,
          ]}>
          verified
        </Text>
      </View>
      <Pressable
        onPress={() =>
          props.navigation.navigate('QRcodeScreen', {
            qrcode: props.qrcode,
            name: props.name,
            mail: props.mail,
            mobilenumber: props.mobilenumber,
          })
        }
        style={tailwind('flex flex-row pt-3 items-center')}>
        <Image
          style={tailwind('w-6 h-6')}
          resizeMode={'contain'}
          source={require('../../assets/images/qr_scanner.png')}
        />
        <Text
          style={[
            tailwind('px-3 text-base '),
            styles.SemiBoldFont,
            styles.ColorSecondary,
          ]}>
          Show QR Code
        </Text>
      </Pressable>
    </View>
  );
}

// Profile Edit Screen always get mobilenumber route params
