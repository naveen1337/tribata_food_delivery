import React from 'react';
import {View, Text, Image, Pressable, StyleSheet} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';

export default function HomeTopSeller(props) {
  return (
    <Pressable
      onPress={() =>
        props.navigation.navigate('RestaurantScreen', {
          hotel_id: props.id,
        })
      }>
      <LinearGradient
        colors={['#000000', '#2E2E2E']}
        style={[tailwind('mx-1 px-2 py-2 rounded-2xl')]}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-24 h-24 mx-1 rounded-2xl border')}
          source={{uri: props.image}}
        />
        <View style={[tailwind('flex flex-col flex-grow justify-between')]}>
          <Text
            numberOfLines={1}
            style={[
              tailwind('w-24 px-2 py-2 text-sm'),
              styles.ColorSecondary,
              styles.MediumFont,
              mystyles.textheight,
            ]}>
            {props.name}
          </Text>
          <View style={[tailwind('flex flex-row items-center px-2')]}>
            <Icon name="star" size={15} color="#CBA960" />
            <Text
              style={[
                tailwind('mx-1 text-xs text-gray-400'),
                styles.RegularFont,
              ]}>
              {props.rating}
            </Text>
          </View>
        </View>
      </LinearGradient>
    </Pressable>
  );
}

const mystyles = StyleSheet.create({
  textheight: {
    height: 50,
  },
});
