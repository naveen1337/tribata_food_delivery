import React from 'react'
import { View, Text } from 'react-native'
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function blueprint() {
    return (
        <View style={tailwind('border')}>
            <Text>Hello World</Text>
        </View>
    )
}
