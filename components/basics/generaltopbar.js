import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function generaltopbar(props) {
  return (
    <Pressable
      onPress={() => {
        props.navigation.goBack();
      }}
      style={[
        tailwind('flex bg-black pr-3 flex-row items-center justify-between'),
        {height: 56},
      ]}>
      <Image
        style={tailwind('w-8 h-8 ml-1')}
        source={require('../../assets/icons/back.png')}
      />
      <Text style={[tailwind('text-base text-white'), styles.RegularFont]}>
        {props.name}
      </Text>
    </Pressable>
  );
}
