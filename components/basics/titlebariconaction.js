import React from 'react';
import {View, Text, Pressable, Image} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export default function TitleBarIconAction(props) {
  return (
    <View style={tailwind('flex flex-row px-3 items-center justify-between')}>
      <View style={tailwind('flex flex-row items-center')}>
        {props.iconname === 'offer' ? (
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-6')}
            source={require('../../assets/icons/discount.png')}
          />
        ) : null}

        {props.iconname === 'up' ? (
          <View style={[tailwind('bg-black rounded-full'), {padding: 3}]}>
            <Image
              resizeMode={'contain'}
              style={[tailwind('w-4 h-4 '), {tintColor: 'white'}]}
              source={require('../../assets/icons/ic_upward.png')}
            />
          </View>
        ) : null}

        {props.iconname === 'location' ? (
          <Icon name="location" size={20} color="black" />
        ) : null}

        <Text style={[tailwind('text-base px-2'), styles.MediumFont]}>
          {props.title}
        </Text>
      </View>
      <Pressable
        onPress={() =>
          props.navigation.navigate('OrderStack', {
            goto: props.goto,
          })
        }>
        {props.action ? (
          <View
            style={[tailwind('border-2 rounded-2xl'), {paddingVertical: 2}]}>
            <Text
              style={[tailwind('text-xs px-1 uppercase'), styles.MediumFont]}>
              View All
            </Text>
          </View>
        ) : null}
      </Pressable>
    </View>
  );
}
