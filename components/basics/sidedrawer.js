import React, {useState, useEffect} from 'react';
import {ScrollView, View, Text, Pressable, Dimensions} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector, useDispatch} from 'react-redux';
import {WalletBox} from '../';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNExitApp from 'react-native-exit-app';
import jwt_decode from 'jwt-decode';
import {save_user} from '../../actions/appActions';
import {clearCart} from '../../actions/userAction';
import Modal from 'react-native-modal';

export default function SideDrawer(props) {
  const dispatch = useDispatch();
  const [modal, setmodal] = useState(false);
  const userState = useSelector((state) => state.app.userinfo);

  const logout = async () => {
    if (userState.login) {
      const remove = await AsyncStorage.removeItem('@isUserLoggedIn');
      if (remove === null) {
        console.log('User Logging Out');
        setmodal(false);
        dispatch(
          save_user({
            login: false,
            mobilenumber: null,
            name: null,
            mail: null,
            qrcode: null,
            verified: null,
            point: null,
            alladdress: [],
            selectedaddress: false,
          }),
          dispatch(clearCart(false)),
          props.navigation.navigate('HomeScreen'),
        );
      } else {
        Alert.alert('Failed', 'Logout Failed');
      }
    } else {
      RNExitApp.exitApp();
    }
  };
  return (
    <View style={tailwind('h-full')}>
      <ScrollView style={styles.BackgroundSecondary}>
        <View style={[tailwind('py-10'), styles.BackgroundPrimary]}>
          <Text
            style={[
              tailwind('py-2 text-lg text-center'),
              styles.ColorSecondary,
              styles.RegularFont,
            ]}>
            Welcome
          </Text>
          {userState.login ? (
            <Text
              style={[
                tailwind('py-1 text-base text-white text-center'),
                styles.SemiBoldFont,
              ]}>
              {userState.name}
            </Text>
          ) : (
            <Text
              style={[
                tailwind('py-1 text-base text-white text-center'),
                styles.SemiBoldFont,
              ]}>
              Tribata
            </Text>
          )}
        </View>

        <View style={[tailwind('my-4 mx-3')]}>
          {userState.login ? (
            <>
              {/* <View>
                <Text
                  style={[
                    tailwind('text-center text-base'),
                    styles.RegularFont,
                    styles.ColorSecondary,
                  ]}>
                  {userState.selectedaddress.address_name}
                </Text>
                <Pressable
                  style={tailwind('bg-black rounded-lg p-2 mx-8 my-3')}>
                  <Text
                    style={[
                      tailwind('text-sm text-center uppercase'),
                      styles.ColorSecondary,
                      styles.MediumFont,
                    ]}>
                    Set Your Location
                  </Text>
                </Pressable>
              </View> */}

              <WalletBox
                amount={jwt_decode(userState.jwt).wallet}
                navigation={props.navigation}
              />

              <Pressable
                elevation={3}
                onPress={() => props.navigation.navigate('OrderHistoryScreen')}
                style={tailwind(
                  'flex flex-row px-2 justify-between py-3 my-4 bg-white rounded-xl',
                )}>
                <Text style={[tailwind('text-base'), styles.RegularFont]}>
                  Order Status
                </Text>
                <Icon
                  name="chevron-forward-outline"
                  size={20}
                  color="#CBA960"
                />
              </Pressable>

              <Pressable
                elevation={3}
                onPress={() => props.navigation.navigate('PastOrdersScreen')}
                style={tailwind(
                  'flex flex-row px-2 justify-between py-3 mb-4 bg-white rounded-xl',
                )}>
                <Text style={[tailwind('text-base'), styles.RegularFont]}>
                  Past Orders
                </Text>
                <Icon
                  name="chevron-forward-outline"
                  size={20}
                  color="#CBA960"
                />
              </Pressable>

              <Pressable
                elevation={3}
                onPress={() => props.navigation.navigate('FavoritesScreen')}
                style={tailwind(
                  'flex flex-row px-2 justify-between py-3 mb-2 bg-white rounded-xl',
                )}>
                <Text style={[tailwind('text-base'), styles.RegularFont]}>
                  Saved Items
                </Text>
                <Icon
                  name="chevron-forward-outline"
                  size={20}
                  color="#CBA960"
                />
              </Pressable>
            </>
          ) : (
            <Pressable
              elevation={3}
              onPress={() => props.navigation.navigate('UserLoginStack')}
              style={tailwind(
                'flex flex-row px-2 justify-between items-center py-3 my-4 bg-white rounded-xl',
              )}>
              <Text
                style={[tailwind('text-base uppercase'), styles.MediumFont]}>
                Login
              </Text>
              <Icon name="chevron-forward-outline" size={20} color="#CBA960" />
            </Pressable>
          )}

          <Pressable
            onPress={() => props.navigation.navigate('WebViewScreen')}
            style={tailwind('py-2')}>
            <Text
              style={[tailwind('text-sm text-gray-500'), styles.MediumFont]}>
              About us
            </Text>
          </Pressable>

          <Pressable
            onPress={() => props.navigation.navigate('WebViewScreen')}
            style={tailwind('py-3')}>
            <Text
              style={[tailwind('text-sm text-gray-500'), styles.MediumFont]}>
              Contact us
            </Text>
          </Pressable>
          <Pressable
            onPress={() => props.navigation.navigate('WebViewScreen')}
            style={tailwind('py-2')}>
            <Text
              style={[tailwind('text-sm text-gray-500'), styles.MediumFont]}>
              Privacy Policy
            </Text>
          </Pressable>
          <Pressable
            onPress={() => props.navigation.navigate('WebViewScreen')}
            style={tailwind('py-2')}>
            <Text
              style={[tailwind('text-sm text-gray-500'), styles.MediumFont]}>
              Terms & Conditions
            </Text>
          </Pressable>

          <Pressable
            onPress={() => props.navigation.navigate('WebViewScreen')}
            style={tailwind('pt-4 pb-2')}>
            <Text
              style={[tailwind('text-sm text-gray-500'), styles.MediumFont]}>
              Whatsapp
            </Text>
          </Pressable>
          <Pressable
            onPress={() => props.navigation.navigate('WebViewScreen')}
            style={tailwind('py-2')}>
            <Text
              style={[tailwind('text-sm text-gray-500'), styles.MediumFont]}>
              Facebook
            </Text>
          </Pressable>
          <Pressable
            onPress={() => props.navigation.navigate('WebViewScreen')}
            style={tailwind('py-2')}>
            <Text
              style={[tailwind('text-sm text-gray-500'), styles.MediumFont]}>
              Instagram
            </Text>
          </Pressable>
        </View>
      </ScrollView>

      {userState.login ? (
        <Pressable
          onPress={() => setmodal(true)}
          style={tailwind('flex flex-row items-center px-3 py-4')}>
          <Icon name="log-out-outline" size={23} color="#e84118" />
          <Text style={[tailwind('text-base px-3'), styles.MediumFont]}>
            Log out
          </Text>
        </Pressable>
      ) : (
        <Pressable
          onPress={() => setmodal(true)}
          style={tailwind('flex flex-row items-center px-3 py-4')}>
          <Icon name="log-out-outline" size={23} color="#e84118" />
          <Text style={[tailwind('text-base px-3'), styles.MediumFont]}>
            Exit
          </Text>
        </Pressable>
      )}
      {modal ? (
        <Modal isVisible={true}>
          <View style={tailwind('bg-white p-4 rounded-lg')}>
            <Text style={[tailwind('text-lg pb-5'), styles.MediumFont]}>
              Are you sure want Exit the App ?{' '}
            </Text>
            <View style={tailwind('flex flex-row justify-between')}>
              <Pressable
                onPress={logout}
                style={[tailwind('w-32 border border-gray-300 rounded py-2')]}>
                <Text
                  style={[
                    tailwind('text-center uppercase'),
                    styles.MediumFont,
                  ]}>
                  Ok
                </Text>
              </Pressable>
              <Pressable
                onPress={() => setmodal(false)}
                style={[tailwind('w-32 rounded bg-black border py-2')]}>
                <Text
                  style={[
                    tailwind('text-center text-white uppercase'),
                    styles.MediumFont,
                  ]}>
                  Cancel
                </Text>
              </Pressable>
            </View>
          </View>
        </Modal>
      ) : null}
    </View>
  );
}
