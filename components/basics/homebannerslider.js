import React, {useState, useEffect} from 'react';
import {View, Text, Dimensions} from 'react-native';
import tailwind from 'tailwind-rn';
import {SliderBox} from 'react-native-image-slider-box';
import {styles} from '../../constants/styles';

// import {fetchHomeBanners} from '../../actions/homeactions';

export default function homebanner(props) {
  let [images, setimages] = React.useState([]);

  useEffect(() => {
    let tempimages = [];
    props.homebanner.forEach((item) => {
      tempimages.push(item.bannerurl);
    });
    setimages(tempimages);
  }, [props.homebanner]);

  let onImageClick = (index) => {
    console.log(props.homebanner[index]);
    // This will cause an error if the restaurnat data not on a top sellers list
    props.navigation.navigate('RestaurantScreen', {
      hotel_id: props.homebanner[index].restaurant_id,
    });
  };
  return (
    <SliderBox
      images={images}
      parentWidth={Dimensions.get('window').width - 30}
      imageLoadingColor="transparent"
      onCurrentImagePressed={(index) => onImageClick(index)}
      dotColor="#CBA960"
      sliderBoxHeight={156}
      inactiveDotColor="#FFEAC0"
      resizeMethod={'resize'}
      resizeMode={'contain'}
      paginationBoxStyle={{
        position: 'relative',
        bottom: 0,
        padding: 0,
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
        paddingVertical: 5,
      }}
      dotStyle={{
        width: 30,
        height: 4,
        //   borderRadius: 5,
        marginHorizontal: 0,
        padding: 0,
        margin: 0,
        backgroundColor: 'red',
      }}
    />
  );
}
