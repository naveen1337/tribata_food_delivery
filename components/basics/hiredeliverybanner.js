import React from 'react';
import {View, Text, Image, Pressable, Dimensions} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import {useSelector} from 'react-redux';
import Snackbar from 'react-native-snackbar';

export default function HireDeliveryBanner(props) {
  const userState = useSelector((state) => state.app.userinfo);

  const navigateme = () => {
    if (userState.login) {
      props.navigation.navigate('HireDeliveryStack');
    } else {
      Snackbar.show({
        numberOfLines: 2,
        text: `Login First`,
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Ok',
          textColor: 'yellow',
          onPress: () => {},
        },
      });
    }
  };

  return (
    <Pressable onPress={navigateme} style={tailwind('')}>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-full h-40')}
        source={require('../../assets/images/deliveryposter.png')}
      />
      <View style={tailwind('absolute right-10 bottom-10 bg-black rounded ')}>
        <Text
          style={[
            tailwind('text-white uppercase px-6 py-2'),
            styles.SemiBoldFont,
            styles.ColorSecondary,
          ]}>
          HIRE DELIVERY BOY
        </Text>
      </View>
    </Pressable>
  );
}
