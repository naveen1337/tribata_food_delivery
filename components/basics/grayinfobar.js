import React from 'react';
import {View, Text} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';

export default function GrayInfoBar(props) {
  return (
    <View style={tailwind('py-2 px-2')}>
      <Text
        style={[tailwind('text-sm'), styles.SemiBoldFont, styles.ColorGray]}>
        {props.name}
      </Text>
    </View>
  );
}
