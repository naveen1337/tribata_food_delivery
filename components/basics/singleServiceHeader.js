import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../../constants/styles';
import Icon from 'react-native-vector-icons/Ionicons';

export default function SingleServiceHeader(props) {
  return (
    <View style={tailwind('bg-black px-3 py-3 flex flex-row items-center')}>
      <Pressable
        onPress={() => {
          props.navigation.goBack();
        }}>
        <Icon
          style={tailwind('')}
          name="chevron-back-outline"
          size={25}
          color="#CBA960"
        />
      </Pressable>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-28 h-10 ml-4')}
        source={require('../../assets/images/mainbrand.png')}
      />
    </View>
  );
}
