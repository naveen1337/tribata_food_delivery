import React from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {useSelector} from 'react-redux';
import Snackbar from 'react-native-snackbar';
import {styles} from '../../constants/styles';

export default function hometopbar(props) {
  const userState = useSelector((state) => state.app.userinfo);

  const navigatetoaddress = () => {
    if (userState.login) {
      props.navigation.navigate('AddressEntryScreen');
    } else {
      Snackbar.show({
        numberOfLines: 2,
        text: `Login First`,
        duration: Snackbar.LENGTH_LONG,
        action: {
          text: 'Ok',
          textColor: 'yellow',
          onPress: () => {},
        },
      });
    }
  };

  return (
    <View
      style={[
        tailwind('border flex px-2 py-2 flex-row items-center justify-between'),
        styles.BackgroundPrimary,
      ]}>
      <Pressable
        onPress={() => props.navigation.openDrawer()}
        style={tailwind('flex flex-row items-center ')}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-7 h-7')}
          source={require('../../assets/icons/menu.png')}
        />
        {/* <Image
          resizeMode={'contain'}
          style={tailwind('w-28 h-20 ml-4')}
          source={require('../../assets/images/mainbrand.png')}
        /> */}
      </Pressable>
      <View>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-28 h-8 ml-4')}
          source={require('../../assets/images/nav_logo.png')}
        />
      </View>
      <View style={tailwind('flex flex-row items-center')}>
        <Pressable onPress={navigatetoaddress}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-8 mx-2')}
            source={require('../../assets/icons/location.png')}
          />
        </Pressable>
        <Pressable onPress={() => props.navigation.navigate('CartScreen')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-8 mx-2')}
            source={require('../../assets/icons/cart.png')}
          />
        </Pressable>
      </View>
    </View>
  );
}
