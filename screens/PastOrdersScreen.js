import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  RefreshControl,
  Pressable,
  Image,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useSelector} from 'react-redux';
import {getAllOrders} from '../utils/userRemoteService';
import Spinner from 'react-native-loading-spinner-overlay';

import {GeneralTopBar} from '../components';
import {DrawerItemList} from '@react-navigation/drawer';

export default function OrderHistoryScreen(props) {
  const userInfoState = useSelector((state) => state.app.userinfo);

  const [allorders, setallorders] = useState([]);
  const [loading, setloading] = useState(true);
  // console.log(userInfoState);

  useEffect(() => {
    (async () => {
      const data = await getAllOrders({
        mobile_number: userInfoState.mobilenumber,
      });
      setloading(false);
      if (data) {
        setallorders(data);
      }
    })();
  }, []);

  return (
    <ScrollView style={[styles.BackgroundSecondary]}>
      <GeneralTopBar name="Order Details" navigation={props.navigation} />
      <Spinner visible={loading} />
      {allorders.map((item) => {
        return item.order_status === 'Cancelled' ||
          item.order_status === 'Delivered' ? (
          <Pressable
            elevation={3}
            onPress={() =>
              props.navigation.navigate('OrderTrackingScreen', {
                orderid: item.orderid,
              })
            }
            key={item.orderid}
            style={tailwind('bg-white rounded-lg px-3 py-2 mx-3 my-3')}>
            <View
              style={tailwind(
                'flex flex-row justify-between items-center border-b-2 pb-2 border-yellow-200',
              )}>
              <View
                style={tailwind('flex flex-row items-center items-center  ')}>
                <Text
                  style={[
                    tailwind('text-base'),
                    styles.SemiBoldFont,
                    styles.ColorSecondary,
                  ]}>
                  OID
                </Text>
                <Text
                  style={[
                    tailwind('text-base text-black mx-3'),
                    styles.SemiBoldFont,
                  ]}>
                  {item.orderid}
                </Text>
              </View>
              <Text
                style={[tailwind('text-base text-black'), styles.SemiBoldFont]}>
                {item.booked_date}
              </Text>
            </View>
            <View style={tailwind('flex flex-row justify-between py-3')}>
              <Text
                style={[tailwind('text-lg text-black'), styles.SemiBoldFont]}>
                ₹ {item.total_amount}
              </Text>
              {item.order_status === 'Cancelled' ? (
                <View style={tailwind('flex flex-row items-center')}>
                  <Image
                    style={tailwind('w-3 h-3')}
                    source={require('../assets/icons/fail.png')}
                  />
                  <Text
                    style={[
                      tailwind('text-xs text-red-600 px-1'),
                      styles.MediumFont,
                    ]}>
                    {item.order_status}
                  </Text>
                </View>
              ) : (
                <View style={tailwind('flex flex-row items-center')}>
                  <Image
                    style={tailwind('w-3 h-3')}
                    source={require('../assets/icons/success.png')}
                  />
                  <Text
                    style={[
                      tailwind('text-xs text-black px-1'),
                      styles.MediumFont,
                    ]}>
                    {item.order_status}
                  </Text>
                </View>
              )}
            </View>
          </Pressable>
        ) : null;
      })}
    </ScrollView>
  );
}
