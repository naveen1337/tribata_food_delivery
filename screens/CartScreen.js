import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Image, Pressable} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {save_coupon} from '../actions/userAction';
import {
  getDeliveryChargeRemote,
  CheckDeliveryAddressRemote,
} from '../utils/userRemoteService';
import {insertCartAction, removeCartAction} from '../actions/userAction';
import Spinner from 'react-native-loading-spinner-overlay';
import {
  GeneralTopBar,
  CartProduct,
  CartRestaurantInfo,
  AddressBox,
  PricingCard,
  CheckOutButton,
  Couponbox,
} from '../components';
import {user} from '../reducers/userReducer';

export default function CartScreen(props) {
  const dispatch = useDispatch();
  const [spinnerloading, setSpinnerloading] = useState(true);

  const [packing, setPacking] = useState(0);
  const [delivery, setDelivery] = useState(0);
  const [itemstotal, setitemstotal] = useState(0);
  const [tax, settax] = useState(0);
  const [total, settotal] = useState(0);
  const [mincheckout, setmincheckout] = useState(0);
  const [available, setavailable] = useState(true);

  // Redux State
  const CartState = useSelector((state) => state.user.cart);
  const userState = useSelector((state) => state.app.userinfo);
  const SelectedStore = useSelector((state) => state.user.restaurant);
  const SelectedCouponState = useSelector((state) => state.user.coupon);
  const locationState = useSelector((state) => state.app.location);

  const totalprice = async () => {
    let total = 0;
    CartState.forEach((item, i) => {
      if (item.addons.length > 0) {
        let addontotal = 0;
        item.addons.forEach((addonitem) => {
          addontotal += parseInt(addonitem.price) * item.quantity;
        });
        total += addontotal;
        console.log('Addontotal', addontotal);
      }
      total += parseInt(item.price) * item.quantity;
      console.log('Total', total);
      setitemstotal(total);
    });
    // Bug in that line, need to checked
    if (SelectedCouponState) {
      // console.log(SelectedCouponState);
      let reduction = total * (SelectedCouponState.percentage / 100);
      if (reduction > SelectedCouponState.promohighprice) {
        total -= SelectedCouponState.reduce;
      } else {
        dispatch(save_coupon(false));
      }
    }
    if (userState.selectedaddress) {
      const charge = await getDeliveryChargeRemote({
        user_latitude: userState.selectedaddress.latitude.toString(),
        user_longitude: userState.selectedaddress.longitude.toString(),
        hotelid: SelectedStore.id.toString(),
        total: total.toString(),
        pre_order: '0',
      });

      if (charge) {
        console.log('RESPONSE: ', charge);
        setmincheckout(charge.min);
        setPacking(charge.packaging_charge);
        setDelivery(charge.delivery_charge);
        total += parseInt(charge.packaging_charge);
        settotal(Math.round(total + total * (charge.tax / 100)));
        settax(Math.round(total * (charge.tax / 100)));
      }
    } else {
      const charge = await getDeliveryChargeRemote({
        user_latitude: locationState.latitude.toString(),
        user_longitude: locationState.longitude.toString(),
        hotelid: SelectedStore.id.toString(),
        total: total.toString(),
        pre_order: '0',
      });

      if (charge) {
        console.log('RESPONSE: ', charge);
        setmincheckout(charge.min);
        setPacking(charge.packaging_charge);
        setDelivery(charge.delivery_charge);
        total += parseInt(charge.packaging_charge);
        settotal(Math.round(total + total * (charge.tax / 100)));
        settax(Math.round(total * (charge.tax / 100)));
      }
    }
  };

  // When CartState and Userstate is Mutated
  useEffect(() => {
    if (CartState.length > 0) {
      totalprice();
    }
  }, [CartState, SelectedCouponState, userState]);

  useEffect(() => {
    (async () => {
      if (CartState.length > 0) {
        const isaddressavailable = await CheckDeliveryAddressRemote({
          latitude: userState.selectedaddress.latitude,
          longitude: userState.selectedaddress.longitude,
          branch_id: SelectedStore.id,
        });
        if (isaddressavailable) {
          setavailable(true);
        } else {
          setavailable(false);
        }
      }
    })();
  }, [userState]);

  const addtoCart = (e) => {
    // console.log('Component', e);
    let item = CartState.find((item) => item.variation_id === e.id);
    if (item) {
      let newobj = {
        ...item,
      };
      dispatch(insertCartAction(newobj));
    }
  };

  const removefromCart = (e) => {
    dispatch(removeCartAction(e));
  };

  return (
    <View style={tailwind('h-full')}>
      <GeneralTopBar name="Cart" navigation={props.navigation} />
      {CartState.length > 0 ? (
        <>
          {/* <Spinner visible={spinnerloading} /> */}
          <ScrollView style={[styles.BackgroundSecondary]}>
            <Text
              style={[
                tailwind('text-base text-gray-500 p-2'),
                styles.SemiBoldFont,
              ]}>
              Shop Details
            </Text>
            {/* Information about the restaurant */}
            <View>
              <CartRestaurantInfo
                key={SelectedStore.id}
                name={SelectedStore.name}
                image={SelectedStore.image}
                special={SelectedStore.special}
              />
            </View>

            <Text
              style={[
                tailwind('text-base text-gray-500 p-2'),
                styles.SemiBoldFont,
              ]}>
              Items Added
            </Text>

            {CartState.map((item) => {
              // console.log(item);
              return (
                <CartProduct
                  key={item.variation_id}
                  id={item.variation_id}
                  name={item.name}
                  quantity={item.quantity}
                  price={item.price}
                  addons={item.addons}
                  hasvariation={item.variation != false}
                  variation={item.variation}
                  addtoCart={addtoCart}
                  removefromCart={removefromCart}
                />
              );
            })}

            {/* Coupon box */}
            <Couponbox
              store={SelectedStore.id}
              coupon={SelectedCouponState}
              navigation={props.navigation}
            />

            {userState.selectedaddress ? (
              <View>
                <Text
                  style={[
                    tailwind('text-base text-gray-500 p-2'),
                    styles.SemiBoldFont,
                  ]}>
                  Address
                </Text>
                <AddressBox
                  navigation={props.navigation}
                  address={userState.selectedaddress}
                />
              </View>
            ) : null}

            <Text
              style={[
                tailwind('text-base text-gray-500 p-2'),
                styles.SemiBoldFont,
              ]}>
              Billing
            </Text>

            {/* Pricing Card */}
            <PricingCard
              itemstotal={itemstotal}
              total={total}
              hasdelivery={true}
              hascoupon={SelectedCouponState}
              coupon={SelectedCouponState.reduce}
              delivery={delivery}
              packing={packing}
              tax={tax}
            />

            <View style={tailwind('pb-20')}></View>
          </ScrollView>
          <View style={tailwind('absolute bottom-2 w-full')}>
            <CheckOutButton
              navigation={props.navigation}
              total={total}
              packing={packing}
              delivery={delivery}
              tax={tax}
              mincheckout={mincheckout}
              available={available}
            />
          </View>
        </>
      ) : (
        <View
          style={[
            tailwind('h-full flex flex-col justify-between items-center'),
            styles.BackgroundSecondary,
          ]}>
          <Text></Text>
          <View style={tailwind('flex flex-col justify-center items-center')}>
            <Image
              source={require('../assets/images/no_shopping_cart.png')}
              style={tailwind('w-16 h-16')}
              resizeMode={'contain'}
            />
            <Text
              style={[
                tailwind('text-base py-6 text-center'),
                styles.MediumFont,
              ]}>
              You have No Item in a bag
            </Text>
          </View>
          <View style={tailwind('relative w-full bottom-16')}>
            <Pressable
              onPress={() => props.navigation.navigate('HomeScreen')}
              style={tailwind('py-3 mx-6   rounded-lg bg-black ')}>
              <Text
                style={[
                  tailwind('uppercase text-center text-base'),
                  styles.ColorSecondary,
                  styles.SemiBoldFont,
                ]}>
                {' '}
                Add Items
              </Text>
            </Pressable>
          </View>
        </View>
      )}
    </View>
  );
}
