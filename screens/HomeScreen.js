import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  Dimensions,
  Pressable,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Spinner from 'react-native-loading-spinner-overlay';
// import ActionButton from 'react-native-action-button';
import Icon from 'react-native-vector-icons/Ionicons';

import {
  fetchServices,
  fetchTopSellers,
  fetchPromoRestaurants,
  fetchTopHotelsAction,
} from '../actions/homeActions';
import {fetchHomeBanners} from '../utils/homeRemoteService';
import {hasAnyLiveOrderRemote} from '../utils/userRemoteService';

import {
  HomeTopBar,
  Services,
  HomeImageSlider,
  TitleBarIconAction,
  HomeTopseller,
  DoubleScroll,
  RestaurantCard,
  HireDelivery,
  OrderProcessingBar,
  SmallRestaurantCard,
} from '../components';

export default function HomeScreen(props) {
  const dispatch = useDispatch();

  // From Redux
  const locationState = useSelector((state) => state.app.location);
  const userState = useSelector((state) => state.app.userinfo);

  const servicesState = useSelector((state) => state.home.services);
  const topSellerState = useSelector((state) => state.home.topSellers);
  const promoRestaurantState = useSelector((state) => state.home.allPromo);

  // Local State
  const [homebanner, sethomebanner] = useState([]);
  const [loading, setloading] = useState(false);
  const [liveorder, setliveorder] = useState(false);

  let fetchInitialCalls = async () => {
    setloading(true);
    if (servicesState.length === 0) {
      dispatch(fetchServices(locationState));
    }
    if (homebanner.length === 0) {
      let banners = await fetchHomeBanners(locationState);
      if (banners) {
        sethomebanner(banners);
      }
    }
    if (topSellerState.length === 0) {
      dispatch(fetchTopSellers(locationState));
    }
    if (promoRestaurantState.length === 0) {
      dispatch(fetchPromoRestaurants(locationState));
    }
    dispatch(fetchTopHotelsAction({...locationState, service_id: 1}));
    setloading(false);
    if (userState.login) {
      const result = await hasAnyLiveOrderRemote({
        mobile_number: userState.mobilenumber,
      });
      if (result) {
        setliveorder(true);
      }
    }
  };

  useEffect(() => {
    (async () => {
      await fetchInitialCalls();
    })();
  }, []);

  return (
    <View style={tailwind('h-full')}>
      <ScrollView style={[styles.BackgroundSecondary]}>
        <HomeTopBar navigation={props.navigation} />

        {/* Spinner */}
        {/* <Spinner visible={loading} /> */}

        <LinearGradient
          style={[
            tailwind('pt-3 pb-16'),
            {borderBottomRightRadius: 10, borderBottomLeftRadius: 10},
          ]}
          colors={['#000000', '#2E2E2E']}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {servicesState.map((item) => {
              // console.log(item)
              return (
                <Services
                  key={item.id}
                  id={item.id}
                  image={item.service_image}
                  name={item.service_name}
                  service_status={item.service_status}
                  navigation={props.navigation}
                />
              );
            })}
          </ScrollView>
        </LinearGradient>
        {homebanner.length === 0 ? (
          <View
            style={tailwind('relative bottom-16 flex flex-row justify-center')}>
            <Image
              resize_mode={'contain'}
              style={{height: 156, width: Dimensions.get('window').width - 30}}
              source={require('../assets/images/loading_sliding.png')}
            />
          </View>
        ) : (
          <View
            style={tailwind('relative bottom-16 flex flex-row justify-center')}>
            <HomeImageSlider
              homebanner={homebanner}
              navigation={props.navigation}
            />
          </View>
        )}
        <View style={tailwind('relative bottom-14')}>
          <View
            style={tailwind(
              'flex flex-row px-3 items-center py-2 justify-between',
            )}>
            <View style={tailwind('flex flex-row')}>
              <View style={[tailwind('bg-black rounded-full'), {padding: 3}]}>
                <Image
                  resizeMode={'contain'}
                  style={[tailwind('w-4 h-4 '), {tintColor: 'white'}]}
                  source={require('../assets/icons/ic_upward.png')}
                />
              </View>
              <Text style={[tailwind('text-base px-2'), styles.MediumFont]}>
                Top Brands
              </Text>
            </View>
            <Pressable
              onPress={() => {
                props.navigation.navigate('TopRestaurantScreen');
              }}
              style={[tailwind('border-2 rounded-2xl'), {paddingVertical: 2}]}>
              <Text
                style={[tailwind('text-xs px-1 uppercase'), styles.MediumFont]}>
                View All
              </Text>
            </Pressable>
          </View>

          {/* Top Sellers */}
          <ScrollView
            style={tailwind('pt-4 py-2 mr-2')}
            horizontal={true}
            showsHorizontalScrollIndicator={false}>
            {topSellerState.map((item) => {
              return (
                <HomeTopseller
                  key={item.id}
                  id={item.id}
                  navigation={props.navigation}
                  city_id={item.city_id}
                  image={item.Restaurant_image}
                  name={item.Restaurant_name}
                  rating={item.Restaurant_rating}
                />
              );
            })}
          </ScrollView>

          {/* hire delivery Boy */}

          <HireDelivery navigation={props.navigation} />

          {/* Coupon Offers Section */}
          <View style={tailwind('py-4')}>
            <TitleBarIconAction
              navigation={props.navigation}
              iconname="offer"
              title="Coupon Offers"
              action={true}
              goto="promo"
            />
          </View>

          <View style={tailwind('mx-3')}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              {promoRestaurantState.map((item) => {
                // console.log(item);
                return (
                  <SmallRestaurantCard
                    key={item.branch_name}
                    id={item.restaurant_id}
                    navigation={props.navigation}
                    city_id={item.city_id}
                    image={item.branch_image}
                    name={item.branch_name}
                    rating={item.Restaurant_rating}
                    special={item.branch_special}
                    distance={item.distance}
                    intime={item.Restaurant_intime}
                    outtime={item.Restaurant_outtime}
                    status={item.Restaurant_status === '0'}
                    hasoffer={true}
                    offer={item.sno}
                    navigation={props.navigation}
                  />
                );
              })}
            </ScrollView>
          </View>

          {/* nearby Hotels Section */}
          <View style={tailwind('py-4')}>
            <TitleBarIconAction
              navigation={props.navigation}
              iconname="location"
              title="Nearby Restaurnats"
              action={true}
              goto="nearby"
            />
          </View>

          <View style={tailwind('mr-2')}>
            <ScrollView
              horizontal={false}
              showsHorizontalScrollIndicator={false}>
              {topSellerState.map((item) => {
                return (
                  <RestaurantCard
                    id={item.id}
                    key={item.id}
                    navigation={props.navigation}
                    city_id={item.city_id}
                    image={item.Restaurant_image}
                    name={item.Restaurant_name}
                    rating={item.Restaurant_rating}
                    special={item.special}
                    showdistance={true}
                    distance={item.distance}
                    intime={item.Restaurant_intime}
                    outtime={item.Restaurant_outtime}
                    city={item.city_name}
                    status={item.Restaurant_status === '0'}
                    hasoffer={false}
                  />
                );
              })}
            </ScrollView>
          </View>
        </View>

        <View style={tailwind('mb-1')}></View>
      </ScrollView>
      {liveorder ? (
        <View>
          <OrderProcessingBar navigation={props.navigation} />
        </View>
      ) : null}
    </View>
  );
}
