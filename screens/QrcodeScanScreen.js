import React from 'react';
import {StyleSheet, View, Pressable, Text} from 'react-native';
import {useIsFocused} from '@react-navigation/core';
import {useDispatch} from 'react-redux';
import {saveQRCode} from '../actions/userAction';
import {styles} from '../constants/styles';
import {CommonActions} from '@react-navigation/native';
import Snackbar from 'react-native-snackbar';


import QRCodeScanner from 'react-native-qrcode-scanner';
import tailwind from 'tailwind-rn';

export default function QRScan(props) {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();

  const onSuccess = (e) => {
    try {
      let data = JSON.parse(e.data);
      console.log(data.mobile_number);
      dispatch(saveQRCode(data.mobile_number));
      props.navigation.goBack();
    } catch {
      Snackbar.show({
        numberOfLines: 2,
        text: `Error While QR Code Scanning`,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: 'red',
        action: {
          text: 'Ok',
          textColor: 'black',
          onPress: () => {},
        },
      });
      dispatch(saveQRCode(false));
      props.navigation.goBack();
    }
  };

  return isFocused ? (
    <View>
      <View style={tailwind('')}>
        <Text
          style={[
            tailwind('text-center text-base py-4 px-3'),
            styles.RegularFont,
          ]}>
          Point your phone to this QR Code to capture the Phone Number
        </Text>
        <QRCodeScanner
          cameraProps={{ratio: '8:8'}}
          topViewStyle={{backgroundColor: '#fff', zIndex: 1000}}
          bottomViewStyle={{backgroundColor: '#fff'}}
          style={tailwind()}
          onRead={(e) => onSuccess(e)}
        />
      </View>
    </View>
  ) : null;
}
