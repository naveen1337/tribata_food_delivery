import React, {useState} from 'react';
import {View, Text, Image, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {WebView} from 'react-native-webview';

export default function WebViewScreen(props) {
  return (
    <View style={{width: '100%', height: '100%'}}>
      <View
        style={tailwind(
          'bg-black py-3 items-center flex flex-row justify-between',
        )}>
        <Pressable
          onPress={() => {
            props.navigation.goBack();
          }}>
          <Image
            style={tailwind('w-10 h-10')}
            resizeMode={'contain'}
            source={require('../assets/icons/back.png')}
          />
        </Pressable>

        <Image
          style={tailwind('w-10 h-10')}
          resizeMode={'contain'}
          source={require('../assets/images/nav_logo.png')}
        />
        <Text></Text>
      </View>

      <WebView source={{uri: 'https://reactjs.org/'}} />
    </View>
  );
}
