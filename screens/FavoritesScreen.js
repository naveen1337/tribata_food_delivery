import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  Pressable,
  StyleSheet,
  ScrollView,
  Dimensions,
} from 'react-native';
import {TabView, SceneMap, TabBar} from 'react-native-tab-view';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useNavigation} from '@react-navigation/native';

import {
  GeneralTopBar,
  RestaurantCard,
  RestaurantProduct,
  CartButton,
} from '../components';

const RestaurantRoute = (props) => (
  <ScrollView style={[tailwind('h-full'), styles.BackgroundSecondary]}>
    {props.restaurant ? (
      props.restaurant.map((item) => {
        return (
          <RestaurantCard
            id={item.id}
            key={item.id}
            navigation={props.navigation}
            city_id={item.city_id}
            image={item.Restaurant_image}
            name={item.Restaurant_name}
            rating={item.Restaurant_rating}
            special={item.special}
            showdistance={true}
            distance={item.distance}
            intime={item.Restaurant_intime}
            outtime={item.Restaurant_outtime}
            city={item.city_name}
            status={item.Restaurant_status === '0'}
          />
        );
      })
    ) : (
      <View
        style={tailwind('flex flex-col flex-grow justify-center items-center')}>
        <Icon name="search" size={40} color="#000000" />
        <Text style={[tailwind('text-base py-2'), styles.MediumFont]}>
          No Restaurant Found
        </Text>
      </View>
    )}
  </ScrollView>
);

const ProductsRoute = (props) => (
  <View style={tailwind('h-full')}>
    <ScrollView style={[styles.BackgroundSecondary]}>
      {props.products ? (
        props.products.map((item) => {
          return (
            <View key={item.id} style={tailwind('mx-3 my-2')}>
              <RestaurantProduct
                id={item.id}
                recom={false}
                veg={item.type === '1'}
                name={item.product_name}
                desc={item.description}
                isfav={item.is_favourite}
                price={item.price}
                cityid={0}
                hotelid={item.restaurant_id}
                hasvariation={item.variation === 'true'}
                variation={item.price_variation}
                variation_id={item.variation_id}
                hasaddon={item.addon}
                addon_content={item.addon_content}
                status={item.product_status === '1'}
                is_favourite={item.is_favourite === 0}
              />
            </View>
          );
        })
      ) : (
        <View
          style={tailwind(
            'flex flex-col flex-grow justify-center items-center',
          )}>
          <Icon name="search" size={40} color="#000000" />
          <Text style={[tailwind('text-base py-2'), styles.MediumFont]}>
            No Products Found
          </Text>
        </View>
      )}
      <View style={tailwind('pb-20')}></View>
    </ScrollView>
    <View style={tailwind('absolute w-full bottom-5')}>
      <CartButton navigation={props.navigation} />
    </View>
  </View>
);

const initialLayout = {width: Dimensions.get('window').width};

const renderTabBar = (props) => (
  <TabBar
    renderLabel={({route, focused, color}) =>
      focused ? (
        <Text style={[styles.SemiBoldFont, tailwind('text-white')]}>
          {route.title}
        </Text>
      ) : (
        <Text style={[styles.SemiBoldFont, tailwind('text-white')]}>
          {route.title}
        </Text>
      )
    }
    {...props}
    indicatorStyle={{backgroundColor: 'white'}}
    style={{backgroundColor: 'black'}}
    labelStyle={styles.ColorSecondary}
  />
);

export default function TabViewRoot(props) {
  const navigation = useNavigation();
  const FavoritesResState = useSelector((state) => state.app.fav_res);
  const FavoritesProductState = useSelector((state) => state.app.fav_product);
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    {key: 'restaurant', title: 'Restaurant'},
    {key: 'products', title: 'Products'},
  ]);

  const renderScene = ({route}) => {
    switch (route.key) {
      case 'restaurant':
        return (
          <RestaurantRoute
            navigation={navigation}
            restaurant={FavoritesResState}
          />
        );
      case 'products':
        return (
          <ProductsRoute
            navigation={navigation}
            products={FavoritesProductState}
          />
        );
      default:
        return null;
    }
  };

  return (
    <>
      <GeneralTopBar name="Favorites" navigation={props.navigation} />

      <TabView
        renderTabBar={renderTabBar}
        navigationState={{index, routes}}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
      />
    </>
  );
}
