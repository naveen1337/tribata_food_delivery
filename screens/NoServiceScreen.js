import React, {useState, useEffect} from 'react';
import {View, Text, Image, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import {getAvailableCities} from '../utils/userRemoteService';
import {saveLocationCords} from '../actions/appActions';

export default function NoServiceScreen(props) {
  const dispatch = useDispatch();
  // Redux State
  const LocationState = useSelector((state) => state.app.location);
  const [cities, setcities] = useState([]);

  useEffect(() => {
    (async () => {
      let result = await getAvailableCities(LocationState);
      if (result) {
        setcities(result);
      }
    })();
  }, []);

  const updateLocationState = (e) => {
    console.log(e);
    dispatch(saveLocationCords(e));
    props.navigation.navigate('BottomStack');
  };

  return (
    <ScrollView style={[styles.BackgroundSecondary]}>
      <View style={tailwind('flex flex-col mt-10 justify-center items-center')}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-20 h-20')}
          source={require('../assets/images/noservice.png')}
        />
        <Text style={[tailwind('text-center text-xl'), styles.SemiBoldFont]}>
          Location not serviceable
        </Text>
        <Text
          style={[tailwind('text-center text-base py-4'), styles.RegularFont]}>
          Tribata services are not available at this location yet. Please update
          your location.
        </Text>
        <Text style={[tailwind('border-b-2 w-5/12  border-yellow-200')]}></Text>
        <Text
          style={[tailwind('text-center my-3 text-base'), styles.SemiBoldFont]}>
          Available Location
        </Text>

        <View style={tailwind('flex flex-row flex-wrap justify-center px-3')}>
          {cities.map((item) => {
            return (
              <Pressable
                onPress={() => {
                  updateLocationState({
                    latitude: item.latitude,
                    longitude: item.longitude,
                  });
                }}
                key={item.id}
                style={tailwind(' px-4 mx-2 my-3')}>
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-16 h-16')}
                  source={require('../assets/images/noservice.png')}
                />
                <Text
                  style={[tailwind('text-center text-sm'), styles.RegularFont]}>
                  {item.city}
                </Text>
              </Pressable>
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
}
