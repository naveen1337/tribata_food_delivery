import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  TextInput,
  Image,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import Collapsible from 'react-native-collapsible';
import Icon from 'react-native-vector-icons/Ionicons';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  GeneralTopBar,
  RestaurantInfo,
  CategoryProducts,
  RestaurantProduct,
  RecommendProduct,
  CartButton,
} from '../components';
import {
  fetchCategoriesRemote,
  fetchRecommendedRemote,
  fetchRestaurantOffersRemote,
} from '../utils/restaurantRemoteService';
import {save_coupon} from '../actions/userAction';
import {fetchRestaurantInfo} from '../actions/restaurantAction';

export default function RestaurantScreen(props) {
  const dispatch = useDispatch();
  // Redux state
  const locationState = useSelector((state) => state.app.location);
  const userState = useSelector((state) => state.app.userinfo);
  // const offers = useSelector((state) => state.restaurant.offer);
  const RestaurantInfoState = useSelector((state) => state.restaurant.info);

  const [categories, setcategories] = useState([]);
  const [recommended, setrecommended] = useState([]);
  const [hasoffer, setHasoffer] = useState(false);
  const [offers, setOffers] = useState(false);
  const [loading, setloading] = useState(true);

  // Restaurant Initial Calls
  let restaurantInitCalls = async () => {
    dispatch(
      fetchRestaurantInfo({
        ...locationState,
        hotel_id: props.route.params.hotel_id ,
        mobile_number: userState.login
          ? userState.mobilenumber
          : '+91987654321',
      }),
    );
    let offers = await fetchRestaurantOffersRemote({
      hotel_id: props.route.params.hotel_id,
    });
    if (offers) {
      setHasoffer(true);
      setOffers(offers[0]);
    }
    let categories = await fetchCategoriesRemote({
      branch_id: props.route.params.hotel_id,
    });
    if (categories) {
      setcategories(categories);
    }
    let recommend = await fetchRecommendedRemote({
      branch_id: props.route.params.hotel_id,
    });
    setloading(false);
    if (recommend) {
      setrecommended(recommend);
    }
    // Clear coupon value is redux state
    dispatch(save_coupon(false));
  };

  useEffect(() => {
    (async () => {
      setTimeout(()=>{
        console.log(props.route.params);
        setloading(false)
      },300)
      await restaurantInitCalls();

    })();
  }, []);

  useEffect(() => {
    console.log('===========');
  }, []);

  return (
    <View style={tailwind('h-full')}>
      <ScrollView style={[styles.BackgroundSecondary]}>
        <GeneralTopBar name="Restaurant" navigation={props.navigation} />
        {/* Spinner */}
        <Spinner visible={loading} />

        {!loading ? (
          <LinearGradient
            style={[tailwind('pt-4 rounded-b-2xl pb-4 mb-4')]}
            colors={['#000000', '#2E2E2E']}>
            {RestaurantInfoState.map((item) => {
              // console.log(item);
              return (
                <RestaurantInfo
                  key={item.id}
                  id={item.id}
                  cityid={item.city_id}
                  image={item.Restaurant_image}
                  name={item.Restaurant_name}
                  special={item.special}
                  quantity={item.quantity}
                  distance={item.distance}
                  city_name={item.city_name}
                  rating={item.Restaurant_rating}
                  delivery_time={item.delivery_time}
                  navigation={props.navigation}
                />
              );
            })}
            {hasoffer ? (
              <View
                style={[
                  tailwind('rounded-lg mx-3 p-2 relative top-8'),
                  {backgroundColor: '#CBA960'},
                ]}>
                <View style={tailwind('flex flex-row items-center')}>
                  <Image
                    resizeMode={'contain'}
                    style={tailwind('w-6 h-6')}
                    source={require('../assets/icons/discount.png')}
                  />
                  <View style={tailwind('px-2')}>
                    <Text style={[tailwind('text-white'), styles.RegularFont]}>
                      {offers.promodetailes}
                    </Text>
                  </View>
                </View>

                <View
                  style={[
                    tailwind('py-1 rounded px-1'),
                    {
                      borderStyle: 'dashed',
                      borderRadius: 2,
                      borderWidth: 1,
                      borderColor: '#ffffff',
                    },
                  ]}>
                  <Text
                    style={[
                      tailwind('text-base uppercase text-center'),
                      styles.SemiBoldFont,
                    ]}>
                    {offers.promocode}
                  </Text>
                </View>
              </View>
            ) : null}
          </LinearGradient>
        ) : (
          <LinearGradient
            style={[tailwind('pt-4 rounded-b-2xl pb-4 mb-4')]}
            colors={['#000000', '#2E2E2E']}>
            <RestaurantInfo
              key={null}
              id={null}
              cityid={null}
              image={null}
              name={'...'}
              special={'...'}
              quantity={null}
              distance={'0.0'}
              city_name={'...'}
              rating={'0.0'}
              delivery_time={'0.0'}
            />
          </LinearGradient>
        )}

        {/* Products Section */}
        <View>
          {/* Recommended Section */}
          {recommended.length > 0 ? (
            <Pressable style={tailwind('py-2 mt-2')}>
              <Text
                style={[
                  tailwind('text-base mx-6 text-black'),
                  styles.SemiBoldFont,
                ]}>
                Recommended
              </Text>
            </Pressable>
          ) : null}

          {recommended.map((item) => {
            // console.log(item);
            return (
              <Pressable key={item.id} style={[tailwind('mx-2 my-2')]}>
                <RecommendProduct
                  id={item.id}
                  recom={true}
                  image={item.img_url}
                  veg={item.type === '1'}
                  name={item.product_name}
                  desc={item.description}
                  isfav={item.is_favourite}
                  price={item.price}
                  cityid={props.cityid}
                  hotelid={item.restaurant_id}
                  hasvariation={item.variation === 'true'}
                  variation={item.price_variation}
                  variation_id={item.variation_id}
                  hasaddon={item.addon}
                  addon_content={item.addon_content}
                  status={item.product_status === '1'}
                />
              </Pressable>
            );
          })}

          {categories.map((item) => {
            // console.log(item)
            return (
              <CategoryProducts
                key={item.id}
                hotelid={props.route.params.hotel_id}
                id={item.id}
                name={item.category_name}
                count={item.product_count}
                // cityid={cityid}
              />
            );
          })}
        </View>

        <View style={tailwind('mx-4 flex flex-row items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-12 h-12 mr-3')}
            source={require('../assets/images/food_licence.png')}
          />
          <Text
            style={[
              tailwind('text-gray-400'),
              styles.RegularFont,
              {fontSize: 10},
            ]}>
            Licence No. 550
          </Text>
        </View>

        <View style={tailwind('pb-20')}></View>
      </ScrollView>
      <View style={tailwind('absolute w-full bottom-5')}>
        <CartButton navigation={props.navigation} />
      </View>
    </View>
  );
}

// Hotel id in route params Mandatory Field [hotel_id]
