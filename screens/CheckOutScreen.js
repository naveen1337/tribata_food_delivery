import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Pressable, Image, Alert} from 'react-native';
import tailwind from 'tailwind-rn';
import Icon from 'react-native-vector-icons/Ionicons';
import Modal from 'react-native-modal';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import {clearCart} from '../actions/userAction';
import Spinner from 'react-native-loading-spinner-overlay';
import {CommonActions} from '@react-navigation/native';
import {
  placeOrder,
  UpdateOrderRemote,
  createRazorpay,
} from '../utils/userRemoteService';
import RazorpayCheckout from 'react-native-razorpay';

import {GeneralTopBar} from '../components';

export default function CheckoutScreen(props) {
  const dispatch = useDispatch();
  // const UserAddressState = useSelector((state) => state.user.selectedAddress);
  const userState = useSelector((state) => state.app.userinfo);
  const SelectedStore = useSelector((state) => state.user.restaurant);
  const CouponState = useSelector((state) => state.user.coupon);
  const locationState = useSelector((state) => state.app.location);
  const CartState = useSelector((state) => state.user.cart);

  // const [restaurant, setRestaurant] = useState('');
  const [products, setproducts] = useState([]);
  const [selectedpayment, setselectedpayment] = useState('');
  const [successmodal, setsuccessmodal] = useState(false);
  const [failmodal, setfailmodal] = useState(false);
  const [orderid, setorderid] = useState(0);
  const [loading, setloading] = useState(false);
  const [total, settotal] = useState(false);

  useEffect(() => {
    (async () => {
      const cartProducts = [];
      let total = 0;
      CartState.forEach((item) => {
        let newobj = {
          recipe_id: item.id,
          addon_content: item.addons,
          no_quantity: item.quantity,
          recipe_amount: parseInt(item.quantity * item.price),
          variation: item.variation,
          variation_id: item.variation_id,
        };
        cartProducts.push(newobj);
        total += parseInt(item.quantity * item.price);
        if (item.addons.length > 0) {
          let addontotal = 0;
          item.addons.forEach((addonitem) => {
            addontotal += parseInt(addonitem.price) * item.quantity;
          });
          total += addontotal;
        }
        settotal(total);
      });
      console.log('Total Amount: ', total);
      setproducts(cartProducts);
    })();
  }, [CartState]);

  const initpayment = async () => {
    // console.log(selectedpayment);
    if (selectedpayment === '') {
      Alert.alert(
        'Please Select a Payment Method',
        'Payment Option can not be Empty',
      );
      return;
    }
    setloading(true);
    const createorder = await placeOrder({
      address_id: userState.selectedaddress.address_id,
      booking_date: Date().toString().split(' ').slice(1, 4).join('-'),
      booking_time: Date().toString().split(' ')[4],
      branch_id: SelectedStore.id,
      coupon_applied: CouponState ? CouponState.reduce : '0',
      delivery_charge: props.route.params.delivery,
      delivery_date: '',
      delivery_time: '',
      delivery_type: selectedpayment,
      fcm_token: userState.fcm,
      mobile_number: userState.mobilenumber,
      order_status: 'Received',
      order_type: '0',
      original_delivery_charge: props.route.params.delivery, // Need to be discussed
      packing_charge: props.route.params.packing,
      recipe: products,
      pre_order: '0',
      recipe_total_amount:
        total -
        (parseInt(props.route.params.delivery) +
          parseInt(props.route.params.packing) +
          parseInt(props.route.params.tax)),
      redeem_point_recipe: '0',
      redeem_point_used: '0',
      service_id: '1',
      take_away: '0',
      tax: props.route.params.tax,
      total_amount: total,
    });

    if (createorder) {
      // Order is created, handle type of payment
      setorderid(createorder.order_id);
      if (selectedpayment === 'Cash on Delivery') {
        const updatecod = await UpdateOrderRemote({
          orderid: createorder.order_id,
          payment_status: 'Success',
          payment_method: selectedpayment,
        });
        // Stop the loading Screen
        setloading(false);
        if (updatecod) {
          setsuccessmodal(true);
          dispatch(clearCart());
        } else {
          setfailmodal(true);
        }
      } else if (selectedpayment === 'RazorPay') {
        let rzorder = await createRazorpay({
          amount: total,
          order_id: Date.now().toString(),
        });
        // Stop the Loading Screen
        setloading(false);
        if (rzorder) {
          const options = {
            description: 'Triabata Check',
            currency: 'INR',
            key: 'rzp_test_mclXfXMGebiyKX',
            amount: total * 1000,
            name: userState.name,
            order_id: rzorder.id,
            prefill: {
              name: userState.name,
              contact: userState.mobilenumber,
              email: userState.mail,
            },
            theme: {color: '#53a20e'},
          };
          RazorpayCheckout.open(options)
            .then(async (data) => {
              console.log(`Success: ${data.razorpay_payment_id}`);
              const updaterazorpay = await UpdateOrderRemote({
                orderid: createorder.order_id,
                payment_status: 'Success',
                payment_method: selectedpayment,
              });
              if (updaterazorpay) {
                setsuccessmodal(true);
                dispatch(clearCart()); // it clear it but local state persist have a data
              } else {
                // Error on updating order
                setfailmodal(true);
              }
            })
            .catch((error) => {
              console.log(`Error: ${error.code} | ${error.description}`);
              setfailmodal(true);
            });
        } else {
          // Error while creating order id
          setfailmodal(true);
        }
      }
    } else {
      setfailmodal(true);
    }
  };

  return (
    <View style={tailwind('h-full')}>
      <ScrollView style={[styles.BackgroundSecondary]}>
        <Spinner visible={loading} />

        <GeneralTopBar name="Checkout" navigation={props.navigation} />
        <View
          elevation={4}
          style={tailwind('bg-white p-3 my-2 mx-2 rounded-lg')}>
          <View style={tailwind('flex flex-row px-3')}>
            <Icon name="location" size={30} color="black" />
            <View>
              <Text style={[tailwind('text-base px-2'), styles.SemiBoldFont]}>
                {SelectedStore.name}
              </Text>
              <Text
                style={[
                  tailwind('text-sm px-2 py-1 mr-4'),
                  styles.RegularFont,
                ]}>
                {SelectedStore.address}
              </Text>
            </View>
          </View>

          <View style={tailwind('flex flex-row py-2 px-3')}>
            <Icon name="location" size={30} color="black" />
            <View>
              <Text style={[tailwind('text-base px-2'), styles.SemiBoldFont]}>
                Delivered to
              </Text>
              <Text
                style={[
                  tailwind('text-sm px-2 py-1 mr-4'),
                  styles.RegularFont,
                ]}>
                {userState.selectedaddress.address}
              </Text>
            </View>
          </View>
        </View>

        <Text
          style={[
            tailwind('text-base text-gray-500 px-4 my-3'),
            styles.SemiBoldFont,
          ]}>
          Payment Method
        </Text>

        <Pressable
          elevation={3}
          onPress={() => setselectedpayment('Cash on Delivery')}
          style={tailwind(
            'flex flex-row p-3 my-2 bg-white rounded-lg justify-between mx-3',
          )}>
          <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
            Cash on Delivery
          </Text>
          {selectedpayment === 'Cash on Delivery' ? (
            <Icon name="radio-button-on" size={25} color="#000000" />
          ) : (
            <Icon name="radio-button-off" size={25} color="#000000" />
          )}
        </Pressable>

        <Pressable
          elevation={3}
          onPress={() => setselectedpayment('RazorPay')}
          style={tailwind(
            'flex flex-row p-3 my-2 bg-white rounded-lg justify-between mx-3',
          )}>
          <Text style={[tailwind('text-base'), styles.SemiBoldFont]}>
            Razorpay
          </Text>
          {selectedpayment === 'RazorPay' ? (
            <Icon name="radio-button-on" size={25} color="#000000" />
          ) : (
            <Icon name="radio-button-off" size={25} color="#000000" />
          )}
        </Pressable>
      </ScrollView>
      <View style={tailwind('w-full  absolute bottom-2')}>
        <Pressable
          onPress={initpayment}
          style={tailwind('bg-black  py-3 mx-4 rounded-lg')}>
          <Text
            style={[
              tailwind('text-center text-base uppercase'),
              styles.ColorSecondary,
              styles.SemiBoldFont,
            ]}>
            Continue
          </Text>
        </Pressable>

        {/* Success Modal */}

        {successmodal ? (
          <Modal isVisible={true}>
            <View
              style={tailwind(
                'bg-white p-4 rounded-lg flex flex-col justify-center items-center',
              )}>
              <Text
                style={[
                  tailwind('text-2xl text-green-500'),
                  styles.SemiBoldFont,
                ]}>
                Success!
              </Text>
              <Image
                source={require('../assets/images/successpayment.png')}
                resizeMode={'contain'}
                style={tailwind('w-20 h-20')}
              />
              <Text
                style={[
                  tailwind('text-base text-center text-gray-500'),
                  styles.SemiBoldFont,
                ]}>
                Your Order has been placed Successfully
              </Text>

              <Pressable
                onPress={() => {
                  setsuccessmodal(false);
                  props.navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [{name: 'BottomStack'}],
                    }),
                  );
                }}
                style={tailwind('w-60 py-2 my-3 border rounded-lg')}>
                <Text
                  style={[
                    tailwind('text-base text-center uppercase'),
                    styles.SemiBoldFont,
                  ]}>
                  Back to Home
                </Text>
              </Pressable>

              <Pressable
                onPress={() => {
                  setsuccessmodal(false);
                  props.navigation.navigate('OrderTrackingScreen', {
                    orderid: orderid,
                  });
                }}
                style={tailwind('w-60  py-2 my-3 bg-black border rounded-lg')}>
                <Text
                  style={[
                    tailwind('text-base text-center uppercase'),
                    styles.SemiBoldFont,
                    styles.ColorSecondary,
                  ]}>
                  Track Order
                </Text>
              </Pressable>
            </View>
          </Modal>
        ) : null}

        {/* Failure modal */}
        {failmodal ? (
          <Modal isVisible={true}>
            <View
              style={tailwind(
                'bg-white p-4 rounded-lg flex flex-col justify-center items-center',
              )}>
              <Text
                style={[tailwind('text-2xl text-black'), styles.SemiBoldFont]}>
                Failed!
              </Text>
              <Image
                source={require('../assets/images/failed.png')}
                resizeMode={'contain'}
                style={tailwind('w-20 h-20')}
              />
              <Text
                style={[
                  tailwind('text-base text-center text-gray-500'),
                  styles.SemiBoldFont,
                ]}>
                Your Order has been Failed to place
              </Text>

              <Pressable
                onPress={() => {
                  setfailmodal(false);
                  props.navigation.dispatch(
                    CommonActions.reset({
                      index: 0,
                      routes: [{name: 'BottomStack'}],
                    }),
                  );
                }}
                style={tailwind('w-60  py-2 my-3 border rounded-lg')}>
                <Text
                  style={[
                    tailwind('text-base text-center uppercase'),
                    styles.SemiBoldFont,
                  ]}>
                  Back to Home
                </Text>
              </Pressable>

              <Pressable
                onPress={() => setfailmodal(false)}
                style={tailwind('w-60 py-2 my-3 bg-black border rounded-lg')}>
                <Text
                  style={[
                    tailwind('text-base text-center uppercase'),
                    styles.SemiBoldFont,
                    styles.ColorSecondary,
                  ]}>
                  Try Again
                </Text>
              </Pressable>
            </View>
          </Modal>
        ) : null}
      </View>
    </View>
  );
}
