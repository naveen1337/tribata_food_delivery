import React, {useState, useEffect} from 'react';
import {View, Text, Alert, Image, ActivityIndicator} from 'react-native';
import tailwind from 'tailwind-rn';
import {useDispatch} from 'react-redux';
import {styles} from '../constants/styles';
import firebase from '@react-native-firebase/app';
import {fetchCoords} from '../actions/appActions';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNExitApp from 'react-native-exit-app';
import {save_user} from '../actions/appActions';
import messaging from '@react-native-firebase/messaging';
import {Bubbles, DoubleBounce, Bars, Pulse} from 'react-native-loader';

import {
  isCityAvailable,
  fetchReverseGcodeRemote,
} from '../utils/homeRemoteService';
import {getUserInfo, getUserAddresesRemote} from '../utils/userRemoteService';
import {getJwtToken} from '../utils/helpers';

import {fetchServices, fetchTopSellers} from '../actions/homeActions';
import {saveFavoritesAction} from '../actions/appActions';

export default function InitialScreen(props) {
  const dispatch = useDispatch();
  const [loading, setloading] = useState(true);
  const [landmark, setlandmark] = useState('');
  const [address, setaddress] = useState('');

  const checkUserLogin = async (locationdata) => {
    // const remove = await AsyncStorage.removeItem('@isUserLoggedIn');
    let result = JSON.parse(await AsyncStorage.getItem('@isUserLoggedIn'));
    if (result === null || result.status === false) {
      console.log('Initial Screen, Logged is null or false set to false');
      AsyncStorage.setItem(
        '@isUserLoggedIn',
        JSON.stringify({status: false}),
      ).then(() => {
        dispatch(
          save_user({
            login: false,
            mobilenumber: null,
            name: null,
            mail: null,
            qrcode: null,
            verified: null,
            point: null,
            alladdress: [],
            selectedaddress: false,
          }),
        );
      });
      return false;
    } else {
      console.log('Initail Screen, User is login', result.mobilenumber);
      // User is Login
      let userdata = await getUserInfo({
        mobile_number: result.mobilenumber,
      });
      let useralladdress = await getUserAddresesRemote({
        mobile_number: result.mobilenumber,
      });
      let token = await getJwtToken();
      console.log(` User ${userdata} , ${useralladdress} `);
      dispatch(
        saveFavoritesAction({
          mobile_number: result.mobilenumber,
          latitude: locationdata.latitude,
          longitude: locationdata.longitude,
        }),
      );
      const fcmToken = await firebase.messaging().getToken();
      console.log('FCM TOKEN', fcmToken);
      dispatch(
        save_user({
          login: true,
          id: userdata.id,
          mobilenumber: result.mobilenumber,
          name: userdata.name,
          mail: userdata.email,
          qrcode: userdata.qr_code,
          verified: userdata.user_verify,
          point: userdata.total_point_earned,
          jwt: token,
          fcm: null,
          alladdress: useralladdress,
          selectedaddress:
            useralladdress.length > 0 ? useralladdress[0] : false,
        }),
      );
      setaddress(useralladdress.length > 0 ? useralladdress[0].address : '');
      return true;
    }
  };

  const islocationAvailable = async (locationdata) => {
    const userlogincheck = await checkUserLogin(locationdata);
    if (userlogincheck) {
      const isAvailable = await isCityAvailable(locationdata);
      setloading(false);
      if (isAvailable) {
        props.navigation.reset({
          index: 0,
          routes: [{name: 'BottomStack'}],
        });
      } else {
        console.log('Directing to NO Service Screen');
        props.navigation.reset({
          index: 0,
          routes: [{name: 'NoServiceScreen'}],
        });
      }
    } else {
      // user is not logged in, fetch reverse coordinate
      const reversegeo = await fetchReverseGcodeRemote(locationdata);
      if (reversegeo) {
        setaddress(reversegeo.display_name);
        const isAvailable = await isCityAvailable(locationdata);
        setloading(false);
        if (isAvailable) {
          props.navigation.reset({
            index: 0,
            routes: [{name: 'BottomStack'}],
          });
        } else {
          console.log('Directing to NO Service Screen');
          props.navigation.reset({
            index: 0,
            routes: [{name: 'NoServiceScreen'}],
          });
        }
      }
    }
  };

  const initCalls = async () => {
    fetchCoords()
      .then((locationdata) => {
        console.log('>>', locationdata);
        // Prior Data Fetching
        // checkUserLogin();
        dispatch(fetchServices(locationdata));
        dispatch(fetchTopSellers(locationdata));
        islocationAvailable(locationdata);
      })
      .catch((err) => {
        console.log('Catch', err);
        Alert.alert(
          'Location is Mandatory',
          'Permission Rejected',
          [
            {
              text: 'Close the Application',
              onPress: () => RNExitApp.exitApp(),
            },
            {text: 'OK', onPress: () => initCalls()},
          ],
          {cancelable: false},
        );
      });
  };

  useEffect(() => {
    (async () => {
      await initCalls();
    })();
  }, []);

  return (
    <View
      style={[
        tailwind('flex flex-col h-full justify-center items-center'),
        styles.BackgroundPrimary,
      ]}>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-24 h-24')}
        source={require('../assets/images/nav_logo.png')}
      />
      <Text
        style={[tailwind('text-white py-4 text-center'), styles.SemiBoldFont]}>
        Delivering to
      </Text>
      <Image
        resizeMode={'contain'}
        style={tailwind('w-6 h-6')}
        source={require('../assets/images/ic_notify.png')}
      />
      <Text
        style={[tailwind('text-white py-4 text-center'), styles.SemiBoldFont]}>
        {address}
      </Text>

      {loading ? <Bars size={15} color="#CBA960" /> : null}
      <Text></Text>
    </View>
  );
}
