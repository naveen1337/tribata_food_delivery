import React, {useState} from 'react';
import {View, Text, Image, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

import {GeneralTopBar} from '../components';

export default function QRCodeScreen(props) {
  return (
    <View style={[tailwind('pb-20'), styles.BackgroundSecondary]}>
      <GeneralTopBar name="My QR Code" navigation={props.navigation} />
      <View
        style={tailwind('flex flex-col h-full justify-center items-center')}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-60 h-60')}
          source={{uri: props.route.params.qrcode}}
        />
        <Text style={[tailwind('text-xl py-4'), styles.SemiBoldFont]}>
          {props.route.params.name}
        </Text>
        <Text style={[tailwind(' py-0'), styles.MediumFont]}>
          {props.route.params.mail}
        </Text>
        <Text style={[tailwind(' py-0'), styles.MediumFont]}>
          {props.route.params.mobilenumber}
        </Text>

        <Text style={[tailwind(' py-1'), styles.MediumFont]}>
          Scan My Qr Code
        </Text>
      </View>
    </View>
  );
}

// Get a route params as [qrcode,name,mail,mobilenumber]
