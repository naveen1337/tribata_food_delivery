import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Pressable,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import MapView, {Marker} from 'react-native-maps';
import Modal from 'react-native-modal';
import {useSelector} from 'react-redux';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {fetchReverseGcodeRemote} from '../utils/homeRemoteService';
import {styles} from '../constants/styles';
import find from 'lodash/find';

import {GeneralTopBar} from '../components';

export default function PickupAddressEntryScreen(props) {
  const locationState = useSelector((state) => state.app.location);
  const userState = useSelector((state) => state.app.userinfo);

  const [modal, setmodal] = useState(false);
  const [value, setvalue] = useState('');
  const [title, setTitle] = useState('');
  const [landmark, setLandmark] = useState('');
  const [address, setAddress] = useState('');
  const [city, setcity] = useState('');
  const [pincode, setpincode] = useState('');

  const [pickup, setpickup] = useState('');
  const [spinnerloading, setSpinnerloading] = useState(false);
  const [marginBottom, setmarginBottom] = useState(1);
  const [locallocation, setlocallocation] = useState(locationState);

  const onSubmitAddress = async () => {
    if (title && landmark && address) {
      let pickup = {
        mobile_number: userState.mobilenumber,
        address_name: title,
        address: address,
        city: '00000000',
        landmark: landmark,
        pincode: '000000',
        latitude: locallocation.latitude,
        longitude: locallocation.longitude,
      };
      setpickup(pickup);
      if (userState.alladdress.length === 0) {
        props.navigation.navigate('AddressEntryScreen', {
          back: 'pickup',
        });
      } else {
        setmodal(true);
      }
    } else {
      Alert.alert('Invalid Input', 'All Fields Are mandatory');
    }
  };

  const setDeliverAddress = () => {
    console.log('=======');
    console.log(value);
    console.log(pickup);
    setmodal(false);
    props.navigation.navigate('PickUpScreen', {
      pickupaddress: pickup,
      deliveryaddress: value,
    });
  };

  const changevalue = (e) => {
    let found = find(userState.alladdress, {address_id: e});
    // console.log(found);
    if (found) {
      setvalue(found);
    }
  };

  useEffect(() => {
    let _ismounted = true;
    (async () => {
      const response = await fetchReverseGcodeRemote({
        latitude: locallocation.latitude,
        longitude: locallocation.longitude,
      });
      // console.log('>>>', response);
      if (response && _ismounted) {
        setAddress(response.display_name);
        setcity(response.town);
        setpincode(response.postcode);
        setTitle(response.address.suburb);
        setLandmark(response.address.suburb);
      }
      return () => {
        _ismounted = false;
      };
    })();
    setvalue(userState.selectedaddress);
  }, [locallocation, userState]);

  return (
    <View style={[tailwind('h-full'), styles.BackgroundSecondary]}>
      <GeneralTopBar
        name="Select Pickup Location"
        navigation={props.navigation}
      />

      <View style={tailwind('h-full flex flex-col justify-between')}>
        <MapView
          onMapReady={() => setmarginBottom(0)}
          onRegionChangeComplete={(e) =>
            setlocallocation({
              latitude: e.latitude,
              longitude: e.longitude,
            })
          }
          style={
            (tailwind('flex-grow mb-0'), {height: '40%', margin: marginBottom})
          }
          showsUserLocation={true}
          showsMyLocationButton={true}
          zoomControlEnabled={true}
          initialRegion={{
            latitude: locationState.latitude,
            longitude: locationState.longitude,
            latitudeDelta: 0.0043,
            longitudeDelta: 0.0034,
          }}>
          <Marker
            // onDragEnd={(e) => setlocallocation(e.nativeEvent.coordinate)}
            // draggable
            coordinate={{
              latitude: locallocation.latitude,
              longitude: locallocation.longitude,
              latitudeDelta: 0.0043,
              longitudeDelta: 0.0034,
            }}
            icon={require('../assets/icons/pin_store.png')}></Marker>
        </MapView>

        <ScrollView>
          <View
            style={[
              tailwind('px-4 py-3 flex-grow rounded-2xl w-full'),
              styles.BackgroundSecondary,
            ]}>
            <View
              elevation={3}
              style={tailwind(
                'flex flex-row items-center bg-white rounded mt-3 mb-2',
              )}>
              <Icon
                style={tailwind('p-2')}
                name="home-outline"
                size={20}
                color="#000000"
              />
              <TextInput
                placeholder="Title home / Office"
                value={title}
                onChangeText={(e) => setTitle(e)}
                style={[
                  tailwind('flex-grow py-3 text-base '),
                  styles.RegularFont,
                ]}
              />
            </View>

            <View
              elevation={3}
              style={tailwind(
                'flex flex-row items-center bg-white rounded my-2',
              )}>
              <Icon
                style={tailwind('p-2')}
                name="business-outline"
                size={20}
                color="#000000"
              />
              <TextInput
                placeholder="Landmark"
                value={landmark}
                onChangeText={(e) => setLandmark(e)}
                style={[tailwind('flex-grow text-base'), styles.RegularFont]}
              />
            </View>

            <View elevation={3} style={tailwind('flex bg-white rounded my-2')}>
              <TextInput
                multiline={true}
                value={address}
                onChangeText={(e) => setAddress(e)}
                placeholder="Address"
                style={[tailwind('flex-grow text-base'), styles.RegularFont]}
              />
            </View>

            <Pressable
              onPress={onSubmitAddress}
              style={tailwind('bg-black py-4 px-2 rounded-lg')}>
              <Text
                style={[
                  tailwind('text-base uppercase text-center'),
                  styles.SemiBoldFont,
                  styles.ColorSecondary,
                ]}>
                Confirm Location
              </Text>
            </Pressable>
          </View>
          <Text style={tailwind('pb-10')}></Text>
        </ScrollView>
      </View>

      {/* Modal */}
      {userState.alladdress.length > 0 ? (
        <Modal
          isVisible={modal}
          animationInTiming={100}
          animationOutTiming={100}>
          <View style={tailwind('bg-white rounded-2xl px-6 py-5')}>
            <View
              style={tailwind(
                'flex flex-row justify-between items-center py-1',
              )}>
              <Text style={[tailwind('text-lg'), styles.MediumFont]}>
                Select Delivery Address
              </Text>
              <Pressable
                onPress={() => setmodal(false)}
                style={tailwind('p-3')}>
                <Icon name="close" size={25} color="#000000" />
              </Pressable>
            </View>
            <View
              style={tailwind('border border-gray-200 rounded-lg py-2 my-2')}>
              <Picker
                style={{height: 30, width: '100%'}}
                selectedValue={value.address_id}
                mode={'dropdown'}
                onValueChange={(e, i) => changevalue(e)}>
                {userState.alladdress.map((item) => {
                  return (
                    <Picker.Item
                      key={item.address_id}
                      label={item.address_name}
                      value={item.address_id}
                    />
                  );
                })}
              </Picker>
            </View>
            <View style={tailwind('py-1 px-3')}>
              <Text
                style={[
                  tailwind('text-base'),
                  styles.RegularFont,
                  styles.ColorSecondary,
                ]}>
                {value.address}
              </Text>
            </View>

            <Pressable
              onPress={setDeliverAddress}
              style={[tailwind('py-3 bg-black w-full rounded-lg my-2')]}>
              <Text
                style={[
                  tailwind('text-base uppercase text-center'),
                  styles.ColorSecondary,
                  styles.SemiBoldFont,
                ]}>
                Continue
              </Text>
            </Pressable>
            <Text
              style={[
                tailwind('text-center uppercase py-1 text-gray-600'),
                styles.RegularFont,
              ]}>
              OR
            </Text>
            <Pressable
              onPress={() => {
                setmodal(false);
                props.navigation.navigate('AddressEntryScreen', {
                  back: 'pickup',
                });
              }}
              style={tailwind('py-2 border rounded-lg border-gray-300')}>
              <Text
                style={[
                  tailwind('text-base uppercase text-center'),
                  styles.ColorSecondary,
                  styles.SemiBoldFont,
                ]}>
                Add Address
              </Text>
            </Pressable>
          </View>
        </Modal>
      ) : null}
    </View>
  );
}
