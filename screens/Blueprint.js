import React,{useState} from 'react';
import {View, Text, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

import {GeneralTopBar} from '../components'

export default function HomeScreen(props) {
  return (
    <ScrollView style={[styles.BackgroundPrimary]}>
       <GeneralTopBar name="Cart" navigation={props.navigation} />
    </ScrollView>
  );
}
