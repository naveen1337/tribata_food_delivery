import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  TextInput,
  Pressable,
  Alert,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {fetchRestaurantOffersRemote} from '../utils/restaurantRemoteService';
import {save_coupon} from '../actions/userAction';
import _ from 'lodash';
import Snackbar from 'react-native-snackbar';
import Spinner from 'react-native-loading-spinner-overlay';

import {GeneralTopBar} from '../components';

export default function CouponScreen(props) {
  const dispatch = useDispatch();
  const [total, settotal] = useState([]);
  const [enteredcode, setenteredcode] = useState('');

  const CartState = useSelector((state) => state.user.cart);
  const PromoState = useSelector((state) => state.home.allPromo);

  const totalprice = () => {
    let total = 0;
    CartState.forEach((item, i) => {
      // console.log(item);
      if (item.addons.length > 0) {
        let addontotal = 0;
        item.addons.forEach((addonitem) => {
          addontotal += parseInt(addonitem.price) * item.quantity;
        });
        total += addontotal;
      }
      total += parseInt(item.price) * item.quantity;
      // console.log('Total', total);
      settotal(parseInt(total));
    });
  };

  useEffect(() => {
    (async () => {
      totalprice();
      // console.log(PromoState);
    })();
  }, []);

  const handleApplyCoupon = (e) => {
    const coupon = _.find(PromoState, {promocode: e});
    if (coupon) {
      // Check is coupon is applicable
      try {
        if (total > coupon.promohighprice) {
          let percentage = coupon.sno.split('');
          percentage.pop();
          percentage = parseInt(percentage.join(''));
          // console.log(percentage);
          let reduction = total * (percentage / 100);
          console.log(reduction);
          if (reduction > coupon.promohighprice) {
            console.log('ok reduce');
            dispatch(
              save_coupon({
                code: coupon.promocode,
                reduce: coupon.promohighprice,
                percentage: percentage,
                promoprice: parseInt(coupon.promoprice),
                promohighprice: parseInt(coupon.promohighprice),
              }),
            );
            props.navigation.goBack();
          } else {
            throw 'Not Applicable';
          }
        } else {
          throw 'Total Amount is less';
        }
      } catch (err) {
        console.log(err);
        Snackbar.show({
          text: `Promo Not applicable to this order price`,
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: 'yellow',
            onPress: () => {},
          },
        });
      }

      // if (parseInt(coupon.promohighprice) < total) {
      //   Snackbar.show({
      //     text: `Promo only Not applicable to this order price`,
      //     duration: Snackbar.LENGTH_LONG,
      //     action: {
      //       text: 'Ok',
      //       textColor: 'yellow',
      //       onPress: () => {},
      //     },
      //   });
      // } else {
      //   // Apply coupon
      //   dispatch(save_coupon(coupon));
      //   console.log(coupon);
      //   props.navigation.goBack();
      // }
    } else {
      Alert.alert('Invalid', 'Entered coupon is Invalid');
    }
  };

  return (
    <ScrollView style={[styles.BackgroundSecondary]}>
      <GeneralTopBar name="Apply Coupon" navigation={props.navigation} />
      {/* Spinner */}
      {/* <Spinner visible={loading} /> */}
      <View>
        <View
          style={[
            tailwind(
              'bg-white rounded-lg my-3 py-2 px-3 mx-3 flex flex-row items-center justify-between',
            ),
            {
              borderRadius: 1,
              borderStyle: 'dashed',
              borderWidth: 1,
              borderColor: '#CBA960',
            },
          ]}>
          <View style={tailwind('flex flex-row items-center')}>
            <Image
              resizeMode={'contain'}
              style={tailwind('w-7 h-7')}
              source={require('../assets/icons/discount.png')}
            />
            <TextInput
              value={enteredcode}
              onChangeText={(e) => setenteredcode(e)}
              placeholder="Enter Coupon Code"
              maxLength={20}
              style={[tailwind('w-44 py-1 text-base'), styles.RegularFont]}
            />
          </View>
          <Pressable
            onPress={() => {
              handleApplyCoupon(enteredcode);
            }}>
            <Text
              style={[
                tailwind('uppercase text-base'),
                styles.SemiBoldFont,
                styles.ColorSecondary,
              ]}>
              <Text>Apply</Text>
            </Text>
          </Pressable>
        </View>
      </View>

      {PromoState.map((item) => {
        return item.restaurant_id === props.route.params.store ? (
          <View
            key={item.promocode}
            style={[
              tailwind(
                'bg-white mx-3 border border-gray-300 border-2 my-2 p-3 rounded-lg',
              ),
            ]}>
            <View style={tailwind('flex flex-row items-center')}>
              <View
                style={[
                  tailwind('flex-grow rounded-lg mx-3 py-1'),
                  {
                    borderRadius: 1,
                    borderStyle: 'dashed',
                    borderWidth: 1,
                    borderColor: '#CBA960',
                  },
                ]}>
                <Text
                  style={[
                    tailwind('text-base uppercase px-2'),
                    styles.SemiBoldFont,
                  ]}>
                  {item.promocode}
                </Text>
              </View>
              <Pressable onPress={() => handleApplyCoupon(item.promocode)}>
                <Text
                  style={[
                    tailwind('uppercase text-base'),
                    styles.SemiBoldFont,
                    styles.ColorSecondary,
                  ]}>
                  Apply
                </Text>
              </Pressable>
            </View>
            <Text
              style={[
                tailwind('mx-3 text-xs w-10/12 py-2'),
                styles.RegularFont,
              ]}>
              {item.promodetailes}
            </Text>
          </View>
        ) : null;
      })}
      {/* {noresults ? (
        <Text
          style={[tailwind('text-base text-center pt-6'), styles.RegularFont]}>
          No Coupon Available
        </Text>
      ) : null} */}
    </ScrollView>
  );
}
