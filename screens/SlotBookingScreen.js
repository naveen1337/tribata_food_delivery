import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  Pressable,
  TextInput,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {GeneralTopBar} from '../components';
import Icon from 'react-native-vector-icons/Ionicons';
import {createEnquiryRemote} from '../utils/homeRemoteService';
import Snackbar from 'react-native-snackbar';

export default function SlotBookingScreen(props) {
  const [name, setname] = useState('');
  const [email, setemail] = useState('');
  const [mobile, setmobile] = useState('');
  const [message, setmessage] = useState('');

  const Onformsubmit = async () => {
    if ((name && email, mobile, message)) {
      const responce = await createEnquiryRemote({
        email: email,
        message: message,
        mobile_number: mobile,
        name: name,
      });
      if (responce) {
        Snackbar.show({
          numberOfLines: 2,
          textColor: '#ffffff',
          backgroundColor: 'green',
          text: 'Successfully added Enquiry',
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: '#ffffff',
            onPress: () => {},
          },
        });
        props.navigation.goBack();
      } else {
        Snackbar.show({
          numberOfLines: 2,
          textColor: '#000000',
          backgroundColor: 'red',
          text: 'Faild Try Again',
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: '#000000',
            onPress: () => {},
          },
        });
      }
    } else {
      Alert.alert('Invalid Input', 'All Fields are mandatory');
    }
  };

  return (
    <ScrollView style={[styles.BackgroundSecondary]}>
      <GeneralTopBar name="Enquiry" navigation={props.navigation} />

      <View style={tailwind('px-3 py-2')}>
        <View
          elevation={3}
          style={tailwind(
            'flex flex-row items-center bg-white rounded mt-3 mb-4',
          )}>
          <Icon
            style={tailwind('p-2')}
            name="person-circle-outline"
            size={20}
            color="#000000"
          />
          <TextInput
            value={name}
            onChangeText={(e) => setname(e)}
            placeholder="Name"
            style={[tailwind('text-base flex-grow'), styles.RegularFont]}
          />
        </View>

        <View
          elevation={3}
          style={tailwind('flex flex-row items-center bg-white rounded mb-4')}>
          <Icon
            style={tailwind('p-2')}
            name="call-outline"
            size={20}
            color="#000000"
          />
          <TextInput
            value={mobile}
            onChangeText={(e) => setmobile(e)}
            keyboardType={'number-pad'}
            placeholder="Mobile Number"
            style={[tailwind('text-base flex-grow'), styles.RegularFont]}
          />
        </View>

        <View
          elevation={3}
          style={tailwind('flex flex-row items-center bg-white rounded mb-4')}>
          <Icon
            style={tailwind('p-2')}
            name="at-outline"
            size={20}
            color="#000000"
          />
          <TextInput
            value={email}
            onChangeText={(e) => setemail(e)}
            placeholder="Email Address"
            style={[tailwind('text-base flex-grow'), styles.RegularFont]}
          />
        </View>

        <View
          elevation={3}
          style={tailwind('flex flex-row bg-white rounded mb-4')}>
          <Icon
            style={tailwind('p-2')}
            name="document-text-outline"
            size={20}
            color="#000000"
          />
          <TextInput
            value={message}
            onChangeText={(e) => setmessage(e)}
            multiline={true}
            placeholder="Message"
            style={[tailwind('text-base flex-grow'), styles.RegularFont]}
          />
        </View>

        <Pressable
          onPress={Onformsubmit}
          style={tailwind('py-3 my-3 bg-black rounded')}>
          <Text
            style={[
              tailwind('text-center text-base uppercase'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            Continue
          </Text>
        </Pressable>
      </View>
    </ScrollView>
  );
}
