import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Pressable,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/Ionicons';
import {fetchPickupcostAction} from '../actions/appActions';
import {useSelector, useDispatch} from 'react-redux';
import {GeneralTopBar} from '../components';
// import {uploadImageRemote} from '../utils/homeRemoteService'

export default function PickupScreen(props) {
  const dispatch = useDispatch();

  const pickupCostState = useSelector((state) => state.app.pickupcost);

  const [text, setText] = useState('');

  useEffect(() => {
    (async () => {
      dispatch(fetchPickupcostAction({}));
      console.log(props.route.params.deliveryaddress);
    })();
  }, []);

  const opencamara = () => {
    launchCamera(
      {
        mediaType: 'photo',
        quality: 0.5,
        includeBase64: true,
      },
      async (filedata) => {
        console.log(filedata);
      },
    );
  };

  // When form submited >
  const formSubmit = () => {
    if (text) {
      props.navigation.navigate('PickupCheckOutScreen', {
        pickuplocation: props.route.params.pickupaddress,
        deliverylocation: props.route.params.deliveryaddress,
        pickupcost: pickupCostState,
        items: text,
      });
    }
    else{
      Alert.alert('Field Required','Enter your pickup items')
    }
  };

  return (
    <ScrollView keyboardShouldPersistTaps={'handled'} style={[styles.BackgroundSecondary, tailwind('')]}>
      <GeneralTopBar name="Pickup" navigation={props.navigation} />

      <View
        style={tailwind(
          'bg-white h-40 border  m-4 border border-gray-300 p-4 rounded-xl',
        )}>
        <Text
          style={[
            tailwind('text-sm'),
            styles.RegularFont,
            styles.ColorSecondary,
          ]}>
          Pickup Items (Optional)
        </Text>
        <TextInput
          value={text}
          onChangeText={(e) => setText(e)}
          multiline={true}
        />
      </View>

      <Pressable
        onPress={opencamara}
        style={tailwind('flex flex-col mx-2 justify-center items-center py-3')}>
        <Icon name="image-outline" size={100} color="#000000" />
        <Text style={[tailwind('text-base'), styles.RegularFont]}>
          Select Image From Gallery
        </Text>
      </Pressable>

      <KeyboardAvoidingView style={tailwind('mb-5')}>
        <View>
          <Text
            style={[
              tailwind('text-sm mx-2 py-4'),
              styles.RegularFont,
              styles.ColorSecondary,
            ]}>
            Minimum Charge of Rs.{pickupCostState} will be applicable for Pick &
            Delivery
          </Text>
          <Pressable
            onPress={formSubmit}
            style={tailwind('bg-black py-3 mx-2 rounded-lg')}>
            <Text
              style={[
                tailwind('text-center uppercase text-base'),
                styles.ColorSecondary,
              ]}>
              Continue
            </Text>
          </Pressable>
        </View>
      </KeyboardAvoidingView>
    </ScrollView>
  );
}

// Two route params as [pickupaddress],[pickupcost], [items] & [deliveryaddress]
