import React, {useState, useEffect} from 'react';
import {View, ScrollView, Text} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {SingleServiceHeader, RestaurantCard} from '../components';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {fetchTopHotelsRemote} from '../utils/homeRemoteService';
import {saveRestaurantWithOffer} from '../actions/homeActions';
import Spinner from 'react-native-loading-spinner-overlay';

export default function TopRestaurantScreen(props) {
  const dispatch = useDispatch();
  const locationState = useSelector((state) => state.app.location);
  const RestaunrantWithOffersState = useSelector(
    (state) => state.home.top_hotels,
  );

  return (
    <ScrollView style={styles.BackgroundSecondary}>
      <SingleServiceHeader navigation={props.navigation} />

      <View style={tailwind('flex flex-row px-3 py-3')}>
        <Icon name="restaurant" size={20} color="#000000" />
        <Text style={[tailwind('text-base px-2'), styles.MediumFont]}>
          Top Restaurants Near By You
        </Text>
      </View>

      <View>
        {RestaunrantWithOffersState.map((item) => {
          return (
            <RestaurantCard
              id={item.id}
              key={item.id}
              navigation={props.navigation}
              city_id={item.city_id}
              image={item.Restaurant_image}
              name={item.Restaurant_name}
              rating={item.Restaurant_rating}
              special={item.special}
              showdistance={true}
              distance={item.distance}
              intime={item.Restaurant_intime}
              outtime={item.Restaurant_outtime}
              city={item.city_name}
              status={item.Restaurant_status === '0'}
              hasoffer={item.offer !== '% OFF'}
              offer={item.offer}
              offercode={item.offer_code}
            />
          );
        })}
      </View>
    </ScrollView>
  );
}
