import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  Image,
  Pressable,
  KeyboardAvoidingView,
  Dimensions,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import MapView, {
  Marker,
  Callout,
  UrlTile,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import {fetchReverseGcodeRemote} from '../utils/homeRemoteService';
import {addUserAddressRemote} from '../utils/userRemoteService';
import Icon from 'react-native-vector-icons/Ionicons';
import Snackbar from 'react-native-snackbar';
import {saveUserAddressAction} from '../actions/userAction';
import _ from 'lodash';

import {GeneralTopBar} from '../components';

export default function AdddressEntryScreen(props) {
  const dispatch = useDispatch();

  const locationState = useSelector((state) => state.app.location);
  const userState = useSelector((state) => state.app.userinfo);

  const [locallocation, setlocallocation] = useState(locationState);
  const [title, setTitle] = useState('');

  const [landmark, setLandmark] = useState('');
  const [address, setAddress] = useState('');
  const [spinnerloading, setSpinnerloading] = useState(false);
  const [city, setcity] = useState('');
  const [pincode, setpincode] = useState('');

  const [marginBottom, setmarginBottom] = useState(1);

  const onSubmitAddress = async () => {
    if (title && landmark && address) {
      setSpinnerloading(true);
      let newobj = {
        mobile_number: userState.mobilenumber,
        address_name: title,
        address: address,
        city: '00000000',
        landmark: landmark,
        pincode: '000000',
        latitude: locallocation.latitude,
        longitude: locallocation.longitude,
      };

      const done = await addUserAddressRemote(newobj);

      if (done) {
        Snackbar.show({
          backgroundColor: 'green',
          numberOfLines: 2,
          text: `Address Successfully Added`,
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: 'white',
            onPress: () => {},
          },
        });
        // Dispatch the save user address action
        dispatch(saveUserAddressAction(newobj));
        try {
          if (props.route.params.back === 'profile') {
            setSpinnerloading(false);
            // props.navigation.navigate('UserProfileScreen');
            props.navigation.goBack();
          } else if (props.route.params.back === 'cart') {
            setSpinnerloading(false);
            props.navigation.goBack();
          } else if (props.route.params.back === 'pickup') {
            setSpinnerloading(false);
            // props.navigation.navigate('PickUpLocationScreen');
            props.navigation.goBack();
          } else {
            setSpinnerloading(false);
            // props.navigation.navigate('HomeScreen');
            props.navigation.goBack();
          }
        } catch {
          setSpinnerloading(false);
          props.navigation.goBack();
        }
      } else {
        setSpinnerloading(false);
        Alert.alert('Invalid Input', 'All Fields Are mandatory');
      }
    }
  };

  useEffect(() => {
    let _ismounted = true;

    (async () => {
      const response = await fetchReverseGcodeRemote(locallocation);
      if (response && _ismounted) {
        setAddress(response.display_name);
        setcity(response.town);
        setpincode(response.postcode);
        setTitle(response.address.suburb);
        setLandmark(response.address.suburb);
      }
    })();

    return () => {
      _ismounted = false;
    };
  }, [locallocation, userState]);

  return (
    <KeyboardAvoidingView style={tailwind('h-full')} behavior={'height'}>
      <GeneralTopBar name="Add Address" navigation={props.navigation} />
      <ScrollView style={styles.BackgroundSecondary}>
        <View style={[tailwind('h-full'), styles.BackgroundSecondary]}>
          {/* Spinner */}
          <Spinner visible={spinnerloading} />

          <MapView
            style={[
              tailwind('flex-grow'),
              {
                height: Dimensions.get('window').height / 3,
                width: '100%',
                margin: marginBottom,
              },
            ]}
            onMapReady={() => setmarginBottom(0)}
            onRegionChangeComplete={(e) =>
              setlocallocation({
                latitude: e.latitude,
                longitude: e.longitude,
              })
            }
            showsUserLocation={true}
            showsMyLocationButton={true}
            zoomControlEnabled={true}
            initialRegion={{
              latitude: locationState.latitude,
              longitude: locationState.longitude,
              latitudeDelta: 0.0043,
              longitudeDelta: 0.0034,
            }}>
            <Marker
              coordinate={{
                latitude: locallocation.latitude,
                longitude: locallocation.longitude,
                latitudeDelta: 0.0043,
                longitudeDelta: 0.0034,
              }}
              icon={require('../assets/icons/pin_target.png')}></Marker>
          </MapView>

          <View
            style={[
              tailwind(''),
              {height: Dimensions.get('window').height / 2},
            ]}>
            <ScrollView style={[tailwind('px-4')]}>
              <View
                elevation={3}
                style={tailwind(
                  'flex flex-row items-center bg-white rounded mt-3 mx-1 mb-3',
                )}>
                <Icon
                  style={tailwind('p-2')}
                  name="home-outline"
                  size={20}
                  color="#000000"
                />
                <TextInput
                  placeholder="Title home / Office"
                  value={title}
                  onChangeText={(e) => setTitle(e)}
                  style={[tailwind('text-base flex-grow'), styles.RegularFont]}
                />
              </View>
              <View
                elevation={3}
                style={tailwind(
                  'flex flex-row items-center bg-white rounded mx-1 mb-3',
                )}>
                <Icon
                  style={tailwind('p-2')}
                  name="business-outline"
                  size={20}
                  color="#000000"
                />
                <TextInput
                  placeholder="Landmark"
                  value={landmark}
                  onChangeText={(e) => setLandmark(e)}
                  style={[tailwind('text-base flex-grow'), styles.RegularFont]}
                />
              </View>
              <View
                elevation={3}
                style={tailwind('flex bg-white rounded mb-3 mx-1')}>
                <TextInput
                  multiline={true}
                  value={address}
                  onChangeText={(e) => setAddress(e)}
                  placeholder="Address"
                  style={[tailwind('text-base flex-grow'), styles.RegularFont]}
                />
              </View>

              <Pressable
                onPress={onSubmitAddress}
                style={tailwind('bg-black py-4 px-2 mb-3 rounded-lg')}>
                <Text
                  style={[
                    tailwind('text-base uppercase text-center'),
                    styles.SemiBoldFont,
                    styles.ColorSecondary,
                  ]}>
                  Save Address
                </Text>
              </Pressable>
            </ScrollView>
          </View>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}
