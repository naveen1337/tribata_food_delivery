import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

import {
  RestaurantCard,
  SingleServiceHeader,
  SmallRestaurantCard,
  TitleBarIconAction,
} from '../components';

export default function SingleServiceScreen(props) {
  const PromoState = useSelector((state) => state.home.allPromo);
  const TopHotelsState = useSelector((state) => state.home.top_hotels);

  return (
    <ScrollView style={[styles.BackgroundSecondary]}>
      <SingleServiceHeader navigation={props.navigation} />

      {props.route.params.goto === 'promo' ? (
        <View>
          <View style={tailwind('pt-3 pb-2 mx-3 flex flex-row')}>
            <Icon name="restaurant" size={20} color="#000000" />
            <Text
              style={[
                tailwind('mx-4 text-center text-base'),
                styles.MediumFont,
              ]}>
              Popular Offer Restaurants Near by You
            </Text>
          </View>
          <View style={tailwind('mx-4')}>
            {PromoState.map((item) => {
              // console.log(item)
              return (
                <SmallRestaurantCard
                  id={item.restaurant_id}
                  key={item.restaurant_id}
                  navigation={props.navigation}
                  city_id={item.city_id}
                  image={item.branch_image}
                  name={item.branch_name}
                  special={item.branch_special}
                  rating="3"
                  distance={item.distance}
                  showdistance={true}
                  intime={item.Restaurant_intime}
                  outtime={item.Restaurant_outtime}
                  status={item.Restaurant_status === '0'}
                  hasoffer={true}
                  offer={item.sno}
                  fullwidth={true}
                />
              );
            })}
          </View>
        </View>
      ) : null}

      {props.route.params.goto === 'nearby' ? (
        <View style={tailwind('py-2')}>
          <TitleBarIconAction
            navigation={props.navigation}
            iconname="offer"
            title="Coupon Offers"
            action={false}
            goto="promo"
          />

          <View>
            <View style={tailwind('mx-3')}>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                {PromoState.map((item) => {
                  // console.log(item);
                  return (
                    <SmallRestaurantCard
                      key={item.branch_name}
                      id={item.restaurant_id}
                      navigation={props.navigation}
                      city_id={item.city_id}
                      image={item.branch_image}
                      name={item.branch_name}
                      rating={item.Restaurant_rating}
                      special={item.branch_special}
                      distance={item.distance}
                      intime={item.Restaurant_intime}
                      outtime={item.Restaurant_outtime}
                      status={item.Restaurant_status === '0'}
                      hasoffer={true}
                      offer={item.sno}
                      navigation={props.navigation}
                    />
                  );
                })}
              </ScrollView>
            </View>

            <View style={tailwind('flex flex-row px-3 py-3')}>
              <Icon name="restaurant" size={20} color="#000000" />
              <Text style={[tailwind('text-base px-2'), styles.MediumFont]}>
                NearBy Restaurants
              </Text>
            </View>

            {TopHotelsState.map((item) => {
              return (
                <RestaurantCard
                  id={item.id}
                  key={item.id}
                  navigation={props.navigation}
                  city_id={item.city_id}
                  image={item.Restaurant_image}
                  name={item.Restaurant_name}
                  rating={item.Restaurant_rating}
                  special={item.special}
                  showdistance={true}
                  distance={item.distance}
                  intime={item.Restaurant_intime}
                  outtime={item.Restaurant_outtime}
                  city={item.city_name}
                  status={item.Restaurant_status === '0'}
                  hasoffer={item.offer !== '% OFF'}
                  offer={item.offer}
                  offercode={item.offer_code}
                />
              );
            })}
          </View>
        </View>
      ) : null}
    </ScrollView>
  );
}
