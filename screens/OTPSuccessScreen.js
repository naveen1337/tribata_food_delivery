import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Pressable,
  Image,
  Dimensions,
  TextInput,
  Alert,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import tailwind from 'tailwind-rn';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {styles} from '../constants/styles';
import {save_user} from '../actions/appActions';
import {getUserInfo, getUserAddresesRemote} from '../utils/userRemoteService';
import {getJwtToken} from '../utils/helpers';

export default function OTPSuccess(props) {
  const dispatch = useDispatch();

  const CartState = useSelector((state) => state.user.cart);

  const initcalls = async () => {
    const remove = await AsyncStorage.removeItem('@isUserLoggedIn');
    const update = await AsyncStorage.setItem(
      '@isUserLoggedIn',
      JSON.stringify({
        status: true,
        mobilenumber: props.route.params.mobilenumber,
      }),
    );
    const jwtres = await getJwtToken();
    console.log('JWT TOKEN:', jwtres);
    if (update === null) {
      const userdata = await getUserInfo({
        mobile_number: props.route.params.mobilenumber,
      });
      if (userdata) {
        // User already exist in the system
        const useralladdress = await getUserAddresesRemote({
          mobile_number: props.route.params.mobilenumber,
        });
        dispatch(
          save_user({
            login: true,
            id: userdata.id,
            mobilenumber: userdata.mobile_number,
            name: userdata.name,
            mail: userdata.email,
            qrcode: userdata.qr_code,
            verified: userdata.user_verify,
            jwt: jwtres,
            point: userdata.total_point_earned,
            alladdress: useralladdress,
            selectedaddress:
              useralladdress.length > 0 ? useralladdress[0] : false,
          }),
        );
        {
          CartState.length > 0
            ? props.navigation.navigate('CartScreen')
            : props.navigation.navigate('HomeScreen');
        }
      } else {
        // User not exist in the system
        Alert.alert('New user');
        props.navigation.navigate('ProfileEditScreen');
        return;
      }
    } else {
      console.log('Storage Error');
      Alert.alert('Storage Error');
    }
  };

  useEffect(() => {
    (async () => {
      await initcalls();
    })();
  }, []);

  return (
    <ScrollView style={[tailwind(''), styles.BackgroundSecondary]}>
      <View style={mystyles.top}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-40 h-60 mt-3')}
          source={require('../assets/images/otp.png')}
        />
      </View>

      <View
        elevation={3}
        style={[tailwind('px-4 py-6 rounded-lg bg-white'), mystyles.loginbox]}>
        <Text
          style={[tailwind('py-2 text-xl text-center'), styles.SemiBoldFont]}>
          Awesome!
        </Text>

        <Text
          style={[
            tailwind('text-sm py-4 text-center px-1'),
            styles.RegularFont,
          ]}>
          Your Phone Number has been verified successfully
        </Text>
        <Pressable
          onPress={() => {
            props.navigation.navigate('HomeStack');
          }}
          style={[tailwind('rounded-lg py-3'), styles.BackgroundPrimary]}>
          <Text
            style={[
              tailwind('text-lg uppercase text-center'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            Get Started
          </Text>
        </Pressable>
      </View>

      <View style={mystyles.bottom}>
        <Text></Text>
      </View>
    </ScrollView>
  );
}

let mystyles = StyleSheet.create({
  top: {
    height: Dimensions.get('window').height / 2,
    backgroundColor: '#F6E4B8',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bottom: {
    // height: Dimensions.get('window').height / 2,
  },
  loginbox: {
    marginHorizontal: Dimensions.get('window').width / 15,
    position: 'relative',
    bottom: Dimensions.get('window').height / 6,
    // marginHorizontal: 30,
  },
});
