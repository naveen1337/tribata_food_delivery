import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Pressable,
  Image,
  Dimensions,
  TextInput,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {NumberAvailableRemote} from '../utils/userRemoteService';

import {OTPInput} from '../components/';

import {OTPCheckRemote} from '../utils/userRemoteService';
import {setJwtToken} from '../utils/helpers';

export default function OTPScreen(props) {
  const [otp, setotp] = useState('');
  const [expiry, setexpiry] = useState(false);
  const [time, settime] = useState(60);

  useEffect(() => {
    if (expiry === false) {
      setTimeout(() => {
        if (time > 1) {
          settime(time - 1);
        } else {
          setexpiry(true);
        }
      }, 1000);
    }
  }, [time]);

  const onOtpEnter = (e) => {
    console.log(e);
    setotp(e);
  };

  const resendOTP = async (e) => {
    let result = await NumberAvailableRemote({
      mobile_number: props.route.params.mobilenumber,
      fcm_token: 'test',
    });
    console.log(result);
  };

  const onOTPSumbit = async () => {
    console.log(props.route.params.mobilenumber);
    let result = await OTPCheckRemote({
      mobile_number: props.route.params.mobilenumber,
      // otp: '1234', //Testing
      otp: otp, //Production
    });
    // Set false for Testing
    if (result) {
      console.log('OTP OK');
      await setJwtToken(result.jwt_token);
      setexpiry(true);
      props.navigation.navigate('OTPSuccessScreen', {
        mobilenumber: props.route.params.mobilenumber,
      });
    } else {
      Alert.alert('Invalid OTP');
    }
  };

  return (
    <ScrollView style={[tailwind(''), styles.BackgroundSecondary]}>
      <View style={mystyles.top}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-40 h-60 mt-3')}
          source={require('../assets/images/otp.png')}
        />
      </View>

      <View
        elevation={4}
        style={[tailwind('px-4 py-5 rounded-xl bg-white'), mystyles.loginbox]}>
        <Text
          style={[tailwind('py-3 text-xl text-center'), styles.SemiBoldFont]}>
          Verification
        </Text>
        <Text
          style={[
            tailwind('text-xs py-2 text-center px-1'),
            styles.RegularFont,
          ]}>
          Enter the OTP code sent to {props.route.params.mobilenumber}
        </Text>

        <OTPInput onOtpEnter={onOtpEnter} />

        {expiry ? (
          <Pressable onPress={resendOTP} style={tailwind('mt-3 py-2')}>
            <Text
              style={[
                tailwind('text-center text-base'),
                styles.RegularFont,
                styles.ColorSecondary,
              ]}>
              Resend OTP
            </Text>
          </Pressable>
        ) : (
          <Text
            style={[
              tailwind('text-center mt-5 py-2 text-sm'),
              styles.RegularFont,
            ]}>
            Time Remaining {time}
          </Text>
        )}

        <Pressable
          onPress={() => onOTPSumbit()}
          style={[tailwind('rounded-lg mt-3 py-3'), styles.BackgroundPrimary]}>
          <Text
            style={[
              tailwind('text-lg uppercase text-center'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            Verify
          </Text>
        </Pressable>
      </View>

      <View style={mystyles.bottom}>
        <Text></Text>
      </View>
    </ScrollView>
  );
}

let mystyles = StyleSheet.create({
  top: {
    height: Dimensions.get('window').height / 2,
    backgroundColor: '#F6E4B8',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bottom: {
    // height: Dimensions.get('window').height / 2,
  },
  loginbox: {
    marginHorizontal: Dimensions.get('window').width / 15,
    position: 'relative',
    bottom: Dimensions.get('window').height / 6,
    // marginHorizontal: 30,
  },
});

// OTP Screen Must Receive [mobilenumber] params in props.route

// OTP Sucess Screen Get Receive [mobilenumber] params in props.route

// Bug unmounted hooks components
