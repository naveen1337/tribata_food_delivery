import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  RefreshControl,
  Pressable,
} from 'react-native';
import {useSelector} from 'react-redux';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  getOrderTrackingRemote,
  getOrderInfoRemote,
} from '../utils/userRemoteService';
import {GeneralTopBar, OrderTracking, PricingCard} from '../components';

export default function OrderTrackingScreen(props) {
  const [orderid, setorderid] = useState(props.route.params.orderid);
  // const [orderid, setorderid] = useState('TRIIB24022164');
  const userState = useSelector((state) => state.app.userinfo);

  // Local state
  const [ordertracking, setordertracking] = useState([]);
  const [orderInfo, setOrderInfo] = useState([]);
  const [loaded, setloaded] = useState(false);
  const [refreshing, setRefreshing] = React.useState(false);

  const loadorderInfo = async () => {
    console.log('Loader Info');
    const trackingdata = await getOrderTrackingRemote({
      order_id: orderid,
    });
    if (trackingdata) {
      setordertracking(trackingdata);
    }
    const orderInfo = await getOrderInfoRemote({
      mobile_number: userState.mobilenumber,
      order_id: orderid,
    });
    console.log(orderInfo);
    if (orderInfo) {
      setOrderInfo(orderInfo[0]);
    }

    setloaded(true);
  };

  useEffect(() => {
    (async () => {
      await loadorderInfo();
    })();
  }, []);

  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);
    await loadorderInfo();
    setRefreshing(false);
  }, []);

  return (
    <View style={([styles.BackgroundSecondary], tailwind('h-full'))}>
      <GeneralTopBar name="Cart" navigation={props.navigation} />
      {loaded ? (
        <ScrollView
          style={[styles.BackgroundSecondary]}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }>
          <View>
            <View style={[tailwind('px-2 py-2')]}>
              <OrderTracking
                id={orderid}
                date={orderInfo.booked_date}
                placed={ordertracking.placed === 'Y'}
                accepted={ordertracking.accepted === 'Y'}
                processing={ordertracking.processing === 'Y'}
                packed={ordertracking.packed === 'Y'}
                dispatched={ordertracking.dispatched === 'Y'}
                delivered={ordertracking.delivered === 'Y'}
                cancelorder={false}
                orderstatus={orderInfo.order_status}
              />
            </View>

            {/* Delevery Address */}
            {orderInfo.delivery_partner.id != '' ? (
              <View
                elevation={3}
                style={tailwind(
                  'bg-white m-3 p-3 rounded-lg flex flex-row justify-between items-center pr-2',
                )}>
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-20 h-20 rounded-lg')}
                  source={{uri: orderInfo.delivery_partner.image}}
                />
                <View>
                  <Text
                    style={[
                      tailwind('text-gray-600 text-xs'),
                      styles.SemiBoldFont,
                    ]}>
                    Delivery Boy
                  </Text>
                  <Text style={styles.SemiBoldFont}>
                    {orderInfo.delivery_partner.name}
                  </Text>
                </View>

                <View style={tailwind('border rounded-lg p-3')}>
                  <Icon name="call" size={30} color="#000000" />
                </View>
              </View>
            ) : null}

            <View style={tailwind('flex flex-row mx-4 py-2 items-center')}>
              {/* {console.log('>>>', orderInfo)} */}
              {orderInfo.branch.name === '' ? (
                <Text
                  style={[
                    tailwind('text-base px-2'),
                    styles.MediumFont,
                    styles.ColorSecondary,
                  ]}>
                  {orderInfo.branch.name}
                </Text>
              ) : (
                <>
                  <Text style={[tailwind('text-sm'), styles.MediumFont]}>
                    Order at
                  </Text>
                  <Text
                    style={[
                      tailwind('text-base px-2'),
                      styles.MediumFont,
                      styles.ColorSecondary,
                    ]}>
                    {orderInfo.branch.name}
                  </Text>
                </>
              )}
            </View>

            {orderInfo.pickupitems != '' ? (
              <View
                elevation={3}
                style={tailwind('mx-3 p-3 bg-white rounded-lg')}>
                <Text>{orderInfo.pickupitems}</Text>
              </View>
            ) : null}

            {orderInfo.recipie_details.map((item) => {
              return (
                <View
                  elevation={3}
                  key={item.product_id + item.variation_id}
                  style={tailwind('bg-white p-3 rounded-2xl my-2 mx-6')}>
                  <Text
                    style={[
                      tailwind('text-sm w-11/12 py-1'),
                      styles.MediumFont,
                    ]}>
                    {item.recipe_name}
                  </Text>
                  <Text
                    style={[
                      tailwind('text-xs w-11/12'),
                      styles.RegularFont,
                      styles.ColorSecondary,
                    ]}>
                    {item.description}
                  </Text>
                  <View style={tailwind('flex flex-row justify-between py-1')}>
                    <View style={tailwind('flex flex-row')}>
                      <Text style={[tailwind('text-sm'), styles.MediumFont]}>
                        {item.recipe_quantity}
                      </Text>
                      <Text
                        style={[tailwind('text-sm px-1'), styles.MediumFont]}>
                        x
                      </Text>
                      <Text style={[tailwind('text-sm'), styles.MediumFont]}>
                        ₹ {item.recipe_price}
                      </Text>
                    </View>
                    <Text style={[tailwind('text-sm'), styles.SemiBoldFont]}>
                      ₹{' '}
                      {parseInt(item.recipe_quantity) *
                        parseInt(item.recipe_price)}
                    </Text>
                  </View>
                </View>
              );
            })}
            <Text
              style={[
                tailwind(' text-gray-500 mx-2 p-2'),
                styles.SemiBoldFont,
              ]}>
              Delivery Address
            </Text>

            <View
              elevation={3}
              style={tailwind('p-3 mx-4 bg-white rounded-xl my-2')}>
              <Text style={[tailwind('text-sm w-11/12'), styles.SemiBoldFont]}>
                {orderInfo.delivery_address}
              </Text>
              <Text style={[tailwind('py-2 text-xs'), styles.RegularFont]}>
                {orderInfo.delivery_landmark}
              </Text>
            </View>

            <Text
              style={[tailwind('text-gray-500 mx-2 p-2'), styles.SemiBoldFont]}>
              Amount
            </Text>
            <PricingCard
              itemstotal={orderInfo.total_recipe_amount}
              tax={orderInfo.tax}
              hascoupon={orderInfo.coupon_applied != '0'}
              coupon={orderInfo.coupon_applied}
              hasdelivery={orderInfo.pickupitems != ''}
              delivery={orderInfo.delivery_charges}
              packing={orderInfo.packing_charges}
            />
            <View style={tailwind(' py-2 flex flex-row justify-between')}>
              <Text
                style={[
                  tailwind(' text-gray-500 mx-2 p-2'),
                  styles.SemiBoldFont,
                ]}>
                Mode of Payment
              </Text>
              <Text
                style={[
                  tailwind('  text-black mx-2 p-2'),
                  styles.SemiBoldFont,
                ]}>
                {orderInfo.mode_of_payment}
              </Text>
            </View>
          </View>
          <View style={tailwind('pb-20')}></View>
        </ScrollView>
      ) : (
        <View style={[tailwind('h-full'), styles.BackgroundSecondary]}>
          <Spinner visible={!loaded} />
        </View>
      )}
      <View style={tailwind('absolute bottom-5 w-full')}>
        <Pressable
          style={tailwind(
            'mx-4 bg-black py-4 px-3 rounded-lg flex flex-row justify-between items-center',
          )}>
          <View style={tailwind('flex flex-row items-center')}>
            <Text style={tailwind('text-white')}>Total</Text>
            <Text
              style={[
                tailwind('text-base px-2 text-white'),
                styles.SemiBoldFont,
              ]}>
              ₹ {orderInfo.total_amount}
            </Text>
          </View>
          <Text
            style={[
              tailwind('text-base text-white uppercase'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}></Text>
        </Pressable>
      </View>
    </View>
  );
}

// props.route.params.orderid
