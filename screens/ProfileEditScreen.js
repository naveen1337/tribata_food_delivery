import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Pressable,
  Image,
  Dimensions,
  TextInput,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import {updateUser} from '../utils/userRemoteService';
import Spinner from 'react-native-loading-spinner-overlay';
import {save_user} from '../actions/appActions';

export default function ProfileEditScreen(props) {
  const dispatch = useDispatch();
  const [name, setname] = useState(props.route.params.name);
  const [mail, setmail] = useState(props.route.params.mail);
  const [loading, setloading] = useState(false);

  const userState = useSelector((state) => state.app.userinfo);

  const gotoaddress = async () => {
    if (name && mail) {
      // Create aka [Update] a user and Navigate to Home Stack
      setloading(true);
      let result = await updateUser({
        mobile_number: props.route.params.mobilenumber,
        name: name,
        email: mail,
        fcm_token: 'test',
      });
      setloading(false);

      if (result) {
        let newobj = {...userState};
        newobj.name = name;
        newobj.ename = mail;
        dispatch(save_user(newobj));
        console.log('User Created / Updated');
        props.navigation.navigate('HomeStack');
      } else {
        Alert.alert('Invalid Response', 'User Creation Failed');
      }
    } else {
      Alert.alert('Invalid Input', 'All Fields are Required');
    }
  };

  return (
    <ScrollView style={[tailwind(''), styles.BackgroundSecondary]}>
      {/* Spinner */}
      <Spinner visible={loading} />

      <View style={mystyles.top}>
        <Image
          resizeMode={'contain'}
          style={tailwind('w-60 h-56 mt-3')}
          source={require('../assets/images/signin.png')}
        />
      </View>

      <View
        style={[tailwind('px-4 py-3 rounded-lg bg-white'), mystyles.loginbox]}>
        <Text style={[tailwind('py-1 text-xl'), styles.SemiBoldFont]}>
          Profile
        </Text>
        <View style={[tailwind('border-b-2 border-gray-300')]}>
          <TextInput
            value={props.route.params.mobilenumber}
            style={[
              tailwind('px-2 text-base py-2 w-11/12'),
              styles.RegularFont,
            ]}
          />
        </View>

        <View style={[tailwind('border-b-2 my-2 border-gray-300')]}>
          <TextInput
            value={name}
            onChangeText={(e) => setname(e)}
            placeholder={'Your Name'}
            style={[
              tailwind('px-2 text-base py-2 w-11/12'),
              styles.RegularFont,
            ]}
          />
        </View>

        <View style={[tailwind('border-b-2 my-2 border-gray-300')]}>
          <TextInput
            value={mail}
            onChangeText={(e) => setmail(e)}
            placeholder={'Your Email'}
            style={[
              tailwind('px-2 text-base py-2 w-11/12'),
              styles.RegularFont,
            ]}
          />
        </View>
        <Pressable
          onPress={gotoaddress}
          style={[tailwind('rounded-lg py-3 my-4'), styles.BackgroundPrimary]}>
          <Text
            style={[
              tailwind('text-lg text-center'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            CONTINUE
          </Text>
        </Pressable>
      </View>

      <View style={mystyles.bottom}>
        <Text></Text>
      </View>
    </ScrollView>
  );
}

let mystyles = StyleSheet.create({
  top: {
    height: Dimensions.get('window').height / 2,
    backgroundColor: '#F6E4B8',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bottom: {
    // height: Dimensions.get('window').height / 2,
  },
  loginbox: {
    marginHorizontal: Dimensions.get('window').width / 15,
    position: 'relative',
    bottom: Dimensions.get('window').height / 6,
    // marginHorizontal: 30,
  },
});
