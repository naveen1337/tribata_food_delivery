import React, {useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  Pressable,
  Image,
  Dimensions,
  TextInput,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {NumberAvailableRemote} from '../utils/userRemoteService';
import LinearGradient from 'react-native-linear-gradient';

export default function LoginScreen(props) {
  const [mobilenumber, setmobilenumber] = useState('');

  const onFormSubmit = async () => {
    try {
      if (mobilenumber.length === 10) {
        let result = await NumberAvailableRemote({
          mobile_number: '+91' + mobilenumber,
          fcm_token: 'test',
        });
        if (result) {
          props.navigation.navigate('OTPScreen', {
            mobilenumber: '+91' + mobilenumber,
          });
        } else {
          throw 'Unservicable Number';
        }
      } else {
        throw 'Number Wrong';
      }
    } catch (err) {
      Alert.alert('Invalid', err);
    }
  };

  return (
    <ScrollView style={[tailwind(''), styles.BackgroundSecondary]}>
      <LinearGradient style={[]} colors={['#FFF0CE', '#E9CB96']}>
        <View style={mystyles.top}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-40 h-40 mt-3')}
            source={require('../assets/images/signin.png')}
          />
        </View>
      </LinearGradient>
      <View
        elevation={4}
        style={[tailwind('px-4 py-6 rounded-xl bg-white'), mystyles.loginbox]}>
        <Text style={[tailwind('py-3 text-xl'), styles.SemiBoldFont]}>
          Sign In
        </Text>
        <View
          style={[
            tailwind('flex flex-row items-center border-b-2 border-gray-300'),
          ]}>
          <Text style={[tailwind('px-1 text-base'), styles.SemiBoldFont]}>
            +91
          </Text>
          <TextInput
            value={mobilenumber}
            keyboardType="number-pad"
            maxLength={10}
            onChangeText={(e) => {
              setmobilenumber(e);
            }}
            style={[
              tailwind('px-2 text-base py-2 w-11/12'),
              styles.RegularFont,
            ]}
          />
        </View>
        <Text
          style={[
            tailwind('text-xs py-4 text-center px-1'),
            styles.RegularFont,
          ]}>
          You will receive a 4 digit OTP throgh SMS
        </Text>
        <Pressable
          onPress={() => onFormSubmit()}
          style={[tailwind('rounded-lg py-3'), styles.BackgroundPrimary]}>
          <Text
            style={[
              tailwind('text-lg text-center'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            LOGIN
          </Text>
        </Pressable>
      </View>
      <View style={mystyles.bottom}>
        <Text></Text>
      </View>

      <View style={tailwind('py-3 absolute bottom-0 w-full')}>
        <View style={tailwind('flex flex-col items-center h-4 w-full')}>
          <Text style={[tailwind('text-center'), styles.RegularFont]}>
            By signing in, you accept to the{' '}
            <View style={tailwind('flex flex-col items-center h-4 w-full')}>
              <Pressable
                style={tailwind('')}
                onPress={() => {
                  props.navigation.navigate('WebViewScreen');
                }}>
                <Text
                  style={[
                    tailwind('text-center underline'),
                    styles.ColorSecondary,
                    styles.RegularFont,
                  ]}>
                  privacy policy
                </Text>
              </Pressable>
            </View>
          </Text>
        </View>

        <View style={tailwind('flex flex-col items-center my-2 w-full')}>
          <Text style={[tailwind('text-center'), styles.RegularFont]}>
            and{' '}
            <Text
              onPress={() => {
                props.navigation.navigate('WebViewScreen');
              }}
              style={[
                tailwind('text-center underline'),
                styles.ColorSecondary,
                styles.RegularFont,
              ]}>
              Terms & Conditions
            </Text>
            <Text style={tailwind('text-center')}>of Tribata</Text>
          </Text>
        </View>
      </View>
    </ScrollView>
  );
}

let mystyles = StyleSheet.create({
  top: {
    height: Dimensions.get('window').height / 2.1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  bottom: {
    // height: Dimensions.get('window').height / 2,
  },
  loginbox: {
    marginHorizontal: Dimensions.get('window').width / 15,
    position: 'relative',
    bottom: Dimensions.get('window').height / 6,
    // marginHorizontal: 30,
  },
});

// OTP Screeen Get Mobile number in route params as mobilenumber with +9187654768567
