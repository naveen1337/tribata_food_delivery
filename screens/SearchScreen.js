import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  Pressable,
  TextInput,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {findRestaurantRemote} from '../utils/restaurantRemoteService';
import {useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';

import {GeneralTopBar, RestaurantCard} from '../components';

export default function HomeScreen(props) {
  const LocationState = useSelector((state) => state.app.location);

  const [loading, setloading] = useState(false);
  const [result, setresult] = useState([]);
  const [keyword, setkeyword] = useState('');
  const [noresult, setnoresult] = useState(false);

  useEffect(() => {
    (async () => {
      if (keyword != '') {
        setresult([]);
        setnoresult(false);
        setloading(true);
        let result = await findRestaurantRemote({
          latitude: LocationState.latitude,
          longitude: LocationState.longitude,
          search_keyword: keyword,
        });
        // console.log('>>>>', result);
        if (result.length > 0) {
          setresult(result);
          setloading(false);
        } else {
          setnoresult(true);
          setloading(false);
        }
      } else {
        setresult([]);
      }
    })();
  }, [keyword]);

  return (
    <ScrollView
      keyboardShouldPersistTaps={'handled'}
      style={[styles.BackgroundSecondary]}>
      <GeneralTopBar name="Search Restaurant" navigation={props.navigation} />

      {/* Spinner */}
      {/* <Spinner visible={loading} /> */}

      <View
        elevation={3}
        style={tailwind('flex flex-row bg-white items-center m-3 rounded')}>
        <Icon
          style={tailwind('px-3')}
          name="search"
          color="#000000"
          size={22}
        />
        <TextInput
          placeholder="Search Restaurant"
          value={keyword}
          onChangeText={(e) => setkeyword(e)}
          style={[
            tailwind(
              'flex-grow px-2 bg-white rounded border-gray-500 rounded text-base',
            ),
            styles.RegularFont,
          ]}
        />
        <Pressable style={tailwind('px-1')} onPress={() => setkeyword('')}>
          <Icon
            style={tailwind('px-3')}
            name="close"
            color="#000000"
            size={22}
          />
        </Pressable>
      </View>

      {result.map((item) => {
        return (
          <RestaurantCard
            id={item.id}
            key={item.id}
            navigation={props.navigation}
            city_id={item.city_id}
            image={item.Restaurant_image}
            name={item.Restaurant_name}
            rating={item.Restaurant_rating}
            special={item.special}
            showdistance={true}
            distance={item.distance}
            intime={item.Restaurant_intime}
            outtime={item.Restaurant_outtime}
            city={item.city_name}
            status={item.Restaurant_status === '0'}
          />
        );
      })}
      {loading ? <ActivityIndicator size="large" color="#0000ff" /> : null}
      {noresult ? (
        <View style={tailwind('flex flex-row justify-center items-center')}>
          <View style={tailwind('flex flex-col justify-center items-center')}>
            <Icon name="search" size={60} color="#000000" />
            <Text
              style={[tailwind('text-center text-lg my-4'), styles.MediumFont]}>
              No Restaurant Found
            </Text>
          </View>
        </View>
      ) : null}
    </ScrollView>
  );
}
