import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {useSelector} from 'react-redux';
import jwt_decode from 'jwt-decode';

import {
  GeneralTopBar,
  ProfileCard,
  WalletBox,
  UserAddressBox,
} from '../components';

export default function UserProfileScreen(props) {
  const userInfoState = useSelector((state) => state.app.userinfo);

  const [localuserinfo, setlocaluserinfo] = useState('');

  useEffect(() => {
    setlocaluserinfo(userInfoState);
  }, [userInfoState]);

  return (
    <ScrollView style={[styles.BackgroundSecondary]}>
      <GeneralTopBar name="Profile" navigation={props.navigation} />
      <LinearGradient
        style={[tailwind('pt-4 pb-8 rounded-b-xl')]}
        colors={['#000000', '#2E2E2E']}>
        <ProfileCard
          name={localuserinfo.name}
          mail={localuserinfo.mail}
          mobilenumber={localuserinfo.mobilenumber}
          verified={localuserinfo.verified === 'true'}
          qrcode={localuserinfo.qrcode}
          navigation={props.navigation}
        />
      </LinearGradient>

      <View style={tailwind('mx-3 my-3')}>
        <WalletBox
          amount={jwt_decode(userInfoState.jwt).wallet}
          navigation={props.navigation}
        />
      </View>

      <Pressable
        elevation={3}
        onPress={() => props.navigation.navigate('OrderHistoryScreen')}
        style={tailwind(
          'flex flex-row flex-grow px-2 justify-between mx-4 my-2 py-4 bg-white rounded-xl',
        )}>
        <Text style={[tailwind('text-base'), styles.MediumFont]}>
          Order History
        </Text>
        <Icon name="chevron-forward-outline" size={20} color="#CBA960" />
      </Pressable>

      <Pressable
        elevation={3}
        onPress={() => props.navigation.navigate('OrderHistoryScreen')}
        style={tailwind(
          'flex flex-row px-2 flex-grow justify-between mx-4 my-2 py-4 bg-white rounded-xl',
        )}>
        <Text style={[tailwind('text-base'), styles.MediumFont]}>
          My Orders
        </Text>
        <Icon name="chevron-forward-outline" size={20} color="#CBA960" />
      </Pressable>

      <Text style={[tailwind('text-sm pt-2 px-4'), styles.MediumFont]}>
        Addresses
      </Text>

      {userInfoState.alladdress.length > 0 ? (
        userInfoState.alladdress.map((item) => {
          return (
            <View key={item.address_id}>
              <UserAddressBox address={item} />
            </View>
          );
        })
      ) : (
        <View
          style={tailwind(
            'flex flex-col items-center justify-center pb-5 pt-5',
          )}>
          <Icon name="search" size={30} color="#000000" />
          <Text style={[tailwind('text-sm pt-2 px-4'), styles.MediumFont]}>
            No Addresses Found
          </Text>
        </View>
      )}

      <Pressable
        onPress={() =>
          props.navigation.navigate('AddressEntryScreen', {
            back: 'profile',
          })
        }
        style={[
          tailwind(
            'rounded-lg py-3 my-2 mx-4 flex flex-row justify-center items-center',
          ),
          styles.BackgroundPrimary,
        ]}>
        <Icon name="add" size={25} color="#ffffff" />
        <Text
          style={[
            tailwind('text-lg uppercase text-center'),
            styles.SemiBoldFont,
            styles.ColorSecondary,
          ]}>
          Add Addresses
        </Text>
      </Pressable>
    </ScrollView>
  );
}
