import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  Pressable,
  Keyboard,
  TextInput,
  ScrollView,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {useSelector, useDispatch} from 'react-redux';
import MapView, {Marker} from 'react-native-maps';
import Spinner from 'react-native-loading-spinner-overlay';
import {fetchReverseGcodeRemote} from '../utils/homeRemoteService';
import {addUserAddressRemote} from '../utils/userRemoteService';
import Icon from 'react-native-vector-icons/Ionicons';
import Snackbar from 'react-native-snackbar';
import {saveUserAddressAction} from '../actions/userAction';
import {useNavigation, useRoute} from '@react-navigation/native';

export default function EditAddressScreen() {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();

  const UserState = useSelector((state) => state.app.userinfo);

  const [idle, setidle] = useState(false);
  const [title, settitle] = useState('');
  const [address, setaddress] = useState('');
  const [landmark, setlandmark] = useState('');

  const [locallocation, setlocallocation] = useState('');
  const [marginBottom, setmarginBottom] = useState(1);

  useEffect(() => {
    const filtered = UserState.alladdress.find(
      (item) => item.address_id === route.params.id,
    );
    if (filtered) {
      settitle(filtered.address_name);
      setaddress(filtered.address);
      setlandmark(filtered.landmark);
      console.log('Filtered', filtered);
      setlocallocation({
        latitude: filtered.latitude,
        longitude: filtered.longitude,
      });
      setidle(true);
    }
    // const kbshow = Keyboard.addListener('keyboardDidShow', (e) => console.log(e));
    // const kbshide = Keyboard.addListener('keyboardDidHide', (e) => console.log(e));
  }, []);

  useEffect(() => {
    let _ismounted = true;
    (async () => {
      if (_ismounted) {
        const result = await fetchReverseGcodeRemote(locallocation);
        if (result) {
          //   console.log(result);
          setaddress(result.display_name);
        }
      }
    })();

    return () => {
      _ismounted = false;
    };
  }, [locallocation]);

  const onFormSubmit = async () => {
    if (landmark && address) {
      let newobj = {
        mobile_number: UserState.mobilenumber,
        address_name: title,
        address: address,
        city: '00000000',
        landmark: landmark,
        pincode: '000000',
        latitude: locallocation.latitude,
        longitude: locallocation.longitude,
      };
      setidle(false);
      const done = await addUserAddressRemote(newobj);
      if (done) {
        Snackbar.show({
          backgroundColor: 'green',
          numberOfLines: 2,
          text: `Address Updated`,
          duration: Snackbar.LENGTH_LONG,
          action: {
            text: 'Ok',
            textColor: 'white',
            onPress: () => {},
          },
        });
        dispatch(saveUserAddressAction(newobj));
        setidle(true);
        navigation.goBack();
      } else {
        setidle(true);
        Alert.alert('Error', 'Address Updation Failed');
      }
    } else {
      Alert.alert(
        'All Fields are Required',
        'Got a null input for mandatory textbox',
      );
    }
  };

  return (
    <ScrollView style={[styles.BackgroundSecondary]}>
      <Spinner visible={!idle} />

      <MapView
        onRegionChangeComplete={(e) =>
          setlocallocation({
            latitude: e.latitude,
            longitude: e.longitude,
          })
        }
        style={[
          tailwind(''),
          {
            height: Dimensions.get('window').height / 2,
            width: '100%',
            margin: marginBottom,
          },
        ]}
        onMapReady={() => setmarginBottom(0)}
        showsUserLocation={true}
        showsMyLocationButton={true}
        zoomControlEnabled={true}
        initialRegion={{
          latitude: parseFloat(locallocation.latitude),
          longitude: parseFloat(locallocation.longitude),
          latitudeDelta: 0.0043,
          longitudeDelta: 0.0034,
        }}>
        <Marker
          coordinate={{
            latitude: parseFloat(locallocation.latitude),
            longitude: parseFloat(locallocation.longitude),
            latitudeDelta: 0.0043,
            longitudeDelta: 0.0034,
          }}
          icon={require('../assets/icons/pin_target.png')}></Marker>
      </MapView>

      {/* The Input Section */}
      <View
        style={[
          tailwind('px-3 py-2'),
          styles.BackgroundSecondary,
          {height: Dimensions.get('window').height / 2},
        ]}>
        <View
          elevation={3}
          style={tailwind(
            'flex flex-row items-center bg-white rounded mt-3 mb-3',
          )}>
          <Icon
            style={tailwind('p-2')}
            name="home-outline"
            size={20}
            color="#000000"
          />
          <TextInput
            value={title}
            editable={false}
            placeholder="Title"
            style={[
              tailwind('text-base text-gray-600 flex-grow text-black'),
              styles.RegularFont,
            ]}
          />
        </View>

        <View
          elevation={3}
          style={tailwind('flex flex-row items-center bg-white rounded  mb-3')}>
          <Icon
            style={tailwind('p-2')}
            name="business-outline"
            size={20}
            color="#000000"
          />
          <TextInput
            value={landmark}
            onChangeText={(e) => setlandmark(e)}
            placeholder="Landmark"
            style={[tailwind('text-base flex-grow'), styles.RegularFont]}
          />
        </View>

        <View elevation={3} style={tailwind('flex  bg-white rounded mb-3')}>
          <TextInput
            value={address}
            multiline={true}
            onChangeText={(e) => setaddress(e)}
            placeholder="Address"
            style={[tailwind('text-base flex-grow'), styles.RegularFont]}
          />
        </View>

        <Pressable
          onPress={onFormSubmit}
          style={tailwind('bg-black rounded py-3')}>
          <Text
            style={[
              tailwind('text-center text-base uppercase'),
              styles.SemiBoldFont,
              styles.ColorSecondary,
            ]}>
            Update Address
          </Text>
        </Pressable>
      </View>
    </ScrollView>
  );
}

// Address id is a mandatory in route param
