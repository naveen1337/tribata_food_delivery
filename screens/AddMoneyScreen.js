import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  ScrollView,
  RefreshControl,
  Pressable,
  Alert,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import {useIsFocused} from '@react-navigation/core';
import {CommonActions} from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';
import {save_user} from '../actions/appActions';
import {saveQRCode} from '../actions/userAction';
import Snackbar from 'react-native-snackbar';
import {useSelector, useDispatch} from 'react-redux';
import jwt_decode from 'jwt-decode';

import {
  checkTokenRemote,
  transferWalletRemote,
  addWalletRemote,
} from '../utils/userRemoteService';
import Modal from 'react-native-modal';
import RazorpayCheckout from 'react-native-razorpay';
import {createRazorpay} from '../utils/userRemoteService';
import {GeneralTopBar, TransactionHistory} from '../components';
import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import {getJwtToken, setJwtToken} from '../utils/helpers';
import {saveTransactionHistoryAction} from '../actions/appActions';
import {cloneDeep} from 'lodash';

export default function AddMoneyScreen(props) {
  const isFocused = useIsFocused();
  const dispatch = useDispatch();
  const userState = useSelector((state) => state.app.userinfo);
  const QrcodeState = useSelector((state) => state.user.qrcodedata);
  const TransactionHistoryState = useSelector(
    (state) => state.app.transactions,
  );

  const [refreshing, setRefreshing] = useState(false);
  const [addmoneymodal, setaddmoneymodal] = useState(false);
  const [transferoptionmodal, settransferoptionmodal] = useState(false);
  const [moneytransfermodal, setmoneytransfermodal] = useState(false);
  const [loading, setloading] = useState(false);
  const [qrcode, setqrcode] = useState(false);
  const [successmodal, setsuccessmodal] = useState(false);

  // const [enteredmobile, setenteredmobile] = useState('+919566710494');
  const [enteredmobile, setenteredmobile] = useState('');
  const [enteredamount, setenteredamount] = useState('');

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    setTimeout(() => {
      dispatch(saveTransactionHistoryAction({}));
      setRefreshing(false);
    }, 3000);
  }, []);

  const closeoptionmodal = () => {
    console.log('Close Option Modal');
    settransferoptionmodal(false);
    dispatch(saveQRCode(false));
  };

  const closetranfermoneymodal = () => {
    console.log('Close closetranfermoneymodal Modal');
    setmoneytransfermodal(false);
    dispatch(saveQRCode(false));
  };

  const inittransfer = async () => {
    if(enteredmobile === '' || enteredamount == ''){
      Alert.alert('Invalid input','All Fields are mandatory')
      return
    }
    setmoneytransfermodal(false);
    setloading(true);
    const response = await transferWalletRemote({
      amount: enteredamount,
      mobile_number: '+91' + enteredmobile,
    });
    console.log(response);
    if (response) {
      await setJwtToken(response);
      const newuserstate = cloneDeep(userState);
      newuserstate['jwt'] = response;
      // console.log('New User', response);
      dispatch(save_user(newuserstate));
      dispatch(saveTransactionHistoryAction({}));
      setloading(false);
      Snackbar.show({
        numberOfLines: 2,
        text: `Wallet Updated Succesfully`,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: 'green',
        action: {
          text: 'Ok',
          textColor: 'black',
          onPress: () => {},
        },
      });
      props.navigation.dispatch(
        CommonActions.reset({
          index: 1,
          routes: [{name: 'HomeStack'}],
        }),
      );
    } else {
      setloading(false);
      Snackbar.show({
        numberOfLines: 2,
        text: `Tranfer Failed`,
        duration: Snackbar.LENGTH_LONG,
        backgroundColor: 'red',
        action: {
          text: 'Ok',
          textColor: 'black',
          onPress: () => {},
        },
      });
    }
  };

  useEffect(() => {
    (async () => {
      // await  setJwtToken("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJ0cmliYXRhLmluIiwiYXVkIjoiVEhFX0FVRElFTkNFIiwiaWF0IjoxNjE1NjExODkzLCJuYmYiOjE2MTU2MTE5MDMsImV4cCI6MTYxODIzOTg5MywibW9iaWxlX251bWJlciI6Iis5MTg3Nzg2MTU5MjEiLCJ3YWxsZXQiOjMzMTUsImNvZGUiOjYwODM1N30.Ny759h54_oE6sl7Mt9n2SfsFq0kaA_eps9jaYNaqgoc")
      console.log('mounted');
      if (isFocused) {
        console.log('hit');
        const checktoken = await checkTokenRemote({
          mobile_number: userState.mobilenumber,
        });
        dispatch(saveTransactionHistoryAction({}));
      } else {
        return;
      }
    })();
  }, [userState]);

  useEffect(() => {
    setmoneytransfermodal(false);
    if (QrcodeState) {
      setenteredmobile(QrcodeState);
      setmoneytransfermodal(true);
    } else {
      setmoneytransfermodal(false);
    }
  }, [QrcodeState]);

  const addmoneytowallet = async () => {
    setaddmoneymodal(false);
    setloading(true);
    const rzorder = await createRazorpay({
      amount: parseInt(enteredamount),
      order_id: Date.now().toString(),
    });
    if (rzorder) {
      const options = {
        description: 'Triabata Check',
        currency: 'INR',
        key: 'rzp_test_mclXfXMGebiyKX',
        amount: parseInt(enteredamount) * 1000,
        name: userState.name,
        order_id: rzorder.id,
        prefill: {
          name: userState.name,
          contact: userState.mobilenumber,
          email: userState.mail,
        },
        theme: {color: '#53a20e'},
      };
      setloading(false);
      RazorpayCheckout.open(options)
        .then(async (data) => {
          const updatewallet = await addWalletRemote({
            amount: enteredamount,
          });
          console.log('updatewallet : ', updatewallet);
          if (updatewallet) {
            await setJwtToken(updatewallet);
            const newuserstate = cloneDeep(userState);
            newuserstate['jwt'] = updatewallet;
            console.log('New User', newuserstate);
            dispatch(save_user(newuserstate));
          }
          console.log(`Success: ${data.razorpay_payment_id}`);
          Snackbar.show({
            numberOfLines: 2,
            text: `Wallet Updated Succesfully`,
            duration: Snackbar.LENGTH_LONG,
            backgroundColor: 'green',
            action: {
              text: 'Ok',
              textColor: 'black',
              onPress: () => {},
            },
          });
          props.navigation.dispatch(
            CommonActions.reset({
              index: 1,
              routes: [{name: 'HomeStack'}],
            }),
          );
          // setsuccessmodal(true);
        })
        .catch((error) => {
          console.log(`Error: ${error}`);
        });
    }
  };

  return (
    <ScrollView
      style={[styles.BackgroundSecondary]}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      <GeneralTopBar name="" navigation={props.navigation} />

      <Spinner visible={loading} />

      <LinearGradient
        style={[tailwind('pt-4 pb-3 rounded-b-xl')]}
        colors={['#000000', '#2E2E2E']}>
        <Text style={[tailwind('text-xl text-white px-3'), styles.RegularFont]}>
          Bucketry Balance
        </Text>
        <View
          style={tailwind(
            'flex flex-row justify-between px-4 py-4 items-center',
          )}>
          <Text style={[tailwind('text-3xl text-white'), styles.BoldFont]}>
            ₹ {jwt_decode(userState.jwt).wallet}
          </Text>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-16 h-16')}
            source={require('../assets/images/wallet.png')}
          />
        </View>
      </LinearGradient>

      <Pressable
        elevation={3}
        onPress={() => setaddmoneymodal(true)}
        style={tailwind(
          'flex flex-row  justify-between bg-white rounded-xl mx-3 my-3 py-4 px-3',
        )}>
        <View style={tailwind('flex flex-row items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-6')}
            source={require('../assets/images/addmoney.png')}
          />
          <Text style={[tailwind('text-base px-2'), styles.MediumFont]}>
            Add Money
          </Text>
        </View>
        <Icon name="chevron-forward" size={20} color="black" />
      </Pressable>

      <Pressable
        elevation={3}
        onPress={() => settransferoptionmodal(true)}
        style={tailwind(
          'flex flex-row justify-between bg-white rounded-xl mx-3 my-1 py-4 px-3',
        )}>
        <View style={tailwind('flex flex-row items-center')}>
          <Image
            resizeMode={'contain'}
            style={tailwind('w-6 h-6')}
            source={require('../assets/images/transfermoney.png')}
          />
          <Text style={[tailwind('text-base px-2'), styles.MediumFont]}>
            Transfer Money
          </Text>
        </View>
        <Icon name="chevron-forward" size={20} color="black" />
      </Pressable>

      <Text
        style={[tailwind('text-base px-3 py-2 mt-3 mb-1'), styles.MediumFont]}>
        Transaction History
      </Text>

      {/* Transaction history */}
      {TransactionHistoryState ? (
        TransactionHistoryState.map((item) => {
          return <TransactionHistory key={item.id} item={item} />;
        })
      ) : (
        <View
          style={tailwind('flex flex-col justify-center items-center py-6')}>
          <Icon name="search" size={20} color="#000000" />
          <Text style={[tailwind('text-sm py-3'), styles.SemiBoldFont]}>
            No Transaction Found
          </Text>
        </View>
      )}

      {/* Add Money Modal */}
      <Modal isVisible={addmoneymodal}>
        <View style={tailwind('bg-white rounded-lg py-6 px-3')}>
          <View style={tailwind('flex flex-row justify-between items-center')}>
            <View style={tailwind('flex flex-row items-center py-2')}>
              <Image
                resizeMode={'contain'}
                style={tailwind('w-6 h-6')}
                source={require('../assets/images/addmoney.png')}
              />
              <Text style={[tailwind('text-lg px-3'), styles.SemiBoldFont]}>
                Add Money
              </Text>
            </View>
            <Pressable
              onPress={() => setaddmoneymodal(false)}
              style={tailwind('px-3')}>
              <Icon name="close" size={20} color="#000000" />
            </Pressable>
          </View>
          <Text style={[tailwind('text-xs py-2 pb-7'), styles.RegularFont]}>
            Amount will be added to your wallet
          </Text>
          <View
            style={tailwind(
              'flex bg-white border border-gray-600 px-2 rounded flex-row items-center',
            )}>
            <Icon name="wallet-outline" size={20} color="#000000" />

            <TextInput
              value={enteredamount}
              onChangeText={(e) => setenteredamount(e)}
              placeholder="Enter Amount"
              keyboardType={'number-pad'}
              style={[tailwind('flex-grow'), styles.RegularFont]}
            />
          </View>
          <Pressable
            onPress={addmoneytowallet}
            style={tailwind('bg-black rounded py-3 my-3')}>
            <Text
              style={[
                tailwind('text-center text-base uppercase'),
                styles.ColorSecondary,
                styles.SemiBoldFont,
              ]}>
              Submit
            </Text>
          </Pressable>
        </View>
      </Modal>

      {/* Transfer Money Scanning & Input Modal Modal */}
      {transferoptionmodal ? (
        <Modal isVisible={true}>
          <View style={tailwind('bg-white rounded-lg py-6 px-3')}>
            <View
              style={tailwind(
                'flex flex-row justify-between items-center py-2',
              )}>
              <View style={tailwind('flex flex-row items-center')}>
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-6 h-6')}
                  source={require('../assets/images/transfermoney.png')}
                />
                <Text style={[tailwind('text-lg px-3'), styles.SemiBoldFont]}>
                  Transfer Money
                </Text>
              </View>
              <Pressable
                style={tailwind('px-4  py-1')}
                onPress={closeoptionmodal}>
                {/* <Text>Close Option</Text> */}
                <Icon name="close" size={20} color="#000000" />
              </Pressable>
            </View>

            <View
              style={tailwind(
                'flex flex-row items-center justify-center px-3 py-3',
              )}>
              <Pressable
                onPress={() => {
                  settransferoptionmodal(false);
                  props.navigation.navigate('QRcodeScanScreen');
                }}
                style={tailwind(
                  'p-3 flex flex-col items-center justify-center mx-6 rounded',
                )}>
                <Image
                  resizeMode={'contain'}
                  style={[tailwind('w-16 h-16'), {tintColor: '#CBA960'}]}
                  source={require('../assets/images/qr_scanner.png')}
                />
                <Text
                  style={[tailwind('text-center py-1'), styles.SemiBoldFont]}>
                  Scan
                </Text>
              </Pressable>
              <Pressable
                onPress={() => {
                  setenteredmobile('');
                  setmoneytransfermodal(true);
                }}
                style={tailwind(
                  'p-3 flex flex-col items-center justify-center mx-6 rounded',
                )}>
                <Icon name="phone-portrait-outline" size={60} color="#C39E50" />
                <Text
                  style={[tailwind('text-center py-1'), styles.SemiBoldFont]}>
                  Mobile Number
                </Text>
              </Pressable>
            </View>
          </View>
        </Modal>
      ) : null}

      {/* Transfer Money Modal with phone number and amount */}
      {moneytransfermodal ? (
        <Modal isVisible={true}>
          <View style={tailwind('bg-white rounded-lg py-6 px-3')}>
            <View
              style={tailwind('flex flex-row items-center justify-between')}>
              <View style={tailwind('flex flex-row items-center')}>
                <Image
                  resizeMode={'contain'}
                  style={tailwind('w-6 h-6')}
                  source={require('../assets/images/transfermoney.png')}
                />

                <Text style={[tailwind('text-lg px-3'), styles.MediumFont]}>
                  Transfer Money
                </Text>
              </View>
              <Pressable
                style={tailwind('px-3')}
                onPress={closetranfermoneymodal}>
                {/* <Text>Close</Text> */}
                <Icon name="close-outline" size={20} color="#000000" />
              </Pressable>
            </View>

            <Text style={[tailwind('py-2 text-xs'), styles.RegularFont]}>
              Amount will be transfer to customer wallet
            </Text>

            <View
              elevation={3}
              style={tailwind(
                'flex flex-row items-center bg-white rounded mt-3 mb-3',
              )}>
              <Icon
                style={tailwind('p-2')}
                name="call-outline"
                size={20}
                color="#000000"
              />
              <TextInput
                value={enteredmobile}
                onChangeText={(e) => setenteredmobile(e)}
                placeholder="Mobile Number"
                keyboardType={'number-pad'}
                style={[tailwind('text-base flex-grow'), styles.RegularFont]}
              />
            </View>

            <View
              elevation={3}
              style={tailwind(
                'flex flex-row items-center bg-white rounded mt-3 mb-3',
              )}>
              <Icon
                style={tailwind('p-2')}
                name="cash-outline"
                size={20}
                color="#000000"
              />
              <TextInput
                placeholder="Amount"
                value={enteredamount}
                onChangeText={(e) => setenteredamount(e)}
                keyboardType={'number-pad'}
                style={[tailwind('text-base flex-grow'), styles.RegularFont]}
              />
            </View>
            <Pressable
              onPress={inittransfer}
              style={tailwind('bg-black  rounded py-3')}>
              <Text
                style={[
                  tailwind('text-center text-base uppercase'),
                  styles.SemiBoldFont,
                  styles.ColorSecondary,
                ]}>
                Submit
              </Text>
            </Pressable>
          </View>
        </Modal>
      ) : null}

      {successmodal ? (
        <Modal isVisible={true}>
          <View
            style={tailwind(
              'bg-white p-4 rounded-lg flex flex-col justify-center items-center',
            )}>
            <Text
              style={[
                tailwind('text-2xl text-green-500'),
                styles.SemiBoldFont,
              ]}>
              Success!
            </Text>
            <Image
              source={require('../assets/images/successpayment.png')}
              resizeMode={'contain'}
              style={tailwind('w-20 h-20')}
            />
            <Text
              style={[
                tailwind('text-base text-center text-gray-500'),
                styles.SemiBoldFont,
              ]}>
              ₹ {enteredamount} has been Added to your wallet
            </Text>

            <Pressable
              onPress={() => {
                setsuccessmodal(false);
                // Call the transaction history Refresh
                refresh();
                setTimeout(() => {
                  props.navigation.reset({
                    index: 0,
                    routes: [{name: 'HomeScreen'}],
                  });
                }, 100);
              }}
              style={tailwind('w-60 py-2 my-3 border rounded-lg')}>
              <Text
                style={[
                  tailwind('text-base text-center uppercase'),
                  styles.SemiBoldFont,
                ]}>
                Back to Home
              </Text>
            </Pressable>

            <Pressable
              onPress={() => {
                setsuccessmodal(false);
                // Call the transaction history Refresh
                refresh();
              }}
              style={tailwind('w-60  py-2 my-3 bg-black border rounded-lg')}>
              <Text
                style={[
                  tailwind('text-base text-center uppercase'),
                  styles.SemiBoldFont,
                  styles.ColorSecondary,
                ]}>
                Ok
              </Text>
            </Pressable>
          </View>
        </Modal>
      ) : null}
    </ScrollView>
  );
}
