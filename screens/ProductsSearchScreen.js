import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  Pressable,
  TextInput,
} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';
import {findRestaurantProductsRemote} from '../utils/restaurantRemoteService';
import {useSelector} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import Icon from 'react-native-vector-icons/Ionicons';

import {GeneralTopBar, RestaurantProduct, CartButton} from '../components';

export default function HomeScreen(props) {
  const LocationState = useSelector((state) => state.app.location);

  const [loading, setloading] = useState(false);
  const [result, setresult] = useState([]);
  const [keyword, setkeyword] = useState('');
  const [noresult, setnoresult] = useState(false);

  useEffect(() => {
    (async () => {
      if (keyword != '') {
        setresult([]);
        setnoresult(false);
        setloading(true);
        let result = await findRestaurantProductsRemote({
          latitude: LocationState.latitude,
          longitude: LocationState.longitude,
          search_keyword: keyword,
          branch_id: props.route.params.branch_id,
        });
        // console.log('>>>>', result);
        if (result.length > 0) {
          setresult(result);
          setloading(false);
        } else {
          setnoresult(true);
          setloading(false);
        }
      } else {
        setresult([]);
      }
    })();
  }, [keyword]);

  return (
    <View style={tailwind('h-full')}>
      <ScrollView
        keyboardShouldPersistTaps={'handled'}
        style={[styles.BackgroundSecondary]}>
        <GeneralTopBar name="Search Products" navigation={props.navigation} />

        {/* Spinner */}
        {/* <Spinner visible={loading} /> */}

        <View
          elevation={3}
          style={tailwind('flex flex-row bg-white items-center m-3 rounded')}>
          <Icon
            style={tailwind('px-3')}
            name="search"
            color="#000000"
            size={22}
          />
          <TextInput
            placeholder="Search Products"
            value={keyword}
            onChangeText={(e) => setkeyword(e)}
            style={[
              tailwind(
                'flex-grow px-2 bg-white rounded border-gray-500 rounded text-base',
              ),
              styles.RegularFont,
            ]}
          />
          <Pressable onPress={() => setkeyword('')}>
            <Icon
              style={tailwind('px-3')}
              name="close"
              color="#000000"
              size={22}
            />
          </Pressable>
        </View>

        {result.map((item) => {
          // console.log(item);
          return (
            <View key={item.id} style={tailwind('mx-3')}>
              <RestaurantProduct
                id={item.id}
                recom={false}
                veg={item.type === '1'}
                name={item.product_name}
                desc={item.description}
                isfav={item.is_favourite}
                price={item.price}
                cityid={props.route.params.cityid}
                hotelid={item.restaurant_id}
                hasvariation={item.variation === 'true'}
                variation={item.price_variation}
                variation_id={item.variation_id}
                hasaddon={item.addon}
                addon_content={item.addon_content}
                status={item.product_status === '1'}
              />
            </View>
          );
        })}
        {loading ? <ActivityIndicator size="large" color="#0000ff" /> : null}
        {noresult ? (
          <View style={tailwind('flex flex-row justify-center items-center')}>
            <View style={tailwind('flex flex-col justify-center items-center')}>
              <Icon name="search" size={60} color="#000000" />
              <Text
                style={[
                  tailwind('text-center text-lg my-4'),
                  styles.MediumFont,
                ]}>
                No Products Found
              </Text>
            </View>
          </View>
        ) : null}
        <View style={tailwind('mb-20')}></View>
      </ScrollView>
      <View style={tailwind('absolute w-full bottom-5')}>
        <CartButton navigation={props.navigation} />
      </View>
    </View>
  );
}

// get a Route params as [branch_id]
