import React, {useState} from 'react';
import {View, Text, ScrollView, Pressable} from 'react-native';
import tailwind from 'tailwind-rn';
import {styles} from '../constants/styles';

export default function containerScreen(props) {
  const [state, setstate] = useState([
    'HomeScreen',
    'CartScreen'
  ]);
  return (
    <ScrollView style={tailwind('px-6 my-2 bg-white')}>
      {state.map((item) => {
        return (
          <Pressable
            key={item}
            onPress={() => props.navigation.navigate(item)}
            style={tailwind(
              'bg-black border border-yellow-300 my-2 rounded px-4 py-3',
            )}>
            <Text
              style={[tailwind('text-white text-center'), styles.RegularFont]}>
             {item}
            </Text>
          </Pressable>

        );
      })}
    </ScrollView>
  );
}
