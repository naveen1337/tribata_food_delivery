```js
// Add on Insert
const insertaddon = () => {
  // setaddonprompt(false)
  setselectedaddons([]);
  console.log('insertaddon', selectedaddons);
  let cleanselectedaddons = _.find(selectedaddons, {checked: true});
  console.log(cleanselectedaddons);
  if (cleanselectedaddons) {
    setselectedaddons(cleanselectedaddons);
    if (props.hasvariation) {
      addtoCart(svar.variation_id);
      setaddonmodal(false);
    } else {
      addtoCart(props.variation_id);
      setaddonmodal(false);
    }
  } else {
    console.log('No Add on Selected');
    if (props.hasvariation) {
      setaddonmodal(false);
      addtoCart(svar.variation_id);
    } else {
      addtoCart(props.variation_id);
      setaddonmodal(false);
    }
  }
};
```



```js
const addtoCart = (e) => {
  try {
    if (cartState.length === 10) {
      // Set Selected Restaurant in State [*] Need to work afterwards
      dispatch(saveRestaurant(props.hotelid));
    } else {
      console.log('Add to Cart Payload', e);
      if (props.hasaddon && selectedaddons.length === 0) {
        setaddonmodal(true);
      } else {
        console.log('>> Here', e);
        if (props.hasvariation) {
          let isincart = _.find(cartState, {variation_id: e.id});
          if (isincart === undefined) {
            // Variation not in cart
            let newobj = {
              name: props.name,
              price: svar.price,
              quantity: svar.quantity + 1,
              variation: svar.variation,
              variation_id: svar.variation_id,
              addons: selectedaddons,
            };
            console.log('Cart', newobj);
            dispatch(updateCart(newobj));
          } else {
            // Variation in a cart
            dispatch(updateCart(isincart));
          }
        } else {
          console.log('NO Variation Add to Cart');
          let isincart = _.find(cartState, {variation_id: e.id});
          if (isincart) {
            dispatch(updateCart(isincart));
            setloading(!setloading);
          } else {
            // Not in a Cart
            let newobj = {
              name: props.name,
              price: props.price,
              quantity: quantity + 1,
              variation: false,
              variation_id: e.id,
              addons: selectedaddons,
            };
            console.log(newobj);
            dispatch(updateCart(newobj));
            setloading(!setloading);
          }
        }
      }
    }
  } catch (err) {
    console.log('Add to Cart Error', err);
  }
};

```
