# old code

```js
const mutatelocalstorage = async () => {
    const remove = await AsyncStorage.removeItem('@isUserLoggedIn');
    const update = await AsyncStorage.setItem(
      '@isUserLoggedIn',
      JSON.stringify({
        status: true,
        mobilenumber: props.route.params.mobilenumber,
      }),
    );
    if (update === null) {
      const getlocal = await AsyncStorage.getItem('@isUserLoggedIn');
      // console.log(getlocal);
      dispatch(save_user({
        login:true,
        mobilenumber:props.route.params.mobilenumber
      }));
      props.navigation.navigate('HomeStack');
    } else {
      Alert.alert('Storage Error');
    }
  };

  // Check if user is a new into the system
  const userInfo = async () => {
    const user = await getUserInfo({
      mobile_number: props.route.params.mobilenumber,
    });
    if (user) {
      mutatelocalstorage();
    } else {
      // User Is new into the system, show Profile Edit Screen
      props.navigation.navigate('ProfileEditScreen', {
        mobilenumber: props.route.params.mobilenumber,
      });
    }
  };
```