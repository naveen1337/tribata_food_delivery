import {HTTP_METHODS} from '../constants/utils';
import {BASE_URL} from '../constants/utils';
import requestServer from './requestServer';

// URL's
let svc_city_status = 'get_city_status.php';
let svc_services = 'get_all_service.php';
let svc_banners = 'get_all_banner.php';
let svc_topsellers = 'nearby_hotel.php';
let svc_promo_restaurants = 'get_all_promo_code.php';
let svc_get_api = 'get_apikey.php ';
let svc_add_enquiry = 'add_enquiry.php';
let svc_distance = 'get_distance.php';

export const isCityAvailable = async (payload) => {
  try {
    // console.log('fetchServicesRemote Hit')
    let svc_url = BASE_URL + svc_city_status;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return true;
    } else {
      throw 'City is Not Available';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchServicesRemote = async (payload) => {
  try {
    // console.log('fetchServicesRemote Hit')
    let svc_url = BASE_URL + svc_services;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No Service Found';
    }
  } catch (err) {
    console.log(err);
    return [];
  }
};

export const fetchHomeBanners = async (payload) => {
  try {
    console.log('Fetch Home Banners Hit');
    let svc_url = BASE_URL + svc_banners;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No Banners Found';
    }
  } catch (err) {
    console.log(`Error: ${err} Payload: ${JSON.stringify(payload)}`);
    return false;
  }
};

export const fecthTopSellersRemote = async (payload) => {
  try {
    console.log('Fetch fecthTopSellersRemote Hit');
    console.log('TopSellers Payload: ', payload);
    let svc_url = BASE_URL + svc_topsellers;

    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log(data)
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No TopSellers Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchPromoRestaurantsRemote = async (payload) => {
  try {
    console.log('Fetch fetchPromoRestaurantsRemote Hit');
    console.log(payload);
    let svc_url = BASE_URL + svc_promo_restaurants;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log(data)
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No TopSellers Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchTopHotelsRemote = async (payload) => {
  try {
    console.log('Fetch fetchTopHotelsRemote Hit');
    let svc_url = BASE_URL + svc_distance;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log(data)
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No fetchTopHotelsRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};


export const fetchGetApiRemote = async (payload) => {
  try {
    console.log('Fetch fetchGetApiRemote Hit');
    console.log(payload);
    let svc_url = BASE_URL + svc_get_api;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log(data)
    if (data) {
      return data;
    } else {
      throw 'No fetchGetApiRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchReverseGcodeRemote = async (payload) => {
  try {
    console.log('Fetch fetchReverseGcodeRemote Hit');
    console.log(payload);
    let svc_url = `https://nominatim.openstreetmap.org/reverse?lat=${payload.latitude}&lon=${payload.longitude}&format=json`;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data) {
      return data;
    } else {
      throw 'No fetchReverseGcodeRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const createEnquiryRemote = async (payload) => {
  try {
    console.log('Fetch createEnquiryRemote Hit');
    // console.log(payload);
    let svc_url = BASE_URL + svc_add_enquiry;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log(data)
    if (data.status === 'success') {
      return true;
    } else {
      throw 'No createEnquiryRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};
