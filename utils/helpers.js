import SInfo from 'react-native-sensitive-info';

export const deleteToken = async (tokenName) => {
  try {
    const delToken = await SInfo.deleteItem(tokenName, {
      sharedPreferencesName: 'tribata_shared_prefs',
      keychainService: 'tribata_keychain',
    });
    return delToken;
  } catch (err) {
    console.log('Error in Shared Preference deleteToken');
  }
};

export const setJwtToken = async (token) => {
  try {
    const tokenRes = await SInfo.setItem('jwttoken', token, {
      sharedPreferencesName: 'tribata_shared_prefs',
      keychainService: 'tribata_keychain',
    });
    return true;
  } catch (err) {
    console.log('Error in Shared Preference setToken');
    return false;
  }
};

export const getJwtToken = async () => {
  try {
    const token = await SInfo.getItem('jwttoken', {
      sharedPreferencesName: 'tribata_shared_prefs',
      keychainService: 'tribata_keychain',
    });
    return token;
  } catch (err) {
    console.log('Error in Shared Preference getToken',err);
  }
};
