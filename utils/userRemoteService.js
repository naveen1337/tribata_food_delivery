import {HTTP_METHODS} from '../constants/utils';
import {BASE_URL} from '../constants/utils';
import requestServer from './requestServer';
import authRequestServer from './authRequestServer';
const axios = require('axios');

import store from '../store';
import {save_user} from '../actions/appActions';
import _ from 'lodash';

const svc_mobile_available = 'mobile_number_check.php';
const svc_otp_check = 'mobile_number_verification.php';
const svc_user_info = 'get_user.php ';
const svc_fav_res = 'get_fav_shop.php';
const svc_fav_products = 'get_favorites.php';
const svc_add_fav_res = 'add_fav_shop.php';
const svc_add_fav_product = 'add_favourite.php';
const svc_user_address = 'get_user_address.php';
const svc_add_address = 'user_address.php';
const svc_delete_address = 'delete_address.php';
const svc_update_user = 'update_user.php';
const get_all_city = 'get_all_city.php';
const svc_delivery_charge = 'get_delivery_charges.php';
const svc_create_order = 'create_order.php';
const svc_update_order = 'status_update.php';
const svc_order_tracking = 'order_status.php';
const svc_order_info = 'get_order.php';
const svc_all_orders = 'get_order_details.php';
const svc_razorpay = 'razorpay_key_demo.php';
const svc_check_token = 'check_token.php';
const svc_wallet_history = 'get_wallet_history.php';
const svc_add_wallet = 'add_wallet.php';
const svc_transfer = 'update_wallet.php';
const svc_check_delivery_address = 'change_delivery_address.php';

export const NumberAvailableRemote = async (payload) => {
  try {
    console.log('NumberAvailableRemote Hit');
    console.log(payload);
    let svc_url = BASE_URL + svc_mobile_available;
    console.log(svc_url);
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'Number Unservicable ';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const OTPCheckRemote = async (payload) => {
  try {
    console.log('OTPCheckRemote Hit');
    console.log(payload);
    let svc_url = BASE_URL + svc_otp_check;
    console.log(svc_url);
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log("vv",data)
    if (data.status === 'success') {
      return data;
    } else {
      throw 'OTP Invalid';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getUserInfo = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_user_info;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log("getUserInfo Data",data)
    if (data.status === 'success') {
      return data;
    } else {
      throw 'NO user Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getFavRestaurantRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_fav_res;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log("vv",data)
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'NO getFavRestaurantRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getFavProductsRemote = async (payload) => {
  try {
    // console.log('getUserInfo Hit');
    let svc_url = BASE_URL + svc_fav_products;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);

    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'NO getFavProductsRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const addFavResRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_add_fav_res;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'Error addFavResRemote';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const addFavProductRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_add_fav_product;
    console.log(payload);
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    console.log('vv', data);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'Error addFavProductRemote';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getUserAddresesRemote = async (payload) => {
  try {
    // console.log('getUserAddreses Hit');
    // console.log(payload);
    let svc_url = BASE_URL + svc_user_address;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'getUserAddreses Failed';
    }
  } catch (err) {
    console.log(err);
    return [];
  }
};

export const addUserAddressRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_add_address;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'addUserAddressRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const deleteAddressRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_delete_address;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      let userclone = _.cloneDeep(store.getState().app.userinfo);
      let addressindex = _.findIndex(store.getState().app.userinfo.alladdress, {
        address_id: payload.address_id,
      });
      userclone.alladdress.splice(addressindex, 1);
      if (userclone.alladdress.length === 0) {
        userclone.selectedaddress = false;
      }
      store.dispatch(save_user(userclone));
      return data;
    } else {
      throw 'deleteAddressRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const updateUser = async (payload) => {
  try {
    console.log('Update user Hit');
    console.log(payload);
    let svc_url = BASE_URL + svc_update_user;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'updateUser Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getAvailableCities = async (payload) => {
  try {
    console.log('getAvailableCities user Hit');
    console.log(payload);
    let svc_url = BASE_URL + get_all_city;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'getAvailableCities Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getDeliveryChargeRemote = async (payload) => {
  try {
    console.log('getDeliveryCharge Hit');
    console.log("Payload",payload);
    let svc_url = BASE_URL + svc_delivery_charge;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'getDeliveryCharge Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const CheckDeliveryAddressRemote = async (payload) => {
  try {
    console.log('CheckDeliveryAddressRemote Hit');
    // console.log(payload);
    let svc_url = BASE_URL + svc_check_delivery_address;
    console.log(payload);
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    console.log('vv', data);
    if (data.status === 'success') {
      return true;
    } else {
      return false;
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const placeOrder = async (payload) => {
  try {
    console.log('PlaceOrder Hit');
    // console.log(payload);
    let svc_url = BASE_URL + svc_create_order;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    console.log('Order:', data);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'placeOrder Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const createRazorpay = async (payload) => {
  try {
    // console.log('getOrderInfoRemote Hit');
    console.log(payload);
    console.log('createRazorpayRemote hit');
    let svc_url = BASE_URL + svc_razorpay;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    return data;
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const UpdateOrderRemote = async (payload) => {
  try {
    console.log('UpdateOrderRemote Hit');
    // console.log(payload);
    let svc_url = BASE_URL + svc_update_order;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    console.log('vv', data);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'UpdateOrder Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getOrderTrackingRemote = async (payload) => {
  try {
    // console.log('UpdateOrderRemote Hit');
    // console.log(payload);
    let svc_url = BASE_URL + svc_order_tracking;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'GetOrderTrackingRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getOrderInfoRemote = async (payload) => {
  try {
    // console.log('getOrderInfoRemote Hit');
    // console.log(payload);
    let svc_url = BASE_URL + svc_order_info;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'getOrderInfoRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getAllOrders = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_all_orders;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'getOrderInfoRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const hasAnyLiveOrderRemote = async (payload) => {
  try {
    // console.log(payload);
    let svc_url = BASE_URL + svc_all_orders;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    // console.log('vv', data);
    if (data.status === 'success') {
      const filter = data.Tribata.filter(
        (item) =>
          item.order_status != 'Cancelled' && item.order_status != 'Delivered',
      );
      if (filter.length > 1) {
        return true;
      } else {
        return false;
      }
    } else {
      throw 'hasAnyLiveOrderRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const checkTokenRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_check_token;
    console.log('checkTokenRemote hit');
    console.log(svc_url);
    const data = await authRequestServer(HTTP_METHODS.POST, svc_url, payload);
    console.log('>> Remote', data);
    if (data.status === 'success') {
      return data;
    } else {
      throw 'checkTokenRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const getWalletHistoryRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_wallet_history;
    console.log('getWalletHistoryRemote hit');
    const data = await authRequestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'getWalletHistoryRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const addWalletRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_add_wallet;
    console.log('addWalletRemote hit');
    const data = await authRequestServer(HTTP_METHODS.POST, svc_url, payload);
    console.log('>> Remote', data);
    if (data.status === 'success') {
      return data.jwt_token;
    } else {
      throw 'addWalletRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const transferWalletRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_transfer;
    console.log('transferWalletRemote hit');
    const data = await authRequestServer(HTTP_METHODS.POST, svc_url, payload);
    console.log('>> Remote', data);
    if (data.status === 'success') {
      return data.jwt_token;
    } else {
      throw 'transferWalletRemote Failed';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};
