import {HTTP_METHODS} from '../constants/utils';
import {BASE_URL} from '../constants/utils';
import requestServer from './requestServer';

const svc_res_info = 'get_single_store.php';
const svc_categories = 'get_category.php';
const svc_products = 'get_category_product.php';
const svc_find_res = 'search_restaurant.php';
const svc_recommended = 'get_recommended.php';
const svc_offer = 'get_offer_hotel.php';
const svc_find_res_products = 'search_product.php';

export const fetchRestaurantInfoRemote = async (payload) => {
  try {
    // console.log(`fetchCategoriesRemote Hit: Payload ${JSON.stringify(payload)}`)
    let svc_url = BASE_URL + svc_res_info;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      console.log('Payload', payload);
      throw 'No fetchRestaurantInfoRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchRestaurantOffersRemote = async (payload) => {
  try {
    // console.log(`fetchCategoriesRemote Hit: Payload ${JSON.stringify(payload)}`)
    let svc_url = BASE_URL + svc_offer;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      // console.log('Payload', payload)
      throw 'No fetchRestaurantOffersRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchCategoriesRemote = async (payload) => {
  try {
    // console.log(`fetchCategoriesRemote Hit: Payload ${JSON.stringify(payload)}`)
    let svc_url = BASE_URL + svc_categories;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No Categories Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchRecommendedRemote = async (payload) => {
  try {
    // console.log(
    //   `fetchRecommendedRemote Hit: Payload ${JSON.stringify(payload)}`,
    // );
    let svc_url = BASE_URL + svc_recommended;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No fetchRecommendedRemote Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const fetchProductsRemote = async (payload) => {
  try {
    let svc_url = BASE_URL + svc_products;
    // console.log(
    //   `fetchProductsRemote Hit: Payload ${svc_url} ${JSON.stringify(payload)}`,
    // );

    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No Categories Found';
    }
  } catch (err) {
    console.log(err);
    return false;
  }
};

export const findRestaurantRemote = async (payload) => {
  try {
    console.log(`findRestaurantRemote Hit: Payload ${JSON.stringify(payload)}`);
    let svc_url = BASE_URL + svc_find_res;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No findRestaurantRemote Found';
    }
  } catch (err) {
    console.log(err);
    return [];
  }
};

export const findRestaurantProductsRemote = async (payload) => {
  try {
    // console.log(`findRestaurantProductsRemote Hit: Payload ${JSON.stringify(payload)}`);
    let svc_url = BASE_URL + svc_find_res_products;
    const data = await requestServer(HTTP_METHODS.POST, svc_url, payload);
    if (data.status === 'success') {
      return data.Tribata;
    } else {
      throw 'No findRestaurantProductsRemote Found';
    }
  } catch (err) {
    console.log(err);
    return [];
  }
};
