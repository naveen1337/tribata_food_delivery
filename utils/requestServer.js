import {HTTP_METHODS} from '../constants/utils';
export default requestServer = async (method, url, payload) => {
  return new Promise((resolve, reject) => {
    let options = {
      method: method,
      headers: {
        'Content-Type': 'application/json',
      },
    };
    if (method === HTTP_METHODS.POST) {
      options.body = JSON.stringify(payload);
    }
    console.log(`Request ${url} : ${JSON.stringify(payload)}`);
    fetch(url, options)
      .then((serverResponse) => {
        if (serverResponse.ok) {
          serverResponse.json().then((data) => {
            resolve(data);
          });
        } else {
          serverResponse.json((err) => {
            console.log(err);
            reject(err);
          });
        }
      })
      .catch((err) => {
        console.log('Request Faild', err);
      });
  });
};
