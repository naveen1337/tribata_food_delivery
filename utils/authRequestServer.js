import {HTTP_METHODS} from '../constants/utils';
import {getJwtToken} from '../utils/helpers';

export default authRequestServer = async (method, url, payload) => {
  // JWT Token
  const token = await getJwtToken();
  return new Promise((resolve, reject) => {
    let options = {
      method: method,
      headers: {
        'Content-Type': 'application/json',
        Authorization: token,
      },
      credentials: 'include',
    };
    if (method === HTTP_METHODS.POST) {
      options.body = JSON.stringify(payload);
    }
    fetch(url, options)
      .then((serverResponse) => {
        if (serverResponse.ok) {
          serverResponse.json().then((data) => {
            resolve(data);
          });
        } else {
          serverResponse.json((err) => {
            console.log(err);
            reject(err);
          });
        }
      })
      .catch((err) => {
        console.log('Request Faild', err);
        reject(err);
      });
  });
};
