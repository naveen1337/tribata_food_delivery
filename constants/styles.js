export let styles = {
  BackgroundPrimary: {
    backgroundColor: '#000000',
  },
  BackgroundSecondary: {
    backgroundColor: '#FFF8E8',
  },
  ColorPrimary: {
    color: '#FFF8E8',
  },
  ColorSecondary: {
    color: '#CBA960',
  },
  BackgroundGray: {
    backgroundColor: '#58585B',
  },
  ColorGray: {
    color: '#58585B',
  },
  ShadowBox: {
    elevation: 2,
  },
  RegularFont: {
    fontFamily: 'Poppins-Regular',
  },
  MediumFont: {
    fontFamily: 'Poppins-Medium',
  },
  SemiBoldFont: {
    fontFamily: 'Poppins-SemiBold',
  },
  BoldFont: {
    fontFamily: 'Poppins-Bold',
  },
};
