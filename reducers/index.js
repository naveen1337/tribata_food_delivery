import {combineReducers} from 'redux';
import {home} from './homeReducer';
import {app} from './appReducer';
import {restaurant} from './restaurantReducer';
import {user} from './userReducer';
export default combineReducers({
  home,
  app,
  restaurant,
  user
});
