import {
  SAVE_SERVICES,
  SAVE_TOPSELLERS,
  SAVE_PROMO_RESTAURANT,
  SAVE_TOP_HOTELS,
} from '../actions/actionTypes';

const initialState = {
  services: [],
  topSellers: [],
  allPromo: [],
  top_hotels: [],
};

export const home = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_SERVICES:
      return {
        ...state,
        services: action.payload,
      };
    case SAVE_TOPSELLERS:
      return {
        ...state,
        topSellers: action.payload,
      };
    case SAVE_PROMO_RESTAURANT:
      return {
        ...state,
        allPromo: action.payload,
      };
    case SAVE_TOP_HOTELS:
      return {
        ...state,
        top_hotels: action.payload,
      };
    default:
      return state;
  }
};
