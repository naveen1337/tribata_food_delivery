import {
  SAVE_OFFER,
  SAVE_RESTAURANT_INFO
} from '../actions/actionTypes';

const initialState = {
  info:[],
  offer: [],
};

export const restaurant = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_OFFER:
      return {
        ...state,
        offer: action.payload,
      };
      case SAVE_RESTAURANT_INFO:
      return {
        ...state,
        info: action.payload,
      };
    default:
      return state;
  }
};
