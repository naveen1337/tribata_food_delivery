import {
  ADDTO_CART,
  SAVE_RESTAURANT,
  CLEAR_CART,
  SAVE_COUPON,
  SAVE_QRCODE,
} from '../actions/actionTypes';

const initialState = {
  // selectedrestaurant: 3, // it will be removed later
  restaurant: {
    id: '',
    cityid: '',
    name: '',
    image: '',
    special: '',
  },
  cart: [],
  coupon: false,
  qrcodedata: false,
};

export const user = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_RESTAURANT:
      return {
        ...state,
        restaurant: action.payload,
      };
    case ADDTO_CART:
      return {
        ...state,
        cart: action.payload,
      };
    case CLEAR_CART:
      return {
        ...state,
        cart: [],
      };
    case SAVE_COUPON:
      return {
        ...state,
        coupon: action.payload,
      };
    case SAVE_QRCODE:
      return {
        ...state,
        qrcodedata: action.payload,
      };
    default:
      return state;
  }
};
