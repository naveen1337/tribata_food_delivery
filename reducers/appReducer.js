import {
  SAVE_USER_INFO,
  SAVE_SERVICES,
  SAVE_LOCATION_COORS,
  SAVE_PICKUP_COST,
  SAVE_FAVORITE_RES,
  SAVE_FAVORITE_PRODUCT,
  SAVE_TRANSACTIONS,
} from '../actions/actionTypes';

const initialState = {
  userinfo: {
    login: false,
    id: 0,
    mobilenumber: null,
    name: null,
    mail: null,
    qrcode: null,
    verified: null,
    point: null,
    alladdress: [],
    selectedaddress: false,
    jwt: false,
    fcm:null
  },
  transactions: false,
  fav_res: false,
  fav_product: false,
  location: {
    latitude: '00.000',
    longitude: '00.000',
  },
  pickupcost: 0,
};

export const app = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_SERVICES:
      return {
        ...state,
        services: action.payload,
      };
    case SAVE_LOCATION_COORS:
      return {
        ...state,
        location: action.payload,
      };
    case SAVE_USER_INFO:
      return {
        ...state,
        userinfo: action.payload,
      };
    case SAVE_TRANSACTIONS:
      return {
        ...state,
        transactions: action.payload,
      };
    case SAVE_FAVORITE_RES:
      return {
        ...state,
        fav_res: action.payload,
      };
    case SAVE_FAVORITE_PRODUCT:
      return {
        ...state,
        fav_product: action.payload,
      };
    case SAVE_PICKUP_COST:
      return {
        ...state,
        pickupcost: action.payload,
      };
    default:
      return state;
  }
};
